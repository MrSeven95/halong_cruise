<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('pricepp_pre_coupon')->nullable();
            $table->double('pricepp');
            $table->integer('star')->nullable();
            $table->float('vote')->default(0);
            $table->string('name');
            $table->unsignedBigInteger('cruise_category_id');
            $table->unsignedBigInteger('cruise_area_id');
            $table->unsignedBigInteger('coupon_id')->nullable();
            $table->longText('introduction')->nullable();
            $table->integer('launch_year')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('no_of_decks')->nullable();
            $table->integer('no_of_cabins')->nullable();
            $table->float('height')->nullable();
            $table->float('width')->nullable();
            $table->float('length')->nullable();
            $table->string('address')->nullable();
            $table->string('destionation')->nullable();
            $table->longText('features_included')->nullable();
            $table->longText('features_excluded')->nullable();
            $table->longText('cancel_by_customer')->nullable();
            $table->longText('cancel_by_weather')->nullable();
            $table->longText('childen_policy')->nullable();
            $table->float('deposit_percent')->nullable();
            $table->timestamps();
            $table->foreign('cruise_category_id')->references('id')->on('cruise_categories')->onDelete('cascade');
            $table->foreign('cruise_area_id')->references('id')->on('cruise_areas')->onDelete('cascade');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cruises');
    }
}
