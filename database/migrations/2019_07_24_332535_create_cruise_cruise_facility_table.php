<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruiseCruiseFacilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruise_cruise_facility', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cruise_id');
            $table->unsignedBigInteger('cruise_facility_id');
            $table->timestamps();
            $table->foreign('cruise_id')->references('id')->on('cruises')->onDelete('cascade');
            $table->foreign('cruise_facility_id')->references('id')->on('cruise_facilities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cruise_cruise_facility');
    }
}
