<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaCruisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_cruises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cruise_id');
            $table->string('type_media');
            $table->string('url');
            $table->timestamps();
            $table->foreign('cruise_id')->references('id')->on('cruises')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_cruises');
    }
}
