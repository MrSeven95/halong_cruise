<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('capacity')->nullable();
            $table->double('cabin_size')->nullable();
            $table->string('bed_size')->nullable();
            $table->double('pricepp_2_day')->nullable();
            $table->double('pricepp_3_day')->nullable();
            $table->unsignedBigInteger('cruise_id');
            $table->timestamps();
            $table->foreign('cruise_id')->references('id')->on('cruises')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabins');
    }
}
