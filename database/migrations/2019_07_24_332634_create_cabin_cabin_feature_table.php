<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabinCabinFeatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabin_cabin_feature', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cabin_id');
            $table->unsignedBigInteger('cabin_feature_id');
            $table->timestamps();
            $table->foreign('cabin_id')->references('id')->on('cabins')->onDelete('cascade');
            $table->foreign('cabin_feature_id')->references('id')->on('cabin_features')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabin_cabin_feature');
    }
}
