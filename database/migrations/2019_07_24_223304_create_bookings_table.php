<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cruise_id');
            $table->unsignedBigInteger('cabin_id');
            $table->longText('booking_info');
            $table->longText('customer_info');
            $table->timestamp('time_start')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('time_end')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->double('amount_pre_coupon')->nullable();
            $table->double('amount');
            $table->timestamps();
            $table->foreign('cruise_id')->references('id')->on('cruises')->onDelete('cascade');
            $table->foreign('cabin_id')->references('id')->on('cabins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
