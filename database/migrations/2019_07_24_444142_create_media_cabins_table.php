<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaCabinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_cabins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cabin_id');
            $table->string('type_media');
            $table->string('url');
            $table->timestamps();
            $table->foreign('cabin_id')->references('id')->on('cabins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_cabins');
    }
}
