/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : cruise

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 29/07/2019 13:54:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admins_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (3, 'Admin', 'admin@adamo.com', NULL, '$2y$10$gJ29xAUd6XuctJmk2B1ZUOO.XlDZ0YVpA0IOICm0T6C3276609lMe', NULL, '2019-07-24 08:42:49', '2019-07-24 08:42:49');

-- ----------------------------
-- Table structure for booking_optional_service
-- ----------------------------
DROP TABLE IF EXISTS `booking_optional_service`;
CREATE TABLE `booking_optional_service`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `optional_service_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `booking_optional_service_booking_id_foreign`(`booking_id`) USING BTREE,
  INDEX `booking_optional_service_optional_service_id_foreign`(`optional_service_id`) USING BTREE,
  CONSTRAINT `booking_optional_service_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `booking_optional_service_optional_service_id_foreign` FOREIGN KEY (`optional_service_id`) REFERENCES `optional_services` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for booking_transfer_service
-- ----------------------------
DROP TABLE IF EXISTS `booking_transfer_service`;
CREATE TABLE `booking_transfer_service`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `transfer_service_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `booking_transfer_service_booking_id_foreign`(`booking_id`) USING BTREE,
  INDEX `booking_transfer_service_transfer_service_id_foreign`(`transfer_service_id`) USING BTREE,
  CONSTRAINT `booking_transfer_service_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `booking_transfer_service_transfer_service_id_foreign` FOREIGN KEY (`transfer_service_id`) REFERENCES `transfer_services` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bookings
-- ----------------------------
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cruise_id` bigint(20) UNSIGNED NOT NULL,
  `cabin_id` bigint(20) UNSIGNED NOT NULL,
  `num_rooms` int(11) NOT NULL,
  `num_adults` int(11) NOT NULL,
  `time_start` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `time_end` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `amount_pre_coupon` double NULL DEFAULT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `num_childs` int(255) NULL DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone_number` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `bookings_cruise_id_foreign`(`cruise_id`) USING BTREE,
  INDEX `bookings_cabin_id_foreign`(`cabin_id`) USING BTREE,
  CONSTRAINT `bookings_cabin_id_foreign` FOREIGN KEY (`cabin_id`) REFERENCES `cabins` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `bookings_cruise_id_foreign` FOREIGN KEY (`cruise_id`) REFERENCES `cruises` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bookings
-- ----------------------------
INSERT INTO `bookings` VALUES (1, 1, 1, 1, 2, '2019-07-26 13:36:43', '2019-07-27 13:36:43', 350, 180, NULL, NULL, 0, 'test', 'test@test.com', '', 'addresss', 'hanoi', 'vietnam', 'deposited');
INSERT INTO `bookings` VALUES (2, 1, 1, 0, 0, '2019-07-26 16:47:01', '2019-07-26 16:47:01', NULL, 0, NULL, NULL, NULL, 'adm', NULL, NULL, NULL, NULL, NULL, 'paid');

-- ----------------------------
-- Table structure for cabins
-- ----------------------------
DROP TABLE IF EXISTS `cabins`;
CREATE TABLE `cabins`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity_adult` int(11) NULL DEFAULT NULL,
  `capacity_child` int(11) NULL DEFAULT NULL,
  `cabin_size` double NULL DEFAULT NULL,
  `bed_size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pricepp_2_day` double NULL DEFAULT NULL,
  `pricepp_3_day` double NOT NULL,
  `cruise_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cabins_cruise_id_foreign`(`cruise_id`) USING BTREE,
  CONSTRAINT `cabins_cruise_id_foreign` FOREIGN KEY (`cruise_id`) REFERENCES `cruises` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cabins
-- ----------------------------
INSERT INTO `cabins` VALUES (1, 'Ocean suite ', 2, NULL, 35, 'Double/twin', 150, 200, 1, NULL, NULL);
INSERT INTO `cabins` VALUES (2, 'Elegance Suite ', 2, NULL, 45, 'Double/twin', 200, 250, 1, NULL, NULL);

-- ----------------------------
-- Table structure for coupons
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_coupon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `value_coupon` double NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of coupons
-- ----------------------------
INSERT INTO `coupons` VALUES (1, 'percent', 'discount 30%', 30, NULL, '2019-07-25 09:54:08');

-- ----------------------------
-- Table structure for cruise_categories
-- ----------------------------
DROP TABLE IF EXISTS `cruise_categories`;
CREATE TABLE `cruise_categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cruise_categories_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cruise_categories
-- ----------------------------
INSERT INTO `cruise_categories` VALUES (1, 'Luxury', NULL, NULL);
INSERT INTO `cruise_categories` VALUES (2, 'Mid-range\r\n', NULL, NULL);
INSERT INTO `cruise_categories` VALUES (3, 'Budget\r\n', NULL, NULL);
INSERT INTO `cruise_categories` VALUES (4, 'Family Cruises\r\n', NULL, NULL);
INSERT INTO `cruise_categories` VALUES (5, 'Couple Cruises\r\n', NULL, NULL);
INSERT INTO `cruise_categories` VALUES (6, 'New Cruises 2018\r\n', NULL, NULL);
INSERT INTO `cruise_categories` VALUES (8, 'Editor\'s pick', '2019-07-25 06:25:30', '2019-07-25 07:05:37');
INSERT INTO `cruise_categories` VALUES (11, 'aa', '2019-07-25 08:22:50', '2019-07-25 08:22:50');

-- ----------------------------
-- Table structure for cruise_cruise_facility
-- ----------------------------
DROP TABLE IF EXISTS `cruise_cruise_facility`;
CREATE TABLE `cruise_cruise_facility`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cruise_id` bigint(20) UNSIGNED NOT NULL,
  `cruise_facility_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cruise_cruise_facility_cruise_id_foreign`(`cruise_id`) USING BTREE,
  INDEX `cruise_cruise_facility_cruise_facility_id_foreign`(`cruise_facility_id`) USING BTREE,
  CONSTRAINT `cruise_cruise_facility_cruise_facility_id_foreign` FOREIGN KEY (`cruise_facility_id`) REFERENCES `cruise_facilities` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `cruise_cruise_facility_cruise_id_foreign` FOREIGN KEY (`cruise_id`) REFERENCES `cruises` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cruise_facilities
-- ----------------------------
DROP TABLE IF EXISTS `cruise_facilities`;
CREATE TABLE `cruise_facilities`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cruise_facilities_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cruise_facilities
-- ----------------------------
INSERT INTO `cruise_facilities` VALUES (1, 'Private balcony\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (2, 'Full sea view window\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (3, 'Windows\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (4, 'Air conditioner\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (5, 'Bath tub\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (6, 'Standing shower\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (7, 'Jacuzzi\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (8, 'Safety box\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (9, 'Wi-Fi\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (10, 'TV/DVD/Movie\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (11, 'Satellite TV channels\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (12, 'Outdoor pool\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (13, 'Spa/Massage\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (14, 'Sauna\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (15, 'Gym and fitness\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (16, 'Bar\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (17, 'Restaurant\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (18, 'Library\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (19, 'Babysitter', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (20, 'Souvenir shop\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (21, 'BBQ\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (22, 'Smoking area\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (23, 'Live music\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (24, 'Shuttle bus service\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (25, 'Butler service\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (26, 'Credit card payment\r\n', NULL, NULL);
INSERT INTO `cruise_facilities` VALUES (27, 'Meeting facilities\r\n', NULL, NULL);

-- ----------------------------
-- Table structure for cruises
-- ----------------------------
DROP TABLE IF EXISTS `cruises`;
CREATE TABLE `cruises`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `star` int(11) NULL DEFAULT NULL,
  `vote` double(8, 2) NOT NULL DEFAULT 0.00,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cruise_category_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `introduction` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `launch_year` int(11) NULL DEFAULT NULL,
  `capacity` int(11) NULL DEFAULT NULL,
  `no_of_decks` int(11) NULL DEFAULT NULL,
  `no_of_cabins` int(11) NULL DEFAULT NULL,
  `height` double(8, 2) NULL DEFAULT NULL,
  `width` double(8, 2) NULL DEFAULT NULL,
  `length` double(8, 2) NULL DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `destionation` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cancel_by_customer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `cancel_by_weather` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `booking_policy` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deposit_percent` double(8, 2) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `is_best_seller` int(11) NULL DEFAULT NULL,
  `display_ranking` int(255) NULL DEFAULT NULL,
  `journey_day1_2days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `journey_day2_2days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `journey_day1_3days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `journey_day2_3days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `journey_day3_3days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `included_2days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `excluded_2days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `included_3days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `excluded_3days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `image_avatar_id` int(11) NULL DEFAULT NULL,
  `image_seo_id` int(11) NULL DEFAULT NULL,
  `meta_title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `meta_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `meta_keyword` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cruises_coupon_id_foreign`(`coupon_id`) USING BTREE,
  INDEX `cruises_cruise_category_id_foreign`(`cruise_category_id`) USING BTREE,
  CONSTRAINT `cruises_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `cruises_cruise_category_id_foreign` FOREIGN KEY (`cruise_category_id`) REFERENCES `cruise_categories` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cruises
-- ----------------------------
INSERT INTO `cruises` VALUES (1, 5, 7.50, 'MON CHÉRI CRUISES', NULL, 1, 1, 'If you long for the finer things in life, seek a little adventure and take pleasure in feeling special, then come enjoy glamourous life style on board the 5-star Mon Cheri Cruises. Set sail from  Got Port (Ben Pha Got), it takes 1.5 hours to transfer from Hanoi Old Quarter which saves you from long, boring and tired drive. From the moment you board the Mon Cheri, you\'ll experience pure luxury on water with exquisite exterior and luxurious, comfortable interior. Your experience on the Mon Cheri will create unforgettable memories. Mon Chéri is the perfect blend of adventure and luxury. You\'re sure to find the perfect cruise adventure into a \"forgotten paradise\" of Halong Bay which is untouched by tourists.', 2018, NULL, NULL, 18, NULL, NULL, NULL, 'Got Pier, Hai Phong city, Vietnam\r\n', 'Hai Phong- Lan Ha Bay- Ba Trai Dao beach- Dark & Bright cave', 'More than 31 days prior to check-in: FREE of charge\r\nFrom 11 to 30 days prior to check-in: 30% of total rates\r\nFrom 04 to 10 days prior to check-in: 50% of total rates\r\nLess than 72 hours prior to check-in: 100% of total rates\r\nNo-Show: 100% of total rates', 'If the cancellation be announced by the Management Board of Halong Bay, depend on the timing of the cancellation, following policies will be applied:\r\n\r\nBefore departure from Hanoi to Halong Bay (none of the services has been used): 100% refund.\r\nDuring the trip (some services have been used: transfer, lunch, day trip, hotel in Halong, etc.): used services will be charged, the rest will be refunded.\r\nThe trip be cut down from 2 nights to 1 night, tourists will be charged a 2 days/1 night trip, plus other used services, the rest will be refunded.\r\nThe final cost will be confirmed by your consultant via email. In all cases, please contact us directly for any urgent help. We are here to support you with all of our best!', 'The rate for children applicable for one child sharing a twin or double cabin with two adults on mattress.\r\nChildren from 05 - 12: Stay with parent 75% adult rate.\r\nChildren under 05: Stay with parent FOC\r\nOnly one (01) child under 4 years old sharing room with two (02) adults is allowed for FOC per cabin', 30.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for media_cruises
-- ----------------------------
DROP TABLE IF EXISTS `media_cruises`;
CREATE TABLE `media_cruises`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cruise_id` bigint(20) UNSIGNED NOT NULL,
  `type_media` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `display_ranking` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `media_cruises_cruise_id_foreign`(`cruise_id`) USING BTREE,
  CONSTRAINT `media_cruises_cruise_id_foreign` FOREIGN KEY (`cruise_id`) REFERENCES `cruises` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_07_23_000000_create_admins_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_07_24_023150_create_cruise_activities_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_07_24_023214_create_cruise_facilities_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_07_24_023240_create_cabin_specs_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_07_24_023252_create_cabin_features_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_07_24_023325_create_transfer_services_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_07_24_023333_create_optional_services_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_07_24_023344_create_cruise_categories_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_07_24_023355_create_cruise_areas_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_07_24_023417_create_cabin_amenities_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_07_24_113113_create_coupons_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_07_24_113948_create_cruises_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_07_24_123051_create_cabins_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_07_24_223304_create_bookings_table', 1);
INSERT INTO `migrations` VALUES (17, '2019_07_24_223406_create_reviews_table', 1);
INSERT INTO `migrations` VALUES (18, '2019_07_24_332506_create_cruise_cruise_activity_table', 1);
INSERT INTO `migrations` VALUES (19, '2019_07_24_332535_create_cruise_cruise_facility_table', 1);
INSERT INTO `migrations` VALUES (20, '2019_07_24_332608_create_cabin_cabin_spec_table', 1);
INSERT INTO `migrations` VALUES (21, '2019_07_24_332634_create_cabin_cabin_feature_table', 1);
INSERT INTO `migrations` VALUES (22, '2019_07_24_332658_create_cabin_cabin_amenity_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_07_24_432725_create_booking_transfer_service_table', 1);
INSERT INTO `migrations` VALUES (24, '2019_07_24_432755_create_booking_optional_service_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_07_24_444124_create_media_cruises_table', 1);
INSERT INTO `migrations` VALUES (26, '2019_07_24_444142_create_media_cabins_table', 1);

-- ----------------------------
-- Table structure for optional_services
-- ----------------------------
DROP TABLE IF EXISTS `optional_services`;
CREATE TABLE `optional_services`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `optional_services_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cruise_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_send` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `reviews_cruise_id_foreign`(`cruise_id`) USING BTREE,
  CONSTRAINT `reviews_cruise_id_foreign` FOREIGN KEY (`cruise_id`) REFERENCES `cruises` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for transfer_services
-- ----------------------------
DROP TABLE IF EXISTS `transfer_services`;
CREATE TABLE `transfer_services`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `transfer_services_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
