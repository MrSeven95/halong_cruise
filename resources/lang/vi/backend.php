<?php

return [
    "cruise" => "Cruise",
    "cabin" => "Cabin",
    "cruise-area" => "Khu vực Cruise",
    "faq" => "FAQ",
    "review" => "Review",
    "free-service" => "Dịch vụ miễn phí",
    "transfer-service" => "Dịch vụ vận chuyển",
    "other-service" => "Dịch vụ khác",

    "catalog" => "Cấu hình",
    "coupon" => "Khuyến mãi",

    "cruise-config" => "Cấu hình Cruise",
    "faq-config" => "Cấu hình FAQ",
];