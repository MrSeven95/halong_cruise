<?php

return [
    "star" => "Số sao",
    "vote" => "Bình chọn",
    "name" => "Tên tàu",
    "slug" => "Slug",
    "cruise_category" => "Loại tàu",
    "introduction" => "Mô tả",
    "launch_year" => "Năm sản xuất",
    "no_of_cabins" => "Số cabin",
    "address" => "Cảng xuất phát",

    "is_best_seller" => "Best seller",
    "is_best_seller_on" => "Yes",
    "is_best_seller_off" => "No",

    "display_ranking" => "Thứ tự",

    "meta_title" => "Meta title",
    "meta_description" => "Meta description",
    "meta_keyword" => "Meta keyword",

    "status" => "Trạng thái",
    "status_actived" => "Actived",
    "status_inactived" => "In-Actived",

    "image_avatar" => "Ảnh đại diện",
    "image_seo" => "Ảnh SEO",
    "image_thumbnail" => "Ảnh thu nhỏ",

    "additional" => "Thông tin khác",
    "services" => "Dịch vụ",
    "activities" => "Hoạt động",
    "departure_info" => "Thông tin khởi hành",
    "activities_on_request" => "Hoạt động theo yêu cầu",

    "policy" => "Chính sách",
    "cancel_by_customer" => "Hủy bởi khách hàng",
    "cancel_by_weather" => "Hủy bởi thời tiết",
    "price_include" => "Giá đã bao gồm",
    "price_exclude" => "Giá chưa bao gồm",

    "journal" => "Hành trình",
    "journal_type1" => "2 ngày 1 đêm",
    "journal_type2" => "3 ngày 2 đêm",

    "journey_day1" => "Ngày 1",
    "journey_day2" => "Ngày 2",
    "journey_day3" => "Ngày 3",

    "journey_type1_day1_title" => "Hành trình đi qua các điểm",
    "journey_type1_day1_detail" => "Chi tiết",
    "journey_type1_day2_title" => "Hành trình đi qua các điểm",
    "journey_type1_day2_detail" => "Chi tiết",

    "journey_type2_day1_title" => "Hành trình đi qua các điểm",
    "journey_type2_day1_detail" => "Chi tiết",
    "journey_type2_day2_title" => "Hành trình đi qua các điểm",
    "journey_type2_day2_detail" => "Chi tiết",
    "journey_type2_day3_title" => "Hành trình đi qua các điểm",
    "journey_type2_day3_detail" => "Chi tiết",

    "area" => "Khu vực",
    "album" => "Album ảnh",
    "album-not-found" => "Không có ảnh trong album.",
];