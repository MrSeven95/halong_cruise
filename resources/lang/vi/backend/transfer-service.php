<?php

return [
    "name" => "Tên dịch vụ",
    "type_transfer" => "Loại xe",
    "type_price" => "Phương thức tính giá",
    "price" => "Giá (USD)",
    "price_1" => "Giá (USD/người)",
    "price_2" => "Giá (USD/xe)",


    "type_transfer_1" => "Xe bus",
    "type_transfer_2" => "Xe 4 chỗ",
    "type_transfer_3" => "Xe 7 chỗ",

    "type_price_1" => "Trên 1 người",
    "type_price_2" => "Trên 1 xe",
];