<?php

return [
    "subject" => "Chủ đề",
    "question" => "Câu hỏi",
    "answer" => "Câu trả lời",
    "status" => "Trạng thái",

    "status-on" => "Hiện",
    "status-off" => "Ẩn",
];