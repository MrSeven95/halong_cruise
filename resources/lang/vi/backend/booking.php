<?php

return [
    'status_new' => 'New',
    'status_deposited' => 'Deposited',
    'status_full_payment' => 'Full payment',
    'status_completed' => 'Completed',
    'status_cancel' => 'Cancel',
];