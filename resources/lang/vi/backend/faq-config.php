<?php

return [
    'heading' => 'Heading',
    'slug' => 'Slug',
    'image_avatar' => 'Ảnh đại diện',
    'image_seo' => 'Ảnh SEO',
    'meta_title' => 'Meta title',
    'meta_description' => 'Meta description',
    'meta_keyword' => 'Meta keyword',
    'image_thumbnail' => 'Ảnh thu nhỏ',
];