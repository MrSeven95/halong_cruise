<?php

return [
    "name" => "Tên khách hàng",
    "country" => "Quốc gia",
    "message" => "Nội dung",
    "time_send" => "Thời gian",
    "title" => "Tiêu đề",
    "point_cruise_quality" => "Cruise quality",
    "point_food_and_drink" => "Food & drink",
    "point_cabin_quality" => "Cabin quality",
    "point_customer_service" => "Customer service",
    "point_activities" => "Activities",
    "traveler_type" => "Traveler type",
    "traveler_rating" => "Total traveler rating",
    "display_ranking" => "Thứ tự hiển thị",
    "status" => "Trạng thái",

    "status-on" => "Hiện",
    "status-off" => "Ẩn",
];