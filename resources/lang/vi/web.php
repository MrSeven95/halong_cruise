<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application"s requirements.
    |
    */

    "create" => "Tạo mới",
    "edit" => "Sửa",
    "delete" => "Xóa",
    "save" => "Lưu",

    "success" => "Thành công.",
    "error" => "Có lỗi xảy ra.",

    "create-success" => "Thêm mới thành công.",
    "edit-success" => "Chỉnh sửa thành công.",
    "delete-success" => "Xóa thành công.",
    "save-success" => "Lưu thành công.",

    "create-successed" => "Thêm mới :name thành công.",
    "edit-successed" => "Chỉnh sửa :name thành công.",
    "delete-successed" => "Xóa :name thành công.",
    "save-successed" => "Lưu :name thành công.",

    "create-error" => "Thêm mới thất bại.",
    "edit-error" => "Chỉnh sửa thất bại.",
    "delete-error" => "Xóa thất bại.",
    "save-error" => "Lưu thất bại.",

    "create-errored" => "Thêm mới :name thất bại.",
    "edit-errored" => "Chỉnh sửa :name thất bại.",
    "delete-errored" => "Xóa :name thất bại.",
    "save-errored" => "Lưu :name thất bại.",

    "confirm-delete" => "Bạn có muốn xóa?",
    "confirm-delete-detail" => "Bạn sẽ không thể hoàn lại thao tác này!",
    "accept" => "Đồng ý",
    "cancel" => "Hủy",
    "deleted" => "Đã xóa",
    "cancelled" => "Đã hủy",

    "not-found" => "Không tồn tại :name",
    "invalid-image-type" => "Định dạng ảnh không được phép! Chấp nhận: JPG, JPEG, GIF, PNG",
    "max-upload-album" => "Giới hạn ảnh tối đa được tải lên là : :max . Vui lòng xóa những ảnh không sử dụng.",

    "action" => "Hành động",
    "more-action" => "Thêm",

    "creating" => "Tạo mới :name",
    "editing" => "Chỉnh sửa :name",

    "actived" => "Kích hoạt",
    "in-actived" => "Chưa kích hoạt",

    "select-an-option" => "Chọn một giá trị",
    "something-went-wrong" => "Something went wrong.",
];
