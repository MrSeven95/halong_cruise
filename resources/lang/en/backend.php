<?php

return [
    "cruise" => "Cruise",
    "cabin" => "Cabin",
    "cruise-area" => "Cruise Area",
    "faq" => "FAQ",
    "review" => "Review",
    "free-service" => "Free Services",
    "transfer-service" => "Transfer Services",
    "other-service" => "Other Services",

    "catalog" => "Setting",
    "coupon" => "Promotion",
    "bed-type" => "Bed type",

    "cruise-config" => "Cruise Config",
    "faq-config" => "FAQ Config",

    "apply-cruise" => "Apply for Cruise",
    "cruise-promotion" => "Apply Promotion",
    "cruise-free-service" => "Apply Free Service",

    "cabin-price" => "Price Management",
];