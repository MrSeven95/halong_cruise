<?php

return [
    "name" => "Service",
    "type_transfer" => "Type of car",
    "type_price" => "Type price",
    "price" => "Price (USD)",
    "price_1" => "Price (USD/person)",
    "price_2" => "Price (USD/car)",

    "type_transfer_1" => "Bus",
    "type_transfer_2" => "4-seat car",
    "type_transfer_3" => "7-seat car",

    "type_price_1" => "On 1 person",
    "type_price_2" => "On 1 car",
];