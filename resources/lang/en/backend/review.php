<?php

return [
    "name" => "Fullname",
    "country" => "Country",
    "message" => "Description",
    "time_send" => "Time",
    "title" => "Title",
    "point_cruise_quality" => "Cruise quality",
    "point_food_and_drink" => "Food & drink",
    "point_cabin_quality" => "Cabin quality",
    "point_customer_service" => "Customer service",
    "point_activities" => "Activities",
    "traveler_type" => "Traveler type",
    "traveler_rating" => "Total traveler rating",
    "display_ranking" => "Position",
    "status" => "Status",

    "status-on" => "Show",
    "status-off" => "Hide",
];