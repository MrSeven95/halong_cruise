<?php

return [
    "star" => "Star",
    "vote" => "Vote",
    "name" => "Cruise",
    "slug" => "Slug",
    "cruise_category" => "Type of cruise",
    "introduction" => "Introduction",
    "launch_year" => "Launch year",
    "no_of_cabins" => "Number of cabins",
    "address" => "Departure Port",

    "category_iron" => "Silver Cruise",
    "category_wood" => "Wooden Cruise",

    "is_best_seller" => "Best seller",
    "is_best_seller_on" => "Yes",
    "is_best_seller_off" => "No",

    "display_ranking" => "Position",

    "price_management" => "Price Management",

    "meta_title" => "Meta title",
    "meta_description" => "Meta description",
    "meta_keyword" => "Meta keyword",

    "status" => "Status",
    "status_actived" => "Actived",
    "status_inactived" => "In-Actived",

    "image_avatar" => "Avatar",
    "image_seo" => "SEO image",
    "image_thumbnail" => "Image thumbnail",

    "additional" => "Other information",
    "services" => "Services",
    "activities" => "Activities",
    "departure_info" => "Departure info",
    "departure_info" => "Departure info",
    "activities_on_request" => "Activities on request",

    "policy" => "Policy",
    "children_policy" => "Children Policy",
    "cancel_by_customer" => "Cancellation by Customer",
    "cancel_by_weather" => "Cancellation by Weather",
    "price_include" => "Price Includes",
    "price_exclude" => "Price Excludes",

    "journal" => "Journal",
    "journal_type1" => "2 days 1 night",
    "journal_type2" => "3 days 2 nights",

    "journey_day1" => "Day 1",
    "journey_day2" => "Day 2",
    "journey_day3" => "Day 3",

    "journey_type1_day1_title" => "Itinerary",
    "journey_type1_day1_detail" => "Journal detail",
    "journey_type1_day2_title" => "Itinerary",
    "journey_type1_day2_detail" => "Journal detail",

    "journey_type2_day1_title" => "Itinerary",
    "journey_type2_day1_detail" => "Journal detail",
    "journey_type2_day2_title" => "Itinerary",
    "journey_type2_day2_detail" => "Journal detail",
    "journey_type2_day3_title" => "Itinerary",
    "journey_type2_day3_detail" => "Journal detail",

    "journey_type1_day1_image" => "Album",
    "journey_type1_day2_image" => "Album",
    "journey_type2_day1_image" => "Album",
    "journey_type2_day2_image" => "Album",
    "journey_type2_day3_image" => "Album",

    "area" => "Area",
    "album" => "Album",
    "album-not-found" => "No image found in album.",
];