<?php

return [
    'cabin' => 'Cabin',
    'type' => 'Type',
    'price' => 'Cabin Price',
    'less_than_one_person' => 'Less than one person',
    'surchange' => 'Surchange',

    'type_single' => 'Single',
    'type_double' => 'Double',
    'type_twin' => 'Twin'
];