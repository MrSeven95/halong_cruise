<?php

return [
    "subject" => "Topic",
    "question" => "Question",
    "answer" => "Answer",
    "status" => "Status",

    "status-on" => "Show",
    "status-off" => "Hide",
];