<?php

return [
    'cruise' => 'Cruise',
    'free-service' => 'Free Service',
    'from' => 'From',
    'to' => 'To',
    'all-cruise' => 'All cruises'
];