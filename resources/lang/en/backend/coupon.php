<?php

return [
    'name' => 'Promotion',
    'value' => 'Discount (%)',
    'type_early_bird' => 'Early bird',
    'type_last_minute' => 'Last minute',
    'type_summer_sale' => 'Summer sale',
];