<?php

return [
    'status_new' => 'New',
    'status_deposited' => 'Deposited',
    'status_full_payment' => 'Full payment',
    'status_completed' => 'Completed',
    'status_cancel' => 'Cancel',

    'cruise' => 'Cruise',
    'cabin' => 'Cabin',
    'time_start' => 'Time start',
    'time_end' => 'Time end',
    'amount_pre_coupon' => 'Amount',
    'amount' => 'Amount',

    'passengers' => 'Passengers',
    'num_adults' => 'Capacity adults',
    'num_childs' => 'Capacity childen',
    'full_name' => 'Fullname',
    'email' => 'Email',
    'phone_number' => 'Phone number',
    'address' => 'Address',
    'city' => 'City',
    'country' => 'Country',
    'status' => 'Status',

];