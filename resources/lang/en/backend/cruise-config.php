<?php

return [
    'heading' => 'Heading',
    'slug' => 'Slug',
    'image_avatar' => 'Image',
    'image_seo' => 'Image SEO',
    'meta_title' => 'Meta title',
    'meta_description' => 'Meta description',
    'meta_keyword' => 'Meta keyword',
    'image_thumbnail' => 'Image thumbnail',
];