<?php

return [
    'cruise' => 'Cruise',
    'coupon' => 'Promotion',
    'discount' => 'Discount (%)',
    'from' => 'From',
    'to' => 'To',
    'all-cruise' => 'All cruises'
];