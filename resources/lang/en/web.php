<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application"s requirements.
    |
    */

    "create" => "Create",
    "edit" => "Edit",
    "delete" => "Delete",
    "save" => "Save",

    "success" => "Success.",
    "error" => "Error.",

    "create-success" => "Create successful.",
    "edit-success" => "Edit successful.",
    "delete-success" => "Delete successful.",
    "save-success" => "Save successful.",

    "create-successed" => "Create :name successful.",
    "edit-successed" => "Edit :name successful.",
    "delete-successed" => "Delete :name successful.",
    "save-successed" => "Save :name successful.",

    "upload-successed" => "Upload :name successful.",

    "create-error" => "Create failed.",
    "edit-error" => "Edit failed.",
    "delete-error" => "Delete failed.",
    "save-error" => "Save failed.",

    "create-errored" => "Create :name failed.",
    "edit-errored" => "Edit :name failed.",
    "delete-errored" => "Delete :name failed.",
    "save-errored" => "Save :name failed.",

    "upload-errored" => "Upload :name failed.",

    "confirm-delete" => "Do you want Delete?",
    "confirm-delete-detail" => "You will not be able to recover this operation!",
    "accept" => "Accept",
    "cancel" => "Cancel",
    "deleted" => "Deleted",
    "cancelled" => "Cancelled",

    "not-found" => "Not found :name",
    "invalid-image-type" => "Invalid image type. Accept: JPG, JPEG, GIF, PNG",
    "max-upload-album" => "Maximum size uploadable is : :max . Please re-upload.",

    "action" => "Action",
    "more-action" => "More Information",

    "creating" => "Creating :name",
    "editing" => "Editing :name",

    "actived" => "Actived",
    "in-actived" => "In-actived",

    "select-an-option" => "Select an option",
    "something-went-wrong" => "Something went wrong.",
];
