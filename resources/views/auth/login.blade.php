@extends('layouts.auth-admin')

@push('after-styles')
    <style>
        .logo .logo-outer {
            display: flex;
            width: 100%;
            min-height: 75px;
            margin-bottom: 15px;
            justify-content: center;
            align-items: center;
            background: #5bc0de !important;
        }
        .logo h3 {
            color: #414755;
        }
    </style>
@endpush

@section('content')

    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center">
        <div class="auth-box">
            <div id="loginform">
                <div class="logo">
                    <div class="logo-outer" style="width: 100%; background: #414755;">
                        <img src="{{ url('images/logo_white.png') }}" alt="homepage"/>
                    </div>
                    <h3 class="font-medium" style="margin-bottom: 0;color: #2962FF">{{ __('Login') }}</h3>
                </div>
                <!-- Form -->
                <div class="row">
                    <div class="col-12">
                        {!! Form::open(['method' => 'POST', 'route' => 'login', 'class' => 'form-horizontal mt-3']) !!}
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                            </div>
                            <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{__('Email')}}">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2"><i class="fa fa-lock mr-1"></i></span>
                            </div>
                            <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="current-password" placeholder="{{__('Password')}}">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                    <a href="{{route('password.request')}}" id="to-recover" class="text-dark float-right"><i class="fa fa-lock mr-1"></i> Forgot pwd?</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 pb-3">
                                <button class="btn btn-block btn-lg btn-info" type="submit">Log In</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
