<section class="foot-info">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="logo-foot clearfix">
                    <a href="index.html" title="Adamo Digital">
                        <img src="img/adamo_digital_logo_white.png" alt="" class="img-fluid float-left" width="96%">
                    </a>
                </div>
                <ul class="list-unstyled">
                    <li><a href="" title="About Us">About Us</a></li>
                    <li><a href="" title="Portfolio">Portfolio</a></li>
                    <li><a href="" title="Careers">Careers</a></li>
                    <li><a href="" title="Blog">Blog</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3">
                <h4 class="text-uppercase">OUR SERVICES</h4>
                <ul class="list-unstyled">
                    <li><a href="#" title="Mobile App Development">Mobile App Development</a></li>
                    <li><a href="#" title="Website Development">Website Development</a></li>
                    <li><a href="#" title="Big Data Development">Big Data Development</a></li>
                    <li><a href="#" title="Blockchain Development">Blockchain Development</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3">
                <h4 class="text-uppercase">OUR EXPERTISE</h4>
                <ul class="list-unstyled">
                    <li><a href="#" title="Food & Beverage">Food & Beverage</a></li>
                    <li><a href="#" title="Lifestyle, Travel & Hospitality">Lifestyle, Travel & Hospitality</a></li>
                    <li><a href="#" title="Retail & eCommerce">Retail & eCommerce</a></li>
                    <li><a href="#" title="eLearning">eLearning</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3">
                <h4 class="text-uppercase">GET IN TOUCH</h4>
                <div>
                    <p>#3008, A3 Building, Ecolife Capitol, 58 To Huu, Hanoi, Vietnam</p>
                    <p>Email: <a href="mailto:hello@adamodigital.com">hello@adamodigital.com</a></p>
                    <p>Phone: +84 91 668 1124</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="copy">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <p>&copy; 2019 Adamo Digital Co, Ltd. All Rights Reserved</p>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <nav class="nav">
                    <a class="nav-link" href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a class="nav-link" href="#" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="nav-link" href="#" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a class="nav-link" href="#" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                </nav>
            </div>
        </div>
    </div>
</section>