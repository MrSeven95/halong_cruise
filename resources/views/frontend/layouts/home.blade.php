<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @hasSection('title')
            @yield('title') - Luxury Halong Cruises
        @else
            Luxury Halong Cruises
        @endif
    </title>
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    <link rel="icon" href="{{ url('frontend/img/favicon.png') }}">
    @yield('meta')

    @stack('before-styles')

    <link rel="stylesheet" href="{{ url('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/awesome/css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ url('frontend/css/animate.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/style.css') }}">

    <link rel="stylesheet" href="{{ url('frontend/css/mystyle.css') }}">

    {{ Html::style('css/mystyle.css') }}

    @stack('after-styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <section class="top">
        <div class="container">
            <p><span class="text-brown text-uppercase">CONTACT US:</span> <span>+84 93 594 6688</span> <span><a href="#" title="">info@luxuryhalongcruise.com</a></span></p>
        </div>
    </section>

    @if(Session::has('exception'))
        <div class="alert alert-danger">
            {{ Session::get('exception') }}
        </div>
    @endisset

    @yield('content')

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <img src="{{ url('frontend/img/logo_brown.png') }}" alt="Luxury Cruise" class="img-fluid img-footer">
                    <ul class="list-anchor">
                        <li><a href="#" title="About us">About us</a></li>
                        <li><a href="#" title="FAQs">FAQs</a></li>
                        <li><a href="#" title="Contact us">Contact us</a></li>
                        <li><a href="#" title="Blog">Blog</a></li>
                    </ul>
                </div> <!-- end col -->
                <div class="col-md-6 col-lg-3">
                    <h3 class="text-uppercase">FIND CRUISES & SERVICES</h3>
                    <ul>
                        <li><a href="#" title="Best seller Cruises">Best seller Cruises</a></li>
                        <li><a href="#" title="Luxury Halong Cruise">Luxury Halong Cruise</a></li>
                        <li><a href="#" title="Deluxe Halong Cruise">Deluxe Halong Cruise</a></li>
                        <li><a href="#" title="Transfer Services">Transfer Services</a></li>
                    </ul>
                </div> <!-- end col -->
                <div class="col-12 col-lg-1"></div><!-- end col -->
                <div class="col-md-12 col-lg-5">
                    <h3 class="text-uppercase">FOLLOW US</h3>
                    <ul class="follow-us">
                        <li>
                            <div class="row">
                                <div class="col-3 col-sm-2">
                                    <p class="text-brown">ADDRESS:</p>
                                </div> <!-- end col-sm -->
                                <div class="col-9 col-sm-8">
                                    <p>No 24 Lane 33B Pham Ngu Lao str, Hoan kiem Dist Hanoi, Vietnam</p>
                                </div> <!-- end col-sm -->
                            </div>
                        </li>
                        <li>
                            <div class="row phone">
                                <div class="col-3 col-sm-2 col-md-2">
                                    <p class="text-brown">HOTLINE:</p>
                                </div> <!-- end col-sm -->
                                <div class="col-9 col-sm-4 col-md-4">
                                    <p>+84 93 594 6688</p>
                                </div> <!-- end col-sm -->
                                <div class="col-12 col-sm-6 col-md-6 tel">
                                    <div class="row">
                                        <div class="col-3 col-sm-2 text-brown">TEL:</div>
                                        <div class="col-9 col-sm-10"><p>+84 24 39336222</p></div>
                                    </div>
                                    <!-- <p><span class="text-brown pr-2">TEL:</span> +84 24 39336222</p> -->
                                </div> <!-- end col-sm -->
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-3 col-sm-2">
                                    <p class="text-brown">EMAIL:</p>
                                </div> <!-- end col-sm -->
                                <div class="col-9 col-sm-10">
                                    <p>info@luxurycruisehalong.com</p>
                                </div> <!-- end col-sm -->
                            </div>
                        </li>
                        <li>
                            <a href="#" title="Facebook"><img src="{{ url('frontend/img/icon/facebook.png') }}" alt="" class="img-fluid icon"></a>
                            <a href="#" title="Skype"><img src="{{ url('frontend/img/icon/skype.png') }}" alt="" class="img-fluid icon"></a>
                            <a href="#" title="Instagram"><img src="{{ url('frontend/img/icon/instagram.png') }}" alt="" class="img-fluid icon"></a>
                            <a href="#" title="Youtube"><img src="{{ url('frontend/img/icon/youtube.png') }}" alt="" class="img-fluid icon"></a>
                        </li>
                    </ul>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div> <!-- end container -->
    </footer>
    <section class="copy">
        <div class="container">
            <p class="text-center">Luxury Cruise Halong Managed by Asia Eyes Travel Group International Tour Operator License No: 01682/TCDL.</p>
        </div>
    </section> <!-- end copy -->

    @stack('before-scripts')

    <script src="{{ url('frontend/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('frontend/js/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>

    @stack('middle-scripts')

    <script src="{{ url('frontend/js/datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('frontend/js/custom.js') }}" type="text/javascript"></script>

    @stack('after-scripts')

</body>
</html>