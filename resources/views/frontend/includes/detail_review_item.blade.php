
@if ($listReviews && $listReviews->count() > 0)
@foreach ($listReviews as $review)

    <div class="row list-review__item">
        <div class="col-md-4 col-lg-3 summary-review">
            <p class="score clearfix"><strong class="float-left">{{ $review->traveler_rating }}/10</strong> <span class="float-left">Exceptional</span></p>
            <p class="summary-item clearfix"><img src="{{ url('frontend/img/icon/flag.png') }}" alt=""> <span><small>Timothy</small> from {{ $review->country }}</span></p>
            <p class="summary-item clearfix"><img src="{{ url('frontend/img/icon/travel_type.png') }}" alt=""> <span> {{ $review->traveler_type }}</span></p>
            <p class="summary-item clearfix"><img src="{{ url('frontend/img/icon/time.png') }}" alt=""> <span> {{ DateTime::createFromFormat('Y-m-d h:i:s', $review->time_send)->format('d M, Y h:i a') }}</span></p>
        </div> <!-- end summary-review -->
        <div class="col-md-8 col-lg-9 content-review">
            <h3 class="title-review">"{{ $review->title }}"</h3>
            <p>{{ $review->message }}</p>
        </div> <!-- end content-review -->
    </div> <!-- end list-review__item -->

@endforeach <!-- end list -->

@if ($listReviews->lastPage() > 1)

<div class="page">
    <ul class="pagination justify-content-center">
        <li class="page-item {{ $listReviews->currentPage() == 1 ? 'disabled' : '' }}">
            <a class="page-link page-prev" href="{{ $listReviews->previousPageUrl() }}"><img src="{{ url('frontend/img/homepage/arrow_forward_left.png') }}" alt=""></a>
        </li>
        <li class="page-item pages">Page {{ $listReviews->currentPage() }} of {{ $listReviews->lastPage() }}</li>
        <li class="page-item {{ $listReviews->currentPage() == $listReviews->lastPage() ? 'disabled' : '' }}">
            <a class="page-link page-next" href="{{ $listReviews->nextPageUrl() }}"><img src="{{ url('frontend/img/homepage/arrow_forward_right.png') }}" alt=""></a>
        </li>
    </ul>
</div>

@endif

@endif