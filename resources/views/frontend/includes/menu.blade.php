<div id="menu" class="menu float-panel" data-top="0" data-scroll="300">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg">
            <!-- Brand -->
            <a class="navbar-brand logo" href="index.html" title="Adamo Digital">
                <img src="img/adamo_digital_logo_white.png" alt="" class="img-fluid">
            </a>
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="about_us.html" title="About Us"><span>about us</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="our_service.html" title="Services"><span>services</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="expertise.html" title="Expertise"><span>expertise</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="portfolio.html" title="Portfolio"><span>portfolio</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="careers.html" title="Careers"><span>careers</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" title="Blog"><span>blog</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-contact rounded-pill" href="contact_us.html" title="Contact Us">CONTACT US</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>