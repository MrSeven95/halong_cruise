
@if ($listCabins && $listCabins->count() > 0)
@foreach ($listCabins as $cabin)

<div class="cabin-list">
    <div class="row">
        <div class="col-lg-3">
            <img src="{{ $cabin->image_thumbnail ? $cabin->image_thumbnail : url('/images/no-image-found.jpg') }}" alt="" class="img-fluid">
        </div> <!-- end col-3 -->
        <div class="col-lg-9 cabin-content">
            <h3 class="cabin-text__title">{{ $cabin->name }}</h3>
            <div class="cabin-info">
                <div class="row ">
                    <div class="col-md-6">
                        <div class="cabin-icon float-left">
                            <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                        </div>
                        <p class="float-left cabin-info__paragraph">{{ $cabin->cabin_size }} m2</p>
                    </div>
                    <div class="col-md-6">
                        <div class="cabin-icon float-left">
                            <img src="{{ url('frontend/img/icon/cabin_passenger.png') }}" class="" alt="">
                        </div>
                        <p class="float-left cabin-info__paragraph">{{ $cabin->capacity_adult }} adults, {{ $cabin->capacity_child }} kid under 9 years old</p>
                    </div>

                    <div class="col-md-6">
                        <div class="cabin-icon float-left">
                            <img src="{{ url('frontend/img/icon/cabin_bed.png') }}" class="" alt="">
                        </div>
                        <p class="float-left cabin-info__paragraph">{{ $cabin->bedType->name }}</p>
                    </div>

                    @if ($cabin->cabin_facilities_set_of_bed_sheets == 1)
                    <div class="col-md-6">
                        <div class="cabin-icon float-left">
                            <img src="{{ url('frontend/img/icon/cabin_bed.png') }}" class="" alt="">
                        </div>
                        <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_set_of_bed_sheets') }}</p>
                    </div>
                    @endif
                    @if ($cabin->cabin_facilities_hairdryer == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_hairdryer') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_tivi == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_tivi') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_air_conditional == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_air_conditional') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_balcony == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_bal.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_balcony') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_wifi == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_wifi') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_slippers == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_slippers') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_wardrobe == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_wardrobe') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_sofa == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_sofa') }}</p>
                        </div>
                    @endif
                    @if ($cabin->cabin_facilities_smoking == 1)
                        <div class="col-md-6">
                            <div class="cabin-icon float-left">
                                <img src="{{ url('frontend/img/icon/cabin_dt.png') }}" class="" alt="">
                            </div>
                            <p class="float-left cabin-info__paragraph">{{ __('backend/cabin.cabin_facilities_smoking') }}</p>
                        </div>
                    @endif
                </div>
            </div> <!-- end cabin-info -->
            <div class="cabin-free">
                <p class="flase-sale">

                    @foreach ($cruise->cruiseFreeService as $freeService)

                        <span>{{ $freeService->freeService->name }}</span><br>

                    @endforeach
                </p>
            </div> <!-- end cabin-free -->
            <div class="cabin-price">
                <div class="row">
                    <div class="col-md-6">
                        <p class="clearfix">

                        @if ($cabin->cabinPrice->count() > 0)
                            @php $minPrice = floor($cabin->cruise->cabinPrice->min('price')); @endphp

                            @if ($cabin->cruise->couponCruise->count() > 0)
                                @php $cruiseCouponMin = $cabin->cruise->couponCruise->min('discount'); @endphp

                                @if ($cruiseCouponMin != 0)

                                    <span class="float-left text-right pr-2">
                                        <small class="usd">USD</small><br>
                                        <s>${{  floor($minPrice/ (100 - $cruiseCouponMin) * 100) }}</s>
                                    </span>

                                @endif
                            @endif

                            <span class="float-left">
                                <strong>${{ $minPrice }}</strong>
                                <small>/ 1 cabin</small>
                            </span>

                        @else

                            <span class="float-left">
                                <strong>$0</strong>
                            </span>

                        @endif

                        </p>
                        <p class="charges">All charges included</p>
                    </div><!--  end col-6 -->
                    <div class="col-md-6 text-right">
                        <a href="book.html?cabin_id={{ $cabin->id }}" title="BOOK NOW" class="btn btn-book">BOOK NOW</a>
                    </div> <!-- end col-6 -->
                </div><!--  end row -->
            </div> <!-- end cabin-price -->
        </div> <!-- end col-9 -->
    </div> <!-- end row -->
</div> <!-- end cabin-list -->

@endforeach

@else

    <div class="cabin-list">
        No cabin available now.
    </div>

@endif
