
@if ($listCruises && $listCruises->count() > 0)
@foreach ($listCruises as $cruise)

@php $cruiseLink = $cruise->slug .'.html'; @endphp

<div class="luxury-list__item clearfix">
    <div class="luxury-list__item-image">

        <a href="{{ $cruiseLink }}" title="{{ isset($cruise->name) ? $cruise->name : 'empty' }}"><img src="{{ $cruise->image_thumbnail ? $cruise->image_thumbnail : url('/images/no-image-found.jpg') }}" class="img-fluid" alt="{{ $cruise->name }}"></a>

    </div> <!-- end luxury-list__item-image -->
    <div class="luxury-list__item-content cruise clearfix">
        <div class="luxury-list__item-content__info">
            <h2 class="cruise-title"><a href="{{ $cruiseLink }}" title="">{{ isset($cruise->name) ? $cruise->name : 'empty' }}</a></h2>
            <div class="clearfix mb-2">
                <span class="star float-left">
                    @for ($i=1; $i<=$cruise->star; $i++)

                        <i class="fa fa-star" aria-hidden="true"></i>

                    @endfor
                    @for ($i=$cruise->star + 1; $i<=5; $i++)

                        <i class="fa fa-star-o" aria-hidden="true"></i>

                    @endfor
                </span>
                <span class="rate float-left">
                    <span class="label-rate">{{ $cruise->review->count() > 0 ? $cruise->review->average('traveler_rating') : '9' }}</span>

                    Exceptional <a href="#">{{ $cruise->review->count() }} reviews</a>
                </span>
            </div> <!-- end rate -->

            @php $cruiseCouponMin = 0; @endphp
            @if ($cruise->couponCruise->count() > 0)
                @php $cruiseCouponMin = $cruise->couponCruise->min('discount'); @endphp

                <div class="off my-3 mt-0">
                    <span>UP TO {{ $cruiseCouponMin }}% OFF</span>
                </div> <!-- end off -->

            @endif

            <p class="place">Visiting:
                <span class="place-item">

                    @php $countCruiseCruiseArea = 0; @endphp
                    @php $maxCountCruiseCruiseArea = 30; @endphp
                    @foreach ($cruise->cruiseCruiseArea as $cruiseCruiseArea)
                        @isset ($cruiseCruiseArea->cruiseArea->name)

                        @php $areaName = $cruiseCruiseArea->cruiseArea->name @endphp

                        {{ $areaName }}@if (!$loop->last), @endif

                        @php $countCruiseCruiseArea += strlen($areaName); @endphp
                        @if (!$loop->last && $countCruiseCruiseArea >= $maxCountCruiseCruiseArea)
                            ...
                            @break
                        @endif

                        @endisset

                    @endforeach

                    </span>
            </p>
            <p class="free">SPECIAL OFFER:</p>
            <p class="free">

                @foreach ($cruise->cruiseFreeService as $freeService)

                    <span>{{ $freeService->freeService->name }}</span><br>

                @endforeach
            </p>
        </div> <!-- end luxury-list__item-content__info -->
        <a href="{{ $cruiseLink }}">
            <div class="luxury-list__item-content__price">
                <div class="price">
                    <p>From USD*</p>

                    @if ($cruise->cabinPrice->count() > 0)
                        @php $minPrice = floor($cruise->cabinPrice->min('price')); @endphp

                        <p>

                            @if ($cruiseCouponMin != 0)

                                <s>${{  floor($minPrice/ (100 - $cruiseCouponMin) * 100) }}</s>
                                <span>${{ $minPrice }}</span>

                            @else
                                <span>${{ $minPrice }}</span>
                            @endif
                        </p>
                        <p>per person</p>

                    @else

                        <span>$0</span>

                    @endif

                </div> <!-- end price -->
                <div class="bg-brown"></div>
            </div> <!-- end luxury-list__item-content__price -->
        </a>
        <div class="luxury-list__item-content__link">
            <a href="{{ $cruiseLink }}" class="btn-detail"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div> <!-- end luxury-list__item-content__link -->
    </div><!-- end luxury-list__item-content -->
</div> <!-- end luxury-list__item -->

@endforeach

@if ($listCruises->lastPage() > 1)

<div class="page">
    <ul class="pagination justify-content-center">
        <li class="page-item {{ $listCruises->currentPage() == 1 ? 'disabled' : '' }}">
            <a class="page-link page-prev" href="{{ $listCruises->previousPageUrl() }}"><img src="{{ url('frontend/img/homepage/arrow_forward_left.png') }}" alt=""></a>
        </li>
        <li class="page-item pages">Page {{ $listCruises->currentPage() }} of {{ $listCruises->lastPage() }}</li>
        <li class="page-item {{ $listCruises->currentPage() == $listCruises->lastPage() }} ? 'disabled' : '' }}">
            <a class="page-link page-next" href="{{ $listCruises->nextPageUrl() }}"><img src="{{ url('frontend/img/homepage/arrow_forward_right.png') }}" alt=""></a>
        </li>
    </ul>
</div>

@endif

@endif