
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <img src="{{ url('frontend/img/logo_brown.png') }}" alt="Luxury Cruise" class="img-fluid img-footer">
                <ul class="list-anchor">
                    <li><a href="#" title="About us">About us</a></li>
                    <li><a href="#" title="FAQs">FAQs</a></li>
                    <li><a href="#" title="Contact us">Contact us</a></li>
                    <li><a href="#" title="Blog">Blog</a></li>
                </ul>
            </div> <!-- end col -->
            <div class="col-md-6 col-lg-3">
                <h3 class="text-uppercase">FIND CRUISES & SERVICES</h3>
                <ul>
                    <li><a href="#" title="Best seller Cruises">Best seller Cruises</a></li>
                    <li><a href="#" title="Luxury Halong Cruise">Luxury Halong Cruise</a></li>
                    <li><a href="#" title="Deluxe Halong Cruise">Deluxe Halong Cruise</a></li>
                    <li><a href="#" title="Transfer Services">Transfer Services</a></li>
                </ul>
            </div> <!-- end col -->
            <div class="col-12 col-lg-1"></div><!-- end col -->
            <div class="col-md-12 col-lg-5">
                <h3 class="text-uppercase">FOLLOW US</h3>
                <ul class="follow-us">
                    <li>
                        <div class="row">
                            <div class="col-3 col-sm-2">
                                <p class="text-brown">ADDRESS:</p>
                            </div> <!-- end col-sm -->
                            <div class="col-9 col-sm-8">
                                <p>No 24 Lane 33B Pham Ngu Lao str, Hoan kiem Dist Hanoi, Vietnam</p>
                            </div> <!-- end col-sm -->
                        </div>
                    </li>
                    <li>
                        <div class="row phone">
                            <div class="col-3 col-sm-2 col-md-2">
                                <p class="text-brown">HOTLINE:</p>
                            </div> <!-- end col-sm -->
                            <div class="col-9 col-sm-4 col-md-4">
                                <p>+84 93 594 6688</p>
                            </div> <!-- end col-sm -->
                            <div class="col-12 col-sm-6 col-md-6 tel">
                                <div class="row">
                                    <div class="col-3 col-sm-2 text-brown">TEL:</div>
                                    <div class="col-9 col-sm-10"><p>+84 24 39336222</p></div>
                                </div>
                                <!-- <p><span class="text-brown pr-2">TEL:</span> +84 24 39336222</p> -->
                            </div> <!-- end col-sm -->
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-3 col-sm-2">
                                <p class="text-brown">EMAIL:</p>
                            </div> <!-- end col-sm -->
                            <div class="col-9 col-sm-10">
                                <p>info@luxurycruisehalong.com</p>
                            </div> <!-- end col-sm -->
                        </div>
                    </li>
                    <li>
                        <a href="#" title="Facebook"><img src="{{ url('frontend/img/icon/facebook.png') }}" alt="" class="img-fluid icon"></a>
                        <a href="#" title="Skype"><img src="{{ url('frontend/img/icon/skype.png') }}" alt="" class="img-fluid icon"></a>
                        <a href="#" title="Instagram"><img src="{{ url('frontend/img/icon/instagram.png') }}" alt="" class="img-fluid icon"></a>
                        <a href="#" title="Youtube"><img src="{{ url('frontend/img/icon/youtube.png') }}" alt="" class="img-fluid icon"></a>
                    </li>
                </ul>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- end container -->
</footer>
<section class="copy">
    <div class="container">
        <p class="text-center">Luxury Cruise Halong Managed by Asia Eyes Travel Group International Tour Operator License No: 01682/TCDL.</p>
    </div>
</section> <!-- end copy -->
