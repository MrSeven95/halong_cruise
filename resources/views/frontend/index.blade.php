@extends('frontend.layouts.home')

@section('content')
	<header>
		<div id="menu" class="menu float-panel" data-top="0" data-scroll="300">
			<div class="container">
				<nav class="navbar navbar-expand-lg">
					<!-- Brand -->
					<a class="navbar-brand logo" href="index.html" title="Luxury Cruise">
						<img src="img/logo_white.png" alt="" class="img-fluid">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" aria-controls="collapsibleNavbar" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<!-- Navbar links -->
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="about_us.html" title="About Us"><span>ABOUT US</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="luxury.html" title="Luxury cruise"><span>LUXURY CRUISE</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="expertise.html" title="Expertise"><span>BEST SELLER</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="portfolio.html" title="Portfolio"><span>CAR</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#" title="Blog"><span>BLOG</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#" title="Blog"><span>CONTACT US</span></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<div id="carouselSlide" class="carousel slide carousel-fade" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="{{ url('frontend/img/slide/slide1.jpg') }}" class="w-100 big-img" alt="...">
					<img src="{{ url('frontend/img/slide/tablet/slide1.jpg') }}" class="w-100 medium-img" alt="...">
					<img src="{{ url('frontend/img/slide/vertical/slide1.jpg') }}" class="w-100 small-img" alt="...">
				</div>
				<div class="carousel-item">
					<img src="{{ url('frontend/img/slide/slide2.jpg') }}" class="w-100 big-img" alt="...">
					<img src="{{ url('frontend/img/slide/tablet/slide2.jpg') }}" class="w-100 medium-img" alt="...">
					<img src="{{ url('frontend/img/slide/vertical/slide2.jpg') }}" class="w-100 small-img" alt="...">
				</div>
				<div class="carousel-item">
					<img src="{{ url('frontend/img/slide/slide3.jpg') }}" class="w-100 big-img" alt="...">
					<img src="{{ url('frontend/img/slide/tablet/slide3.jpg') }}" class="w-100 medium-img" alt="...">
					<img src="{{ url('frontend/img/slide/vertical/slide3.jpg') }}" class="w-100 small-img" alt="...">
				</div>
				<div class="carousel-item">
					<img src="{{ url('frontend/img/slide/slide4.jpg') }}" class="w-100 big-img" alt="...">
					<img src="{{ url('frontend/img/slide/tablet/slide4.jpg') }}" class="w-100 medium-img" alt="...">
					<img src="{{ url('frontend/img/slide/vertical/slide4.jpg') }}" class="w-100 small-img" alt="...">
				</div>

			</div>
		</div> <!-- end carousel -->
		<div class="home-search">
			<div class="container">
				<div class="search">
					<div class="row justify-content-center">
						<div class="col-md-10 col-lg-8">
							<h1>BOOKING HA LONG CRUISE HAS NEVER BEEN EASIER</h1>
						</div>
					</div> <!-- end row -->
					<div class="form-search">
						<form action="" method="POST">
							<div class="form-row clearfix">
								<div class="group col-lg-10 col-md-12 col-sm-10">
									<div class="row">
										<div class="form-group col-lg-3 col-md-6 col-sm-3 col-12 mb-2 mb-sm-0" style="position: relative;">
											<label for="">CRUISE NAME</label>
											<input type="text" class="form-control" placeholder="Enter cruise name">
										</div>
										<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 itinerary">
											<label for="">ITINERARY</label>
											<select name="itinerary" id="itinerary" class="form-control" onchange="setCheckout()">
										    	<option value="" disabled hidden selected>PLEASE SELECT</option>
										    	<option value="1">2 Days 1 Night</option>
										    	<option value="2">3 Days 2 Nights</option>
										  	</select>
										</div>
										<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 checkout">
											<label for="">CHECKIN</label>
											<input type="text" class="form-control" name="checkin" id="checkin" data-toggle="datepicker" placeholder="ANY DATE" autocomplete="off" onchange="setCheckout()">
											<i class="fa fa-calendar icon-calendar" id="icon-calendar"></i>
										</div>
										<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 checkout">
											<label for="">CHECKOUT</label>
											<input type="text" class="form-control" name="checkout" id="checkout" placeholder="ANY DATE" readonly>
											<i class="fa fa-calendar icon-calendar"></i>
										</div>
										<div class="form-group col-lg-3 col-md-12 col-sm-3 col-12 mb-2 mb-sm-0" style="position:relative;">
											<label for="">PASSENGER</label>
											<a data-toggle="modal" data-target="#RoomAdultModal"><input type="text" id="passenger" name="passenger" value="" class="form-control" placeholder="0 Room, 0 Adult, 0 Children" ></a>
										</div>
									</div>
									
								</div>
								<div class="col-lg-2 col-md-12 col-sm-2 col-12 button-search">
									<button class="button t400 btn-block" type="submit">Search</button>
								</div>
							</div>
						</form>
					</div> <!-- end form-search -->
				</div> <!-- end search -->
			</div> <!-- end container -->
		</div> <!-- end home-search -->
	</header>
	<div class="modal fade" id="RoomAdultModal" tabindex="-1" role="dialog" aria-labelledby="RoomAdultModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Passenger</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group room pb-3">
						<label for="">Total room</label>
						<div class="input-group mb-3">
							<div class="input-group-prepend" id="desc_room">
								<span class="input-group-text">-</span>
							</div>
							<input type="text" id="total-room" name="total-room" class="form-control text-center" value="1" readonly>
							<div class="input-group-append" id="asc_room">
								<span class="input-group-text">+</span>
							</div>
						</div>
					</div> <!-- end room -->
					<div class="list-room" id="list-room"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-done">Done</button>
				</div>
			</div>
		</div>
	</div> <!-- end modal -->
	<main>
		<section class="home-slogan">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<h3>You’ll discover endless thrills for every kind of adventurer onboard any one of our cruise ships. From the daredevils to the culinary crusaders, and fun-loving families to couples on a romantic getaway.</h3>
					</div> <!-- end col-lg -->
				</div> <!-- end row -->
			</div> <!-- end container -->
		</section> <!-- end home-slogan -->
		<section class="home-best-cruise">
			<div class="container">
				<h2 class="param-title">Best Cruises 2019</h2>
				<div class="slider">
					<section id="demos">
    					<div class="large-12 columns">
      						<div class="owl-carousel owl-theme owl-loaded owl-drag">
      							<div class="owl-stage-outer">
      								<div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 3000px !important;">

										@foreach ($listCruises as $cruise)

										@php $cruiseLink = $cruise->slug .'.html'; @endphp

          								<div class="owl-item">
          									<div class="item">
          										<div class="card cruise-item">
          											<a href="{{ $cruiseLink }}" title=""><img src="{{ $cruise->image_thumbnail ? $cruise->image_thumbnail : url('/images/no-image-found.jpg') }}" class="card-img-top" alt="{{ $cruise->name }}"></a>

													@php $cruiseCouponMin = 0; @endphp
													@if ($cruise->couponCruise->count() > 0)
													@php $cruiseCouponMin = $cruise->couponCruise->min('discount'); @endphp

													<div class="sale">{{ $cruiseCouponMin }}% off</div>

          											@endif

													<div class="card-body">
          												<h3 class="card-title cruise-title"><a href="{{ $cruiseLink }}" title="">{{ $cruise->name }}</a></h3>
          												<div class="clearfix">
          													<span class="star float-left">

																@for ($i=1; $i<=$cruise->star; $i++)

          														<i class="fa fa-star" aria-hidden="true"></i>

																@endfor
																@for ($i=$cruise->star + 1; $i<=5; $i++)

																	<i class="fa fa-star-o" aria-hidden="true"></i>

																@endfor
          													</span>
          													<span class="rate float-right">
          														<span class="label-rate">{{ $cruise->review->count() > 0 ? $cruise->review->average('traveler_rating') : '9' }}</span>

          														Exceptional <a href="#">{{ $cruise->review->count() }} reviews</a>
          													</span>
          												</div>
          												<div class="clearfix mt-3">
          													<div class="place float-left">
          														<p>Visiting:</p>
																<div>
																	@php $countCruiseCruiseArea = 0; @endphp
																	@php $maxCountCruiseCruiseArea = 30; @endphp
																	@foreach ($cruise->cruiseCruiseArea as $cruiseCruiseArea)
																		@php $areaName = $cruiseCruiseArea->cruiseArea->name @endphp

																		<a href="#" title="">{{ $areaName }}</a>@if (!$loop->last), @endif

																		@php $countCruiseCruiseArea += strlen($areaName); @endphp
																		@if (!$loop->last && $countCruiseCruiseArea >= $maxCountCruiseCruiseArea)
																			...
																			@break
																		@endif

																	@endforeach
																</div>
          													</div>
          													<div class="price float-right text-right">
          														<p>From USD*</p>

																@if ($cruise->cabinPrice->count() > 0)
																@php $minPrice = floor($cruise->cabinPrice->min('price')); @endphp

																<p>

																	@if ($cruiseCouponMin != 0)

																	<s>${{  floor($minPrice/ (100 - $cruiseCouponMin) * 100) }}</s>
																	<span>${{ $minPrice }}</span>

																	@else
																		<span>${{ $minPrice }}</span>
																	@endif
																</p>
																<p>per person</p>

																@else

																	<span>$0</span>

																@endif
          													</div>
          												</div>
          											</div>
          											<div class="card-footer">
          												<p class="sub-title">SPECIAL OFFER:</p>

														@foreach ($cruise->cruiseFreeService as $freeService)

          													<p>{{ $freeService->freeService->name }}</p>

														@endforeach

          											</div>
          										</div>
          									</div>
          								</div>

										@endforeach

										@if (count($listCruises) == 0)
											No data.
										@endif

      								</div>
      							</div>
      						</div>
    					</div>
    				</section>
				</div>
				<div class="text-center button-all">
					<a href="{{ route('frontend.luxury') }}" title="">VIEW ALL HALONG CRUISES</a>
				</div>
			</div>
		</section> <!-- end home-best-cruise -->
		<section class="home-why">
			<div class="container">
				<div class="group-title">
					<h2 class="title-head">WHY BOOK WITH US?</h2>
					<h3 class="sub-title">Luxury Halong Cruise</h3>
				</div>
			</div> <!-- end container -->
			<div class="container home-why__panel">
				<div class="home-why__content">
					<div class="row justify-content-center home-why__content-paragraph">
						<div class="col-12 col-lg-10">
							<h4><a href="#" title="">Luxurycruisehalong.com</a> owned by Asia Eyes Travel. We offer the largest collection of best Halong Bay Cruises and a vast of Halong bay tours to any Halong destinations in Halong Bay, Lan Ha Bay and Bai Tu Long Bay or Cat Ba Bay island at the lowest pride from Deluxe Cruises to Luxury Cruises. You can also find a various Halong Cruises & stay package options of Halong cruise combine with Hanoi hotels or Halong Hotels & Halong Cruises. Let’s us know your desire or expectation for your holiday in Vietnam, we always beside you to be your assistance.</h4>
						</div>
					</div>
					<div class="row home-why__content-list">
						<div class="col-md-6 col-lg-3 align-self-center mb-3">
							<div class="home-why__content-item clearfix">
								<div class="home-why__content-icon">
									<img src="{{ url('frontend/img/homepage/icon_cart.png') }}" alt="" class="img-fluid">
								</div>
								<h3 class="home-why__content-text">TRUE ONLINE BOOKING</h3>
							</div>
						</div> <!-- end col-lg-3 -->
						<div class="col-md-6 col-lg-3 align-self-center mb-3">
							<div class="home-why__content-item clearfix">
								<div class="home-why__content-icon">
									<img src="{{ url('frontend/img/homepage/icon_expert.png') }}" alt="" class="img-fluid">
								</div>
								<h3 class="home-why__content-text">CRUISE EXPERT SUPPORT</h3>
							</div>
						</div> <!-- end col-lg-3 -->
						<div class="col-md-6 col-lg-3 align-self-center mb-3">
							<div class="home-why__content-item clearfix">
								<div class="home-why__content-icon">
									<img src="{{ url('frontend/img/homepage/icon_book.png') }}" alt="" class="img-fluid">
								</div>
								<h3 class="home-why__content-text">BOOK & DEPOSIT 30% ONLY</h3>
							</div>
						</div> <!-- end col-lg-3 -->
						<div class="col-md-6 col-lg-3 align-self-center mb-3">
							<div class="home-why__content-item clearfix">
								<div class="home-why__content-icon">
									<img src="{{ url('frontend/img/homepage/icon_protect.png') }}" alt="" class="img-fluid">
								</div>
								<h3 class="home-why__content-text">NO ANY HIDDEN FEES</h3>
							</div>
						</div> <!-- end col-lg-3 -->
					</div> <!-- end home-why__content-list -->
				</div> <!-- end home-why__content -->
			</div> <!-- end home-why__panel -->
		</section> <!-- end home-why -->
		<section class="home-excelente">
			<div class="container">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ul class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
						<li data-target="#myCarousel" data-slide-to="3"></li>
					</ul>
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="row">
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row">
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row">
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row">
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
								<div class="col-12 col-md-6 col-lg-6 clearfix">
									<div class="carousel-item__image">
										<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid">
										<ul class="vote nav justify-content-center">
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
											<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
										</ul>
										<h4 class="title-lead__sub"><a href="#" title="">Regina R</a></h4>
									</div>
									<div class="carousel-item__content">
										<h3 class="title-lead">Excelente experiencia</h3>
										<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
										<p><a href="#" class="read-more">READ MORE</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
						<!-- <span class="carousel-control-prev-icon"></span> -->
						<img src="{{ url('frontend/img/homepage/arrow_forward_left.png') }}" width="20" alt="">
					</a>
					<a class="carousel-control-next" href="#myCarousel" data-slide="next">
						<img src="{{ url('frontend/img/homepage/arrow_forward_right.png') }}" width="20" alt="">
						<!-- <span class="carousel-control-next-icon"></span> -->
					</a>
				</div>
			</div>
		</section> <!-- end home-exelente -->
		<section class="home-blog">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 align-self-center home-blog__go">
						<h3>WHAT TO KNOW</h3>
						<h2>BEFORE YOU GO</h2>
						<p>October to December (Fall) is the best time to visit Halong Bay and peak season thanks to the pleasant weather and low chance of rain, though you can expect double the crowds. While June to September (Summer) is the low season and has a higher chance of storms and typhoon.</p>
						<p>However, with some planning, you can have an amazing Halong tour with half of the normal price when booking cruise in the low season.</p>
					</div> <!-- end col-lg -->
					<div class="col-lg-1"></div>
					<div class="col-lg-6 home-blog__list">
						<div class="row home-blog__list-item">
							<div class="col-sm-4 home-blog__list-img">
								<img src="{{ url('frontend/img/blog/blog1.png') }}" alt="" class="img-fluid">
							</div> <!-- end col-sm -->
							<div class="col-sm-8">
								<h3 class="title-lead"><a href="#" title="">Create your perfect vacation day</a></h3>
								<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
								<p><a href="#" class="read-more">READ MORE</a></p>
							</div> <!-- end col-sm -->
						</div> <!-- end row sm -->
						<div class="row home-blog__list-item">
							<div class="col-sm-4 home-blog__list-img">
								<img src="{{ url('frontend/img/blog/blog2.png') }}" alt="" class="img-fluid">
							</div> <!-- end col-sm -->
							<div class="col-sm-8">
								<h3 class="title-lead"><a href="#" title="">Create your perfect vacation day</a></h3>
								<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
								<p><a href="#" class="read-more">READ MORE</a></p>
							</div> <!-- end col-sm -->
						</div> <!-- end row sm -->
						<div class="row home-blog__list-item">
							<div class="col-sm-4 home-blog__list-img">
								<img src="{{ url('frontend/img/blog/blog3.png') }}" alt="" class="img-fluid">
							</div> <!-- end col-sm -->
							<div class="col-sm-8">
								<h3 class="title-lead"><a href="#" title="">Create your perfect vacation day</a></h3>
								<p class="text-content">We spent two nights in Halong Bay and had a great time. The organization was perfect. The meals abundant and varied and the activities good fun.</p>
								<p><a href="#" class="read-more">READ MORE</a></p>
							</div> <!-- end col-sm -->
						</div> <!-- end row sm -->
						
					</div> <!-- end col-lg -->
				</div> <!-- end row lg -->
			</div> <!-- end container -->
		</section> <!-- end home-blog -->
	</main>

@endsection

@push('middle-scripts')

	<script src="{{ url('frontend/js/owl.carousel.js') }}" type="text/javascript"></script>
	<script src="{{ url('frontend/js/float-panel.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {
		  $('.owl-carousel').owlCarousel({
			loop: true,
			margin: 10,
			responsiveClass: true,
			navText: ["<img src='{{ url('frontend/img/homepage/arrow_forward_left.png') }}'>","<img src='{{ url('frontend/img/homepage/arrow_forward_right.png') }}'>"],
			responsive: {
			  0: {
				items: 1,
				nav: true
			  },
			  600: {
				items: 2,
				nav: true
			  },
			  1000: {
				items: 3,
				nav: true,
				loop: true,
				margin: 30,
				// stagePadding: 88,
			  }
			}
		  });
		});
    </script>

@endpush