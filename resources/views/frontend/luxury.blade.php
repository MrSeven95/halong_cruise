@extends('frontend.layouts.home')

@section('title', 'Luxury')

@push('after-styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/css/bootstrap-slider.min.css">

@endpush

@section('content')

	<header>
		<div id="menu" class="menu float-panel" data-top="0" data-scroll="300">
			<div class="container">
				<nav class="navbar navbar-expand-lg">
					<!-- Brand -->
					<a class="navbar-brand logo" href="index.html" title="Luxury Cruise">
						<img src="img/logo_white.png" alt="" class="img-fluid">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" aria-controls="collapsibleNavbar" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<!-- Navbar links -->
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="about_us.html" title="About Us"><span>ABOUT US</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="luxury.html" title="Luxury Cruise"><span>LUXURY CRUISE</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="expertise.html" title="Expertise"><span>BEST SELLER</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="portfolio.html" title="Portfolio"><span>CAR</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#" title="Blog"><span>BLOG</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#" title="Blog"><span>CONTACT US</span></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<section class="banner">
			<img src="{{ url('frontend/img/banner/destop/luxury.jpg') }}" alt="" class="img-fluid destop-img">
			<img src="{{ url('frontend/img/banner/tablet/luxury.jpg') }}" alt="" class="img-fluid tablet-img">
			<img src="{{ url('frontend/img/banner/mobile/luxury.jpg') }}" alt="" class="img-fluid mobile-img">
		</section>
		<div class="home-search">
			<div class="container">
				<div class="search">
					<div class="row justify-content-center">
						<div class="col-md-10 col-lg-8">
							<h1>EXPLORE HALONG BAY</h1>
						</div>
					</div> <!-- end row -->
					<div class="form-search">
						<form action="" method="POST">
							<div class="form-row clearfix">
								<div class="group col-lg-10 col-md-12 col-sm-10">
									<div class="row">
										<div class="form-group col-lg-3 col-md-6 col-sm-3 col-12 mb-2 mb-sm-0" style="position: relative;">
											<label for="">CRUISE NAME</label>
											<input type="text" class="form-control" placeholder="Enter cruise name">
										</div>
										<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 itinerary">
											<label for="">ITINERARY</label>
											<select name="itinerary" id="itinerary" class="form-control" onchange="setCheckout()">
										    	<option value="" disabled hidden selected>PLEASE SELECT</option>
										    	<option value="1">2 Days 1 Night</option>
										    	<option value="2">3 Days 2 Nights</option>
										  	</select>
										</div>
										<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 checkout">
											<label for="">CHECKIN</label>
											<input type="text" class="form-control" name="checkin" id="checkin" data-toggle="datepicker" placeholder="ANY DATE" autocomplete="off" onchange="setCheckout()">
											<i class="fa fa-calendar icon-calendar" id="icon-calendar"></i>
										</div>
										<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 checkout">
											<label for="">CHECKOUT</label>
											<input type="text" class="form-control" name="checkout" id="checkout" placeholder="ANY DATE" readonly>
											<i class="fa fa-calendar icon-calendar"></i>
										</div>
										<div class="form-group col-lg-3 col-md-12 col-sm-3 col-12 mb-2 mb-sm-0" style="position:relative;">
											<label for="">PASSENGER</label>
											<a data-toggle="modal" data-target="#RoomAdultModal"><input type="text" id="passenger" name="passenger" value="" class="form-control" placeholder="0 Room, 0 Adult, 0 Children" ></a>
										</div>
									</div>
									
								</div>
								<div class="col-lg-2 col-md-12 col-sm-2 col-12 button-search">
									<button class="button t400 btn-block" type="submit">Search</button>
								</div>
							</div>
						</form>
					</div> <!-- end form-search -->
				</div> <!-- end search -->
			</div> <!-- end container -->
		</div> <!-- end home-search -->
	</header>
	<div class="modal fade" id="RoomAdultModal" tabindex="-1" role="dialog" aria-labelledby="RoomAdultModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Passenger</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group room pb-3">
						<label for="">Total room</label>
						<div class="input-group mb-3">
							<div class="input-group-prepend" id="desc_room">
								<span class="input-group-text">-</span>
							</div>
							<input type="text" id="total-room" name="total-room" class="form-control text-center" value="1" readonly>
							<div class="input-group-append" id="asc_room">
								<span class="input-group-text">+</span>
							</div>
						</div>
					</div> <!-- end room -->
					<div class="list-room" id="list-room"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-done">Done</button>
				</div>
			</div>
		</div>
	</div> <!-- end modal -->
	<main>
		<section class="luxury">
			<section class="luxury-filter">
				<div class="container">
					<ul class="nav">
						<li><p class="title-filter">filter by:</p></li>
						<li>
							<div class="filter-field area">
								<span class="filter-label">area</span>
								<span class="field" id="area-field">please select</span>
								<i class="fa fa-chevron-down icon-down" aria-hidden="true"></i>
							</div>
							<div class="filter-area" id="filter-area">
								<ul>

									@foreach ($listAreas as $area)

									<li class="clearfix">
										<label for="area{{ $area->id }}">
											<div class="squaredThree float-left">
												<input type="checkbox" value="{{ $area->id }}" id="area{{ $area->id }}" name="area[]">
												<span></span>
											</div>
											<p class="float-left" for="squaredThree1">{{ $area->name }}</p>
										</label>
									</li>

									@endforeach

								</ul>
							</div>
						</li>
						<li>
							<div class="filter-field star">
								<span class="filter-label">stars</span>
								<span class="field" id="star-field">
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
								</span>
								<i class="fa fa-chevron-down icon-down" aria-hidden="true"></i>
							</div>
							<div class="filter-area" id="filter-star">
								<ul>
									<li class="clearfix">
										<label for="star5">
											<div class="squaredThree float-left">
												<input type="checkbox" value="5" id="star5" name="star[]">
												<span></span>
											</div>
											<p class="float-left">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
											</p>
										</label>
									</li>
									<li class="clearfix">
										<label for="star4">
											<div class="squaredThree float-left">
												<input type="checkbox" value="4" id="star4" name="star[]">
												<span></span>
											</div>
											<p class="float-left">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</p>
										</label>
									</li>
									<li class="clearfix">
										<label for="star3">
											<div class="squaredThree float-left">
												<input type="checkbox" value="3" id="star3" name="star[]">
												<span></span>
											</div>
											<p class="float-left">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</p>
										</label>	
									</li>
									<li class="clearfix">
										<label for="star2">
											<div class="squaredThree float-left">
												<input type="checkbox" value="2" id="star2" name="star[]">
												<span></span>
											</div>
											<p class="float-left">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</p>
										</label>
									</li>
									<li class="clearfix">
										<label for="star1">
											<div class="squaredThree float-left">
												<input type="checkbox" value="1" id="star1" name="star[]">
												<span></span>
											</div>
											<p class="float-left">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</p>
										</label>
									</li>
									<li class="clearfix">
										<label for="star0">
											<div class="squaredThree float-left">
												<input type="checkbox" value="0" id="star0" name="star[]">
												<span></span>
											</div>
											<p class="float-left">
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</p>
										</label>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="filter-field price-range">
								<span class="filter-label">PRICE RANGE</span>
								<span class="field" id="price-field">please select</span>
								<i class="fa fa-chevron-down icon-down" aria-hidden="true"></i>
							</div>
							<div class="filter-area" id="filter-price">
								<h5>Cruise price</h5>
								<div class="slider-wrapper slider-warning slider-strips slider-ghost">
									<input class="input-range" type="text" data-slider-step="1" data-slider-value="{{ $minPrice }}, {{ $maxPrice }}" data-slider-min="{{ $minPrice }}" data-slider-max="{{ $maxPrice }}" data-slider-range="true" data-slider-tooltip_split="true" />
								</div>
								<div class="show-range clearfix mt-3">
									<div class="min-price float-left w-50">
										<label for="">Min price</label><br>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">$</span>
											</div>
											<input type="text" id="min-price" readonly value="" class="form-control">
										</div>
									</div>
									<div class="min-price float-left w-50">
										<label for="">Max price</label><br>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">$</span>
											</div>
											<input type="text" id="max-price" readonly value="" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section> <!-- end luxury-filter -->
			<section class="luxury-list">
				<div class="container" id="list_contents">

					@include('frontend.includes.luxury_cruise_item', ['listCruises' => $listCruises])

				</div> <!-- end container -->
			</section> <!-- end luxury-list -->
		</section> <!-- end luxury -->	
	</main>

@endsection


@push('middle-scripts')

	<script src="{{ url('frontend/js/float-panel.js') }}" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/bootstrap-slider.min.js"></script>

@endpush

@push('after-scripts')

	<script>
		var dataFilterArea = [];
		var dataFilterStar = [];
		var dataFilterPriceMin = 0;
		var dataFilterPriceMin = 0;
		var dataFilterPriceMax = 0;

		$(document).ready(function (e) {
			$('#filter-area input[type=checkbox]').change(function (e) {
				dataFilterArea = [];

				$.each($('#filter-area input:checked'), function (i, cbx) {
					dataFilterArea.push($(cbx).val());
				});

				doFilter();
			});

			$('#filter-star input[type=checkbox]').change(function (e) {
				dataFilterStar = [];

				$.each($('#filter-star input:checked'), function (i, cbx) {
					dataFilterStar.push($(cbx).val());
				});

				doFilter();
			});

			$('.input-range').change(function (e) {
				price = $(this).val();
				price = price.split(',');
				dataFilterPriceMin = price[0];
				dataFilterPriceMax = price[1];

                doFilter();
			});

			function doFilter() {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				let url = '{{ route('frontend.luxury') }}?ajax';

				if (dataFilterArea || dataFilterStar || dataFilterPriceMin != 0 || dataFilterPriceMax != 0) {
					$.ajax({
						url: url,
						type: 'GET',
						dataType: 'json',
						data: {
							dataFilterArea: dataFilterArea,
							dataFilterStar: dataFilterStar,
							dataFilterPriceMin: dataFilterPriceMin,
							dataFilterPriceMax: dataFilterPriceMax
						},
						success: function (data) {
							if (data.status === 'success') {
								$('#list_contents').html(data.html);
								console.log(data);
							} else {
								alert('error');
								console.log(data);
							}
						},
						error: function (data) {
							console.log(data);
						}
					});
				}
			}
		});
	</script>

@endpush