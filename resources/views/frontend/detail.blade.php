@extends('frontend.layouts.home')

@section('title', $cruise->name)

@push('after-styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/css/bootstrap-slider.min.css">
    <style>
        ul.card-list ul {
            padding-left: 0;
        }
        ul.card-list li:before {
            content: "- ";
        }
    </style>

@endpush

@section('content')

    <header>
        <div class="menu bg-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <!-- Brand -->
                    <a class="navbar-brand logo" href="{{ route('frontend.home') }}" title="{{ $cruise->name }}">
                        <img src="{{ url('frontend/img/logo_brown.png') }}" alt="" class="img-fluid">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" aria-controls="collapsibleNavbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- Navbar links -->
                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="about_us.html" title="About Us"><span>ABOUT US</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="luxury.html" title="Luxury Cruise"><span>LUXURY CRUISE</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="expertise.html" title="Expertise"><span>BEST SELLER</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="portfolio.html" title="Portfolio"><span>CAR</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" title="Blog"><span>BLOG</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" title="Blog"><span>CONTACT US</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div> <!-- end menu -->
    </header>
	<div class="modal fade" id="RoomAdultModal" tabindex="-1" role="dialog" aria-labelledby="RoomAdultModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Passenger</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group room pb-3">
						<label for="">Total room</label>
						<div class="input-group mb-3">
							<div class="input-group-prepend" id="desc_room">
								<span class="input-group-text">-</span>
							</div>
							<input type="text" id="total-room" name="total-room" class="form-control text-center" value="1" readonly>
							<div class="input-group-append" id="asc_room">
								<span class="input-group-text">+</span>
							</div>
						</div>
					</div> <!-- end room -->
					<div class="list-room" id="list-room"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-done">Done</button>
				</div>
			</div>
		</div>
	</div> <!-- end modal -->
	<main class="detail-page">
		<section class="rss">
			<div class="container">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('frontend.home') }}"><i class="fa fa-home"></i> Home</a></li>
						<li class="breadcrumb-item"><a href="{{ route('frontend.luxury') }}">Luxury cruise</a></li>
						<li class="breadcrumb-item active" aria-current="page">{{ $cruise->name }}</li>
					</ol>
				</nav>
			</div> <!-- end container -->
		</section> <!-- end breadcrumb -->
		<section class="cruise-detail">
			<div class="container">
				<div class="row detail">
					<div class="col-12 col-md-4 col-lg-4">
						<h1 class="detail-title">{{ $cruise->name }}</h1>
						<p class="star">
							@for ($i=1; $i<=$cruise->star; $i++)

								<i class="fa fa-star" aria-hidden="true"></i>

							@endfor
							@for ($i=$cruise->star + 1; $i<=5; $i++)

								<i class="fa fa-star-o" aria-hidden="true"></i>

							@endfor
						</p>
						<p class="rate">
							<span class="label-rate">{{ $cruise->review->count() > 0 ? $cruise->review->average('traveler_rating') : '9' }}</span>
							Exceptional
						</p>
						<p class="reviews">from {{ $cruise->review->count() }} reviews</p>
					</div>
					<div class="col-6 col-md-4 col-lg-4">
						<h2 class="tripadvisor">Tripadvisor</h2>
						<div class="tripadvisor-item clearfix">
							<img src="{{ url('frontend/img/homepage/icon_excelente.png') }}" alt="" class="img-fluid float-left">
							<ul class="vote nav justify-content-center float-left">
								<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
								<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
								<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
								<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
								<li class="vote-item"><img src="{{ url('frontend/img/homepage/vote_check.png') }}" width="24" alt="" class="img-fluid"></li>
							</ul>
						</div>
						<p class="reviews">from {{ $cruise->review->count() }} reviews</p>
					</div>
					<div class="col-6 col-md-4 col-lg-4 price">
						<p class="from">From USD per person</p>
						<p>

						@if ($cruise->cabinPrice->count() > 0)
							@php $cruiseCouponMin = 0; @endphp
							@if ($cruise->couponCruise->count() > 0)
								@php $cruiseCouponMin = $cruise->couponCruise->min('discount'); @endphp
								@php $minPrice = floor($cruise->cabinPrice->min('price')); @endphp

								<s>${{  floor($minPrice/ (100 - $cruiseCouponMin) * 100) }}</s>

							@endif

								<span class="price-real">${{ $minPrice }}</span>
							</p>

						@else

							<span class="price-real">$0</span>

						@endif
						<p class="flase-sale">FLASH SALE:
						<p class="flase-sale">

							@foreach ($cruise->cruiseFreeService as $freeService)

								<span>{{ $freeService->freeService->name }}</span><br>

							@endforeach
						</p>
                        </p>
					</div>
				</div> <!-- end row -->
					@if ($cruise->mediaCruise->count() > 0)
						<div class="row album">

						@if ($cruise->mediaCruise->count() >= 4)

						<div class="col-lg-6 col-md-6 mb-3">
							<div class="album-thumb">
								<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[0]->url }}" class="img-fluid" alt="">
							</div>
						</div>
						<div class="col-lg-3 col-md-3 mb-3">
							<div class="img-thumb img-up">
								<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[1]->url }}" class="img-fluid" alt="">
							</div>
							<div class="img-thumb">
								<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[2]->url }}" class="img-fluid" alt="">
							</div>
						</div>
						<div class="col-lg-3 col-md-3">
							<div class="album-thumb">
								<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[3]->url }}" class="img-fluid" alt="">
							</div>
						</div>

						</div> <!-- end row -->
						<div class="view-all">
							<a href="#" title="view all photos" class="btn btn-view-all">view all photos</a>


						@elseif ($cruise->mediaCruise->count() == 3)

							<div class="col-lg-6 col-md-6">
								<div class="album-thumb">
									<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[0]->url }}" class="img-fluid" alt="">
								</div>
							</div>
							<div class="col-lg-6 col-md-6 mb-3">
								<div class="img-thumb img-up">
									<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[1]->url }}" class="img-fluid" alt="">
								</div>
								<div class="img-thumb">
									<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[2]->url }}" class="img-fluid" alt="">
								</div>
							</div>

						@elseif ($cruise->mediaCruise->count() == 2)

							<div class="col-lg-6 col-md-6 mb-3">
								<div class="album-thumb">
									<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[0]->url }}" class="img-fluid" alt="">
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="album-thumb">
									<img src="http://54.169.75.223:8780{{ $cruise->mediaCruise[1]->url }}" class="img-fluid" alt="">
								</div>
							</div>

						@elseif ($cruise->mediaCruise->count() == 1)

							<div class="col-lg-12 col-md-12">
								<div class="album-thumb">
									<img src="{{ $cruise->mediaCruise[0]->url }}" class="img-fluid" alt="">
								</div>
							</div>

						@endif

						</div>

					@else

					{{--	<div class="row album">
							<p>No image found in album.</p>
						</div> <!-- end row -->--}}

					@endif
			</div> <!-- end container -->
		</section> <!-- end cruise-detail -->
		<section class="scrollspy">
			<section class="submenu float-panel" data-top="0" data-scroll="300">
				<div class="container">
					<ul class="nav justify-content-center sub-menu" id="submenu">
						<li class="nav-item">
							<a class="nav-link" href="#cabin">cabin & rates</a>
						</li> <!-- end nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#itineraries">cruise itineraries</a>
						</li> <!-- end nav-item --> 
						<li class="nav-item">
							<a class="nav-link" href="#services">services & facilities</a>
						</li> <!-- end nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#reviews">customer reviews</a>
						</li> <!-- end nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#">contact us</a>
						</li> <!-- end nav-item -->
					</ul>
				</div> <!-- end container -->
			</section> <!-- end submenu -->
		</section> <!-- end scrollspy -->
		<section class="param-summary">
			<div class="container">
				<h4>{!! $cruise->introduction !!}</h4>
			</div> <!-- end container -->
		</section> <!-- end param-summary -->
		<section class="cabin" id="cabin">
			<div class="container">
				<h2 class="title-paragraph">Cabin & Rates</h2>
				<div class="form-search">
					<form action="" method="POST">
						<div class="form-row clearfix">
							<div class="group col-lg-10 col-md-12 col-sm-10">
								<div class="row">
									<div class="form-group col-lg-3 col-md-6 col-sm-3 col-12 mb-2 mb-sm-0 itinerary">
										<label for="">ITINERARY</label>
										<select name="itinerary" id="itinerary" class="form-control" onchange="setCheckout()">
									    	<option value="" disabled hidden selected>PLEASE SELECT</option>
									    	<option value="1">2 Days 1 Night</option>
									    	<option value="2">3 Days 2 Nights</option>
									  	</select>
									</div>
									<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 checkout">
										<label for="">CHECKIN</label>
										<input type="text" class="form-control" name="checkin" id="checkin" data-toggle="datepicker" placeholder="ANY DATE" autocomplete="off" onchange="setCheckout()">
										<i class="fa fa-calendar icon-calendar" id="icon-calendar"></i>
									</div>
									<div class="form-group col-lg-2 col-md-6 col-sm-2 col-12 mb-2 mb-sm-0 checkout">
										<label for="">CHECKOUT</label>
										<input type="text" class="form-control" name="checkout" id="checkout" placeholder="ANY DATE" readonly>
										<i class="fa fa-calendar icon-calendar"></i>
									</div>
									<div class="form-group col-lg-3 col-md-12 col-sm-3 col-12 mb-2 mb-sm-0" style="position:relative;">
										<label for="">PASSENGER</label>
										<a data-toggle="modal" data-target="#RoomAdultModal"><input type="text" id="passenger" name="passenger" value="" class="form-control" placeholder="0 Room, 0 Adult, 0 Children" ></a>
									</div>
								</div>
								
							</div>
							<div class="col-lg-2 col-md-12 col-sm-2 col-12 button-search">
								<button class="button t400 btn-block" type="submit">Search</button>
							</div>
						</div>
					</form>
				</div> <!-- end form-search -->

				@include('frontend.includes.detail_cabin_item', ['listCabins' => $listCabins])

			</div> <!-- end container -->
		</section> <!-- end cabin -->
		<section class="rates">
			<div class="container">
				<div class="card-columns">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-1 col-md-1">
									<img src="{{ url('frontend/img/icon/detail_check.png') }}" class="rates-icon" alt="">
								</div> <!-- end col-2 -->
								<div class="col-sm-11 col-md-10">
									<h3 class="text-brown card-title">Excelente experiencia</h3>
									<ul class="card-list">
										{!! $cruise->price_include !!}
									</ul>
								</div> <!-- end col-10 -->
							</div> <!-- end row -->
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-1 col-md-1">
									<img src="{{ url('frontend/img/icon/detail_error.png') }}" class="rates-icon" alt="">
								</div> <!-- end col-2 -->
								<div class="col-sm-11 col-md-10">
									<h3 class="text-brown card-title">Cancellation by Customer</h3>
									<ul class="card-list">
                                        {!! $cruise->cancel_by_customer !!}
									</ul>
								</div> <!-- end col-10 -->
							</div> <!-- end row -->
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-1 col-md-1">
									<img src="{{ url('frontend/img/icon/detail_child.png') }}" class="rates-icon" alt="">
								</div> <!-- end col-2 -->
								<div class="col-sm-11 col-md-10">
									<h3 class="text-brown card-title">Children Policy</h3>
									<ul class="card-list">
                                        {!! $cruise->children_policy !!}
									</ul>
								</div> <!-- end col-10 -->
							</div> <!-- end row -->
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-1 col-md-1">
									<img src="{{ url('frontend/img/icon/detail_list.png') }}" class="rates-icon" alt="">
								</div> <!-- end col-2 -->
								<div class="col-sm-11 col-md-10">
									<h3 class="text-brown card-title">Price Excluded</h3>
									<ul class="card-list">
                                        {!! $cruise->price_exclude !!}
                                    </ul>
								</div> <!-- end col-10 -->
							</div> <!-- end row -->
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-1 col-md-1">
									<img src="{{ url('frontend/img/icon/detail_error.png') }}" class="rates-icon" alt="">
								</div> <!-- end col-2 -->
								<div class="col-sm-11 col-md-10">
									<h3 class="text-brown card-title">Cancellation due to bad weather</h3>
									<ul class="card-list">
                                        {!! $cruise->cancel_by_weather !!}
									</ul>
								</div> <!-- end col-10 -->
							</div> <!-- end row -->
						</div> <!-- end card-body -->
					</div> <!-- end card -->
				</div> <!-- end card-columns -->
			</div> <!-- end container -->
		</section> <!-- end rates -->
		<section class="cruise-itineraries" id="itineraries">
			<div class="container">
				<h2 class="title-paragraph">Cruise Itineraries</h2>

                @if ($cruise->has_journal_type1 == 1)

				<div id="accordion-1" class="cruise-itineraries__item">
					<div class="card">
						<div class="card-header cruise-itineraries__item-heading title-heading" id="" data-toggle="collapse" data-target="#itineraries1-1" aria-expanded="true" aria-controls="itineraries1-1">
							<h3 class="card-header__title">ITINERARY 2 DAYS 1 NIGHT <small class="float-right show-text"><span class="if-collapsed">SHOW LESS</span><span class="if-not-collapsed">details</span></small></h3>
						</div>

						<div id="itineraries1-1" class="collapse show" aria-labelledby="" data-parent="#accordion-1">
							<div class="card-body cruise-itineraries__item-content">
								<div id="day1-1">
									<div class="card card-list">
										<div class="card-header cruise-itineraries__item-content_heading" id="" data-toggle="collapse" data-target="#night1-1" aria-expanded="true" aria-controls="night1-1">
											<h3 class="float-left"><span>Day 1:</span> {{ $cruise->journey_type1_day1_title }}</h3>
											<small class="float-right"><img src="{{ url('frontend/img/icon/icon_up.png') }}" alt="" class="icon-collapse"></small>
										</div> <!-- end cruise-itineraries__item-content_heading -->
										<div id="night1-1" class="collapse show" aria-labeledby="" data-parent="#day1-1">
											<div class="card-body cruise-itineraries__item-content_detail">
												{!! $cruise->journey_type1_day1_detail !!}
                                                <p>Accommodation: {{ $cruise->name }}</p>
												<div class="card-image">
                                                    {{ $cruise->journey_type1_day1_image_1 ? '<img src="'. $cabin->journey_type1_day1_image_1 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type1_day1_image_2 ? '<img src="'. $cabin->journey_type1_day1_image_2 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type1_day1_image_3 ? '<img src="'. $cabin->journey_type1_day1_image_3 .'" alt="">' : '' }}
												</div>
											</div> <!-- end  cruise-itineraries__item-content_detail -->
										</div> <!-- end #night-1 -->
									</div> <!-- end card-list -->
								</div> <!-- end #day-1 -->
								<div id="day1-2">
                                    <div class="card card-list">
                                        <div class="card-header cruise-itineraries__item-content_heading" id="" data-toggle="collapse" data-target="#night1-1" aria-expanded="true" aria-controls="night1-1">
                                            <h3 class="float-left"><span>Day 2:</span> {{ $cruise->journey_type1_day2_title }}</h3>
                                            <small class="float-right"><img src="{{ url('frontend/img/icon/icon_up.png') }}" alt="" class="icon-collapse"></small>
                                        </div> <!-- end cruise-itineraries__item-content_heading -->
                                        <div id="night1-1" class="collapse show" aria-labeledby="" data-parent="#day1-2">
                                            <div class="card-body cruise-itineraries__item-content_detail">
                                                {!! $cruise->journey_type1_day2_detail !!}
                                                <p>Accommodation: {{ $cruise->name }}</p>
                                                <div class="card-image">
                                                    {{ $cruise->journey_type1_day2_image_1 ? '<img src="'. $cabin->journey_type1_day2_image_1 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type1_day2_image_2 ? '<img src="'. $cabin->journey_type1_day2_image_2 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type1_day2_image_3 ? '<img src="'. $cabin->journey_type1_day2_image_3 .'" alt="">' : '' }}
                                                </div>
                                            </div> <!-- end  cruise-itineraries__item-content_detail -->
                                        </div> <!-- end #night-1 -->
                                    </div> <!-- end card-list -->
								</div> <!-- end #day-2 -->
							</div> <!-- end card-body cruise-itineraries__item-content -->
						</div> <!-- end #itineraries-1 -->
					</div> <!-- end card -->
				</div> <!-- end cruise-itineraries__item -->

                @endif
                @if ($cruise->has_journal_type2 == 1)

                <div id="accordion-2" class="cruise-itineraries__item">
					<div class="card">
						<div class="card-header cruise-itineraries__item-heading title-heading" id="" data-toggle="collapse" data-target="#itineraries2-1" aria-expanded="false" aria-controls="itineraries2-1">
							<h3 class="card-header__title">ITINERARY 3 DAYS 2 NIGHT <small class="float-right show-text"><span class="if-collapsed">SHOW LESS</span><span class="if-not-collapsed">details</span></small></h3>
						</div>

						<div id="itineraries2-1" class="collapse" aria-labelledby="" data-parent="#accordion-2">
							<div class="card-body cruise-itineraries__item-content">
								<div id="day2-1">
                                    <div class="card card-list">
                                        <div class="card-header cruise-itineraries__item-content_heading" id="" data-toggle="collapse" data-target="#night1-1" aria-expanded="true" aria-controls="night1-1">
                                            <h3 class="float-left"><span>Day 1:</span> {{ $cruise->journey_type2_day1_title }}</h3>
                                            <small class="float-right"><img src="{{ url('frontend/img/icon/icon_up.png') }}" alt="" class="icon-collapse"></small>
                                        </div> <!-- end cruise-itineraries__item-content_heading -->
                                        <div id="night1-1" class="collapse show" aria-labeledby="" data-parent="#day2-1">
                                            <div class="card-body cruise-itineraries__item-content_detail">
                                                {!! $cruise->journey_type2_day1_detail !!}
                                                <p>Accommodation: {{ $cruise->name }}</p>
                                                <div class="card-image">
                                                    {{ $cruise->journey_type2_day1_image_1 ? '<img src="'. $cabin->journey_type2_day1_image_1 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type2_day1_image_2 ? '<img src="'. $cabin->journey_type2_day1_image_2 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type2_day1_image_3 ? '<img src="'. $cabin->journey_type2_day1_image_3 .'" alt="">' : '' }}
                                                </div>
                                            </div> <!-- end  cruise-itineraries__item-content_detail -->
                                        </div> <!-- end #night-1 -->
                                    </div> <!-- end card-list -->
								</div> <!-- end #day2-1 -->
								<div id="day2-2">
                                    <div class="card card-list">
                                        <div class="card-header cruise-itineraries__item-content_heading" id="" data-toggle="collapse" data-target="#night1-1" aria-expanded="true" aria-controls="night1-1">
                                            <h3 class="float-left"><span>Day 2:</span> {{ $cruise->journey_type2_day2_title }}</h3>
                                            <small class="float-right"><img src="{{ url('frontend/img/icon/icon_up.png') }}" alt="" class="icon-collapse"></small>
                                        </div> <!-- end cruise-itineraries__item-content_heading -->
                                        <div id="night1-1" class="collapse show" aria-labeledby="" data-parent="#day2-1">
                                            <div class="card-body cruise-itineraries__item-content_detail">
                                                {!! $cruise->journey_type2_day2_detail !!}
                                                <p>Accommodation: {{ $cruise->name }}</p>
                                                <div class="card-image">
                                                    {{ $cruise->journey_type2_day2_image_1 ? '<img src="'. $cabin->journey_type2_day2_image_1 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type2_day2_image_2 ? '<img src="'. $cabin->journey_type2_day2_image_2 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type2_day2_image_3 ? '<img src="'. $cabin->journey_type2_day2_image_3 .'" alt="">' : '' }}
                                                </div>
                                            </div> <!-- end  cruise-itineraries__item-content_detail -->
                                        </div> <!-- end #night-1 -->
                                    </div> <!-- end card-list -->
								</div> <!-- end #day2-2 -->
								<div id="day2-3">
                                    <div class="card card-list">
                                        <div class="card-header cruise-itineraries__item-content_heading" id="" data-toggle="collapse" data-target="#night1-1" aria-expanded="true" aria-controls="night1-1">
                                            <h3 class="float-left"><span>Day 1:</span> {{ $cruise->journey_type2_day3_title }}</h3>
                                            <small class="float-right"><img src="{{ url('frontend/img/icon/icon_up.png') }}" alt="" class="icon-collapse"></small>
                                        </div> <!-- end cruise-itineraries__item-content_heading -->
                                        <div id="night1-1" class="collapse show" aria-labeledby="" data-parent="#day2-1">
                                            <div class="card-body cruise-itineraries__item-content_detail">
                                                {!! $cruise->journey_type2_day3_detail !!}
                                                <p>Accommodation: {{ $cruise->name }}</p>
                                                <div class="card-image">
                                                    {{ $cruise->journey_type2_day3_image_1 ? '<img src="'. $cabin->journey_type2_day3_image_1 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type2_day3_image_2 ? '<img src="'. $cabin->journey_type2_day3_image_2 .'" alt="">' : '' }}
                                                    {{ $cruise->journey_type2_day3_image_3 ? '<img src="'. $cabin->journey_type2_day3_image_3 .'" alt="">' : '' }}
                                                </div>
                                            </div> <!-- end  cruise-itineraries__item-content_detail -->
                                        </div> <!-- end #night-1 -->
                                    </div> <!-- end card-list -->
								</div> <!-- end #day2-3 -->
							</div>
						</div>
					</div>
				</div> <!-- end cruise-itineraries__item -->

                @endif

			</div> <!-- end container -->
		</section> <!-- end cruise-itineraries -->
		<section class="services-facilities" id="services">
			<div class="container">
				<h2 class="title-paragraph">Services & Facilities</h2>
				<div class="card-columns">
					<div class="card">
						<div class="card-body">
							<h3 class="text-brown card-title">Services</h3>
							<ul class="card-list">
								{!! $cruise->services !!}
							</ul>
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<h3 class="text-brown card-title">Departure Info</h3>
							<ul class="card-list">
                                {!! $cruise->departure_info !!}
							</ul>
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<h3 class="text-brown card-title">Activities on Board</h3>
							<ul class="card-list">
                                {!! $cruise->activities !!}
							</ul>
						</div> <!-- end card-body -->
					</div> <!-- end card -->
					<div class="card">
						<div class="card-body">
							<h3 class="text-brown card-title">Activities on Request</h3>
							<ul class="card-list">
                                {!! $cruise->activities_on_request !!}
							</ul>
						</div> <!-- end card-body -->
					</div> <!-- end card -->
				</div> <!-- end card-columns -->
			</div> <!-- end container -->
		</section> <!-- end Services & Facilities -->
		<section class="customer-reviews" id="reviews">
			<div class="container">
				<h2 class="title-paragraph">Customer Reviews
					<small><button type="button" class="btn btn-write">WRITE REVIEW</button></small>
				</h2>
				<div class="review-rating">
					<div class="row">
						<div class="col-lg-3 col-md-3">
							<div class="total-review">
								<p class="total-review__text except">Exceptional</p>
								<p class="total-review__percent">{{ $cruise->review->count() > 0 ? $cruise->review->average('traveler_rating') : '9' }}/10</p>
								<p class="total-review__text">From {{ $cruise->review->count() > 0 ? $cruise->review->count() : 1 }} reviews</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="total-rating">
								<h3 class="title-head__review">Traveler rating</h3>
								<div class="group-progress">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: {{ $cruise->review->count() > 0 ? $cruise->review->average('point_cruise_quality')*10 : '90' }}%" aria-valuenow="{{ $cruise->review->count() > 0 ? $cruise->review->average('point_cruise_quality') : '9' }}" aria-valuemin="0" aria-valuemax="10"></div>
									</div> <!-- end progress -->
									<div class="progress-text clearfix">
										<p class="progress-text__title float-left">cruise quality</p>
										<p class="progress-text__number float-right">{{ $cruise->review->count() > 0 ? $cruise->review->average('point_cruise_quality') : '9' }}</p>
									</div> <!-- end progress-text -->
								</div> <!-- end group-progress -->
								<div class="group-progress">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: {{ $cruise->review->count() > 0 ? $cruise->review->average('point_food_and_drink')*10 : '90' }}%" aria-valuenow="{{ $cruise->review->count() > 0 ? $cruise->review->average('point_food_and_drink') : '9' }}" aria-valuemin="0" aria-valuemax="10"></div>
									</div> <!-- end progress -->
									<div class="progress-text clearfix">
										<p class="progress-text__title float-left">food & drink</p>
										<p class="progress-text__number float-right">{{ $cruise->review->count() > 0 ? $cruise->review->average('point_food_and_drink') : '9' }}</p>
									</div> <!-- end progress-text -->
								</div> <!-- end group-progress -->
								<div class="group-progress">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: {{ $cruise->review->count() > 0 ? $cruise->review->average('point_cabin_quality')*10 : '90' }}%" aria-valuenow="{{ $cruise->review->count() > 0 ? $cruise->review->average('point_cabin_quality') : '9' }}" aria-valuemin="0" aria-valuemax="10"></div>
									</div> <!-- end progress -->
									<div class="progress-text clearfix">
										<p class="progress-text__title float-left">cabin quality</p>
										<p class="progress-text__number float-right">{{ $cruise->review->count() > 0 ? $cruise->review->average('point_cabin_quality') : '9' }}</p>
									</div> <!-- end progress-text -->
								</div> <!-- end group-progress -->
								<div class="group-progress">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: {{ $cruise->review->count() > 0 ? $cruise->review->average('point_customer_service')*10 : '90' }}%" aria-valuenow="{{ $cruise->review->count() > 0 ? $cruise->review->average('point_customer_service') : '9' }}" aria-valuemin="0" aria-valuemax="10"></div>
									</div> <!-- end progress -->
									<div class="progress-text clearfix">
										<p class="progress-text__title float-left">custom service</p>
										<p class="progress-text__number float-right">{{ $cruise->review->count() > 0 ? $cruise->review->average('point_customer_service') : '9' }}</p>
									</div> <!-- end progress-text -->
								</div> <!-- end group-progress -->
								<div class="group-progress">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: {{ $cruise->review->count() > 0 ? $cruise->review->average('point_activities')*10 : '90' }}%" aria-valuenow="{{ $cruise->review->count() > 0 ? $cruise->review->average('point_activities') : '9' }}" aria-valuemin="0" aria-valuemax="10"></div>
									</div> <!-- end progress -->
									<div class="progress-text clearfix">
										<p class="progress-text__title float-left">activities</p>
										<p class="progress-text__number float-right">{{ $cruise->review->count() > 0 ? $cruise->review->average('point_activities') : '9' }}</p>
									</div> <!-- end progress-text -->
								</div> <!-- end group-progress -->
							</div> <!-- end total-rating -->
						</div> <!-- end col-lg -->
						<div class="col-lg-1 col-md-1"></div>
						<div class="col-lg-4 col-md-4">
							<div class="travel-type">
								<h3 class="title-head__review">Traveler type</h3>
								<ul class="list-group">
									<li class="list-group-item">Mature couples <span class="badge float-right">{{ $cruise->review->where('traveler_type', '=', 'Mature couples')->count('id') > 0 ? $cruise->review->where('traveler_type', '=', 'Mature couples')->count() : '0' }}</span></li>
									<li class="list-group-item">Solo travellers <span class="badge float-right">{{ $cruise->review->where('traveler_type', '=', 'Solo traveler')->count('id') > 0 ? $cruise->review->where('traveler_type', '=', 'Solo traveler')->count() : '0' }}</span></li>
									<li class="list-group-item">Family with children <span class="badge float-right">{{ $cruise->review->where('traveler_type', '=', 'Family with childrens')->count() > 0 ? $cruise->review->where('traveler_type', '=', 'Family with childrens')->count() : '0' }}</span></li>
									<li class="list-group-item">Groups of friends <span class="badge float-right">{{ $cruise->review->where('traveler_type', '=', 'Group of friends')->count() > 0 ? $cruise->review->where('traveler_type', '=', 'Group of friends')->count() : '0' }}</span></li>
									<li class="list-group-item">Young couples <span class="badge float-right">{{ $cruise->review->where('traveler_type', '=', 'Young couples')->count() > 0 ? $cruise->review->where('traveler_type', '=', 'Young couples')->count() : '0' }}</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div> <!-- end review-rating -->
				<div class="list">

                    @include('frontend.includes.detail_review_item', ['listReviews' => $listReviews])

				</div>
			</div> <!-- end container -->
		</section> <!-- end Customer Reviews -->
	</main>

@endsection


@push('middle-scripts')

	<script src="{{ url('frontend/js/float-panel.js') }}" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/bootstrap-slider.min.js"></script>

@endpush

@push('after-scripts')

	<script>
		$(document).ready(function(){
			// Add scrollspy to <body>
			$('body').scrollspy({target: ".sub-menu", offset: 50});   

			// Add smooth scrolling on all links inside the navbar
			$("#submenu a").on('click', function(event) {
			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
				// Prevent default anchor click behavior
				event.preventDefault();

				// Store hash
				var hash = this.hash;

				// Using jQuery's animate() method to add smooth page scroll
				// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 800, function(){

				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
				});
			}  // End if
			});
		});
	</script>

@endpush