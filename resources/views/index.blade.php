@extends('layouts.app')

@section('title', 'sdsdsd sadsd')

@section('content')
    <section class="team bg-logo-content">
        <div class="container">
            <section class="title-head wow fadeInDown text-center" data-wow-duration="1.5s">
                <h1 class="title-head__heading"><a href="#" title="">A Vietnam’s leading mid-size <br> Software Development Company</a></h1>
            </section>
            <div class="row justify-content-md-center wow fadeInUp">
                <div class="col-md-9">
                    <p>Adamo provides full-cycle services in software development, from mobile and web-based enterprise solutions to web application and portal development. With extensive business domain experience, well-developed technical expertise, a quality-driven delivery model and a profound knowledge of latest industry trends, we offer our clients progressive, timely and high-value solutions.</p>
                </div>
            </div>
            <div class="row list-team">
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/adobe.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/netflix.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/mozilla.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/salesforce.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/xero.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/opera.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/tile.png" class="img-fluid wow pulse" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 list-item text-center align-self-center">
                    <img src="img/homepage/oppo.png" class="img-fluid wow pulse" alt="">
                </div>
            </div>
            <p class="text-center"><a href="our_team.html" title="Talk To Our Team" class="btn border border-primary rounded-pill text-link wow pulse" data-wow-iteration="3" data-wow-duration="0.15">TALK TO OUR TEAM &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
        </div>
    </section> <!-- end h-team -->
    <section class="h-services bg-home-content">
        <div class="container">
            <section class="title-head text-center wow fadeInDown" data-wow-duration="2s">
                <h2 class="title-head__heading"><a href="#" title="" class="text-white">Our Services</a></h2>
            </section>
            <div class="row h-services-list">
                <div class="col-md-4 text-center">
                    <div class="service-item text-center">
                        <div class="service-icon text-center">
                            <a href="#" title=""><img src="img/homepage/mobile_icon.png" class="img-fluid" alt=""></a>
                        </div>
                        <h3><a href="mobile_app.html" title="Mobile App Development" class="text-white">Mobile App Development</a></h3>
                        <p>We offer iOS, Android, and hybrid platform development with collaborative project management and continous quality assurance services</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 text-center">
                    <div class="service-item text-center">
                        <div class="service-icon text-center">
                            <a href="#" title=""><img src="img/homepage/web_icon.png" class="img-fluid" alt=""></a>
                        </div>
                        <h3><a href="#" title="Website Development" class="text-white">Website Development</a></h3>
                        <p>Web Application development has critical objectives: Shorten the product deployment cycle and maximize product’s reach across multiple platforms.</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 text-center">
                    <div class="service-item text-center">
                        <div class="service-icon text-center">
                            <a href="#" title=""><img src="img/homepage/ui_ux_icon.png" class="img-fluid" alt=""></a>
                        </div>
                        <h3><a href="#" title="UX & UI Design" class="text-white">UX & UI Design</a></h3>
                        <p>We offers prototyping, branding, and User Interface/User Experience design services for web and mobile applications. A simple focus on how people will actually use our products.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="h-expertises">
        <section class="title-head text-center wow fadeInDown" data-wow-duration="2s">
            <h2 class="title-head__heading"><a href="#" title="">Our Expertises</a></h2>
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="expertises-item">
                        <a href="#" title="Food & Beverage">
                            <img src="img/homepage/food_begarage.png" alt="">
                            <div class="content">
                                <h3>Food & Beverage</h3>
                                <div class="info">
                                    <p>We offer iOS, Android, and hybrid platform development with collaborative project management and continous quality assurance services</p>
                                    <p><a href="#" title="Read More" class="btn btn-common btn-size border border-white rounded-pill text-uppercase">read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="expertises-item">
                        <a href="#" title="eLearning">
                            <img src="img/homepage/e-learning.png" alt="">
                            <div class="content">
                                <h3>eLearning</h3>
                                <div class="info">
                                    <p>We offer iOS, Android, and hybrid platform development with collaborative project management and continous quality assurance services</p>
                                    <p><a href="#" title="Read More" class="btn btn-common btn-size border border-white rounded-pill text-uppercase">read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="expertises-item">
                        <a href="#" title="Retail & eCommerce">
                            <img src="img/homepage/retail.png" alt="">
                            <div class="content">
                                <h3>Retail & eCommerce</h3>
                                <div class="info">
                                    <p>We offer iOS, Android, and hybrid platform development with collaborative project management and continous quality assurance services</p>
                                    <p><a href="#" title="Read More" class="btn btn-common btn-size border border-white rounded-pill text-uppercase">read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="expertises-item">
                        <a href="#" title="Lifestyle, Travel, Hospitality">
                            <img src="img/homepage/lifestyle.png" alt="">
                            <div class="content">
                                <h3>Lifestyle, Travel, Hospitality</h3>
                                <div class="info">
                                    <p>We offer iOS, Android, and hybrid platform development with collaborative project management and continous quality assurance services</p>
                                    <p><a href="#" title="Read More" class="btn btn-common btn-size border border-white rounded-pill">read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="h-ideas">
        <div class="container text-center">
            <img src="img/homepage/iconstart.png" class="img-fluid" alt="">
            <h4>Ready to start ?</h4>
            <h3>We would love to hear your ideas</h3>
            <p><a href="#" class="btn btn-custom btn-common rounded-pill text-uppercase" title="Let's Talk">let's talk <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
        </div>
    </section>
    <section class="h-latest-news relates">
        <section class="title-head text-center wow fadeInDown" data-wow-duration="2s">
            <h2 class="title-head__heading"><a href="#" title="Lastest News and Insights">Lastest News and Insights</a></h2>
            <p><a href="#" title="All Posts" class="see-more font-weight-bold text-uppercase">all posts <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="relate-item">
                        <div class="relate-img">
                            <a href="#" title=""><img src="img/blog/blog4.jpg" class="img-fluid" alt=""></a>
                        </div>
                        <div class="relate-content">
                            <h3 class="relate-title"><a href="#" title="Should Australian Companies consider outsource software development to Vietnam">Should Australian Companies consider outsource software development to Vietnam</a></h3>
                            <p class="relate-param">Short answer? Yes It’s hard to ignore the growth of Australia’s ICT (Information & Comunication Technologies) market. With major invesments in both infrastructure and labor, companies like Cisco, IBM and even Warner…</p>
                            <a href="#" title="Read More" class="btn-common relate-more text-uppercase">read more <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="relate-list clearfix">
                        <div class="relate-list_img">
                            <img src="img/blog/blog4.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="relate-list_content">
                            <h3 class="relate-title"><a href="#" title="Should Australian Companies consider outsource software development to Vietnam">Should Australian Companies consider outsource software development to Vietnam</a></h3>
                            <p class="relate-param">What is the thing behind Grab’s success? What make Amazon system run smoothly even in the worst situation when technology…</p>
                            <a href="#" title="Read More" class="btn-common relate-more text-uppercase">read more <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="relate-list clearfix">
                        <div class="relate-list_img float-left">
                            <img src="img/blog/blog4.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="relate-list_content float-left">
                            <h3 class="relate-title"><a href="#" title="An introduction to Microservices">An introduction to Microservices</a></h3>
                            <p class="relate-param">What is the thing behind Grab’s success? What make Amazon system run smoothly even in the worst situation when technology …</p>
                            <a href="#" title="Read More" class="btn-common relate-more text-uppercase">read more <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection