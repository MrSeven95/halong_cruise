<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Luxury Halong Cruises')</title>
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

    @stack('before-styles')

    {{ Html::style('xtreme-admin/dist/css/style.min.css') }}
    {{ Html::style('xtreme-admin/dist/css/common.css') }}

    @stack('after-styles')

   <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        var BASE_URL = 'https://adamodigital.com'
    </script>
</head>
<body>
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper">
    <div class="main-wrapper" style="margin-left: 0">
        @yield('content')
    </div>
</div>

<!-- Scripts -->
@stack('before-scripts')

{!! Html::script('xtreme-admin/assets/libs/jquery/dist/jquery.min.js') !!}
{!! Html::script('xtreme-admin/assets/libs/popper.js/dist/umd/popper.min.js') !!}
{!! Html::script('xtreme-admin/assets/libs/bootstrap/dist/js/bootstrap.min.js') !!}
{!! Html::script('xtreme-admin/dist/js/app.min.js') !!}
{!! Html::script('xtreme-admin/dist/js/app.init.horizontal-fullwidth.js') !!}
{!! Html::script('xtreme-admin/dist/js/app-style-switcher.horizontal.js') !!}
{!! Html::script('xtreme-admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('xtreme-admin/assets/extra-libs/sparkline/sparkline.js') !!}
{!! Html::script('xtreme-admin/dist/js/waves.js') !!}
{!! Html::script('xtreme-admin/dist/js/sidebarmenu.js') !!}
{!! Html::script('xtreme-admin/dist/js/custom.js') !!}

@stack('after-scripts')

</body>
</html>