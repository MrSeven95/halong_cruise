
@if(count($breadcrumbs))

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 ml-3 align-self-center">
                {{--@hasSection('title')
                    <h4 class="page-title">
                        @yield('title')
                    </h4>
                @endif--}}
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">

                            @foreach ($breadcrumbs as $breadcrumb)

                                @if ($breadcrumb->url && !$loop->last)
                                    <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
                                @endif

                            @endforeach

                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endif