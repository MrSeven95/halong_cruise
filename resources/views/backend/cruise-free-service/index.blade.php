@extends('layouts.admin')

@section('title', __('backend.cruise-free-service'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
@endpush

@section('content')
    {{ Breadcrumbs::render('cruise-free-service') }}

    <div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6"><h3>{{ __('backend.cruise-free-service') }}</h3></div>
                <div class="col-6 text-right">
                    <a href="{{ route('backend.cruise-free-service.create') }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/cruise-free-service.cruise') }}</th>
                        <th>{{ __('backend/cruise-free-service.free-service') }}</th>
                        <th>{{ __('backend/cruise-free-service.from') }}</th>
                        <th>{{ __('backend/cruise-free-service.to') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
    </div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: "{{ route('backend.cruise-free-service.index') }}?ajax",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'cruise', name: 'cruise'},
                    {data: 'free-service', name: 'free-service'},
                    {data: 'from', name: 'from', searchable: false, className: 'text-center'},
                    {data: 'to', name: 'to', searchable: false, className: 'text-center'},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ]
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.cruise-free-service') }}');
        });
    </script>
@endpush