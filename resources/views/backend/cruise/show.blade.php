@extends('layouts.admin')

@section('title', $data->name)

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
    {{ Html::style('xtreme-admin/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}
@endpush

@section('content')
    {{ Breadcrumbs::render('cruise-show', $data['id'], $data['name']) }}

    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6"><h3>{{ $data->name }}</h3></div>
                    {{--<div class="col-6 text-right">
                        <a href="{{ route('backend.cruise.create') }}" class="btn btn-primary">{{ __('web.create') }}</a>
                    </div>--}}
                </div>

                <div class="table-responsive mt-3">
                    <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                        <thead>
                        <tr>
{{--                            <th>{{ __('No.') }}</th>--}}
                            <th>{{ __('backend/cruise.image_avatar') }}</th>
                            <th>{{ __('backend/cruise.name') }}</th>
                            <th>{{ __('web.more-action') }}</th>
                            <th>{{ __('backend/cruise.is_best_seller') }}</th>
                            <th>{{ __('backend/cruise.display_ranking') }}</th>
                            <th>{{ __('backend/cruise.status') }}</th>
                            <th>{{ __('web.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                processing: false,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: {
                    url: "{{ route('backend.cruise.show', $data->id) }}?ajax"
                },
                columns: [
                    // {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'align-middle', searchable: false},
                    {data: 'image', name: 'image', width: '160px', className: 'text-center', orderable: false, searchable: false, render: function (data, type, row) {
                            var url = data;
                            if (url == null) url = '{{ url('/images/no-image-found.jpg') }}';
                            return '<img src="' + url +'" class="img-thumbnail" alt="{{ __('backend/cabin.image_thumbnail') }} '+ row['name'] +'" width="auto" height="auto"/>';
                        }},
                    {data: 'name', name: 'name', width: '120px', className: 'text-center', orderable: false, searchable: false},
                    {data: 'more', name: 'more', orderable: false, searchable: false, width: '180px'},
                    {data: 'col_best_seller', name: 'col_best_seller', className: 'px-0 text-center', width: '120px', orderable: false, searchable: false},
                    {data: 'display_ranking', name: 'display_ranking', className: 'text-center', searchable: false, width: '40px', orderable: false},
                    {data: 'status', name: 'status', className: 'text-center', orderable: false, searchable: false, width: '80px', render: function (data, type, row) {
                            switch (row['status']) {
                                case 'actived':
                                    return '<span class="label label-success">{{ __('backend/cruise.status_actived') }}</span>';
                                    break;
                                case 'in-actived':
                                    return '<span class="label label-while bg-secondary">{{ __('backend/cruise.status_inactived') }}</span>';
                                    break;
                            }
                            return '';
                        }},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '130px'},
                ],
                drawCallback: function (setting, json) {
                    setupBtSwitchDatatable();
                }
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.cruise') }}');
        });
    </script>
@endpush