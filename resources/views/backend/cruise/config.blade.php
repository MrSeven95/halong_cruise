@extends('layouts.admin')

@section('title', __('backend.cruise-config'))

@push('after-styles')
    <style>
        #preview_image_avatar, #preview_image_seo {
            max-width: 150px;
            height: auto;
        }
    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('cruise-config') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <form class="mt-3" action="{{ route('backend.cruise-config.update') }}" method="post" enctype="multipart/form-data" id="the_form">
                @csrf
                <input type="hidden" name="_method" value="PUT">

                <h3 class="card-title text-center">{{ __('backend.cruise-config') }}</h3>
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="heading">{{ __('backend/cruise-config.heading') }} <span class="text-danger">(*)</span></label>
                                    <input id="heading" type="text" data-name-show="{{ __('backend/cruise-config.heading') }}"
                                           class="custom-validate form-control form-control-lg @error('heading') is-invalid @enderror"
                                           name="heading" value="{{ old('heading') ? old('heading') : $data['heading'] }}" autofocus maxlength="100"
                                           required>

                                    @error('heading')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="slug">{{ __('backend/cruise-config.slug') }} <span class="text-danger">(*)</span></label>
                                    <input id="slug" type="text" data-name-show="{{ __('backend/cruise-config.slug') }}"
                                           class="custom-validate form-control form-control-lg @error('slug') is-invalid @enderror"
                                           name="slug" value="{{ old('slug') ? old('slug') : $data['slug'] }}" maxlength="100"
                                           required>

                                    @error('slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_avatar" class="control-label">{{ __('backend/cruise.image_avatar') }} (1200x900px)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" class="custom-file-input form-control form-control-lg @error('image_avatar') is-invalid @enderror"
                                                   name="image_avatar" value="" onchange="document.getElementById('output_image_avatar').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="is_no_action_image_avatar" value="true" id="is_no_action_image_avatar">
                                    <div class="card mt-2" id="preview_image_avatar">
                                        <img id="output_image_avatar" alt="{{ __('backend/cruise.image_thumbnail') }}" class="img-thumbnail" src="{{ old('image_avatar') ? old('image_avatar') : $data['image_avatar'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="btn btn-danger position-absolute" style="bottom: 10px; right: 10px;" id="remove_image_avatar"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>

                                    @error('image_avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_seo" class="control-label">{{ __('backend/cruise.image_seo') }} (1200x628px)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" class="custom-file-input form-control form-control-lg @error('image_seo') is-invalid @enderror"
                                                   name="image_seo" value="" onchange="document.getElementById('output_image_seo').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="is_no_action_image_seo" value="true" id="is_no_action_image_seo">
                                    <div class="card mt-2" id="preview_image_seo">
                                        <img id="output_image_seo" alt="{{ __('backend/cruise.image_thumbnail') }}" class="img-thumbnail" src="{{ old('image_seo') ? old('image_seo') : $data['image_seo'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="btn btn-danger position-absolute" style="bottom: 10px; right: 10px;" id="remove_image_seo"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>

                                    @error('image_seo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="meta_title">{{ __('backend/cruise-config.meta_title') }} <span class="text-danger">(*)</span></label>
                                    <input id="meta_title" type="text" data-name-show="{{ __('backend/cruise-config.meta_title') }}"
                                           class="custom-validate form-control form-control-lg @error('meta_title') is-invalid @enderror"
                                           name="meta_title" value="{{ old('meta_title') ? old('meta_title') : $data['meta_title'] }}" maxlength="255"
                                           required>

                                    @error('meta_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="meta_description">{{ __('backend/cruise-config.meta_description') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="meta_description" data-name-show="{{ __('backend/cruise-config.meta_description') }}"
                                              class="custom-validate form-control form-control-lg @error('meta_description') is-invalid @enderror"
                                              name="meta_description" maxlength="255"
                                              required>{{ old('meta_description') ? old('meta_description') : $data['meta_description'] }}</textarea>

                                    @error('meta_description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keyword">{{ __('backend/cruise-config.meta_keyword') }} <span class="text-danger">(*)</span></label>
                                    <input id="meta_keyword" type="text" data-name-show="{{ __('backend/cruise-config.meta_keyword') }}"
                                           class="custom-validate form-control form-control-lg @error('meta_keyword') is-invalid @enderror"
                                           name="meta_keyword" value="{{ old('meta_keyword') ? old('meta_keyword') : $data['meta_keyword'] }}" maxlength="255"
                                           required>

                                    @error('meta_keyword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> {{ __('web.save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $('#preview_image_avatar').hide();
            $('#preview_image_seo').hide();

            if ($('#output_image_avatar').attr('src') != '') $('#preview_image_avatar').show();
            if ($('#output_image_seo').attr('src') != '') $('#preview_image_seo').show();

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                validateForm(e);
            });

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __('web.invalid-image-type') }}');
                    return false;
                }
                return true;
            }

            $('input[name=image_avatar]').change(function (e) {
                var file = $(this).val();
                if (checkImage(file)) {
                    $('#preview_image_avatar').show();
                } else {
                    $(this).val('');
                }
            });
            $('input[name=image_seo]').change(function (e) {
                var file = $(this).val();
                if (checkImage(file)) {
                    $('#preview_image_seo').show();
                } else {
                    $(this).val('');
                }
            });
            $('#remove_image_avatar').click(function(e) {
                e.preventDefault();
                $('#is_no_action_image_avatar').val('false');
                $('input[name=image_avatar]').val('');
                $('#preview_image_avatar').hide();
            });
            $('#remove_image_seo').click(function(e) {
                e.preventDefault();
                $('#is_no_action_image_seo').val('false');
                $('input[name=image_seo]').val('');
                $('#preview_image_seo').hide();
            });


            function changeToSlug(txt) {
                var title, slug;

                //Lấy text từ thẻ input title
                title = txt;

                //Đổi chữ hoa thành chữ thường
                slug = title.toLowerCase();

                //Đổi ký tự có dấu thành không dấu
                slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                slug = slug.replace(/đ/gi, 'd');
                //Xóa các ký tự đặt biệt
                slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
                //Đổi khoảng trắng thành ký tự gạch ngang
                slug = slug.replace(/ /gi, "-");
                //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
                //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
                slug = slug.replace(/\-\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-/gi, '-');
                slug = slug.replace(/\-\-/gi, '-');
                //Xóa các ký tự gạch ngang ở đầu và cuối
                slug = '@' + slug + '@';
                slug = slug.replace(/\@\-|\-\@|\@/gi, '');
                //In slug ra textbox có id “slug”

                return slug;
            }

            $('#heading').keyup(function (e) {
                $('#slug').val(changeToSlug($(this).val()));
            });
        });
    </script>
@endpush
