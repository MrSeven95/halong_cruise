@extends('layouts.admin')

@section('title', __('backend/cruise.policy'))

@section('content')
{{ Breadcrumbs::render('cruise-policy', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <h3 class="card-title text-center">{{ __('backend/cruise.policy') }}</h3>

            @if ($data)

            <form class="mt-3" action="{{ route('backend.cruise.policy.update', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">

                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="price_include">{{ __('backend/cruise.price_include') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="price_include" class="form-control form-control-lg @error('price_include') is-invalid @enderror"
                                              name="price_include">{{ old('price_include') ? old('price_include') : $data['price_include'] }}</textarea>

                                    @error('price_include')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_price_include" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="price_exclude">{{ __('backend/cruise.price_exclude') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="price_exclude" class="form-control form-control-lg @error('price_exclude') is-invalid @enderror"
                                              name="price_exclude">{{ old('price_exclude') ? old('price_exclude') : $data['price_exclude'] }}</textarea>

                                    @error('price_exclude')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_price_exclude" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="cancel_by_customer">{{ __('backend/cruise.cancel_by_customer') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="cancel_by_customer" class="form-control form-control-lg @error('cancel_by_customer') is-invalid @enderror"
                                              name="cancel_by_customer">{{ old('cancel_by_customer') ? old('cancel_by_customer') : $data['cancel_by_customer'] }}</textarea>

                                    @error('cancel_by_customer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_cancel_by_customer" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="cancel_by_weather">{{ __('backend/cruise.cancel_by_weather') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="cancel_by_weather" class="form-control form-control-lg @error('cancel_by_weather') is-invalid @enderror"
                                              name="cancel_by_weather">{{ old('cancel_by_weather') ? old('cancel_by_weather') : $data['cancel_by_weather'] }}</textarea>

                                    @error('cancel_by_weather')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_cancel_by_weather" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="children_policy">{{ __('backend/cruise.children_policy') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="children_policy" class="form-control form-control-lg @error('children_policy') is-invalid @enderror"
                                              name="children_policy">{{ old('children_policy') ? old('children_policy') : $data['children_policy'] }}</textarea>

                                    @error('children_policy')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_children_policy" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                    </div>
                </div>
            </form>

            @else
                <p>{{ __('web.not-found', ['name' => __('backend.cruise')]) }}</p>
            @endif

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

    <script>
        $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route('backend.uploadPhoto').'?_token='.csrf_token() !!}';
            CKEDITOR.config.height = '300px';
            CKEDITOR.replace(document.querySelector( '#price_include' ));
            CKEDITOR.replace(document.querySelector( '#price_exclude' ));
            CKEDITOR.replace(document.querySelector( '#cancel_by_customer' ));
            CKEDITOR.replace(document.querySelector( '#cancel_by_weather' ));
            CKEDITOR.replace(document.querySelector( '#children_policy' ));

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.price_include.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.price_include') }} field is required.';
                    addErrorValidateForm($('#validate_price_include'), msg);
                }
                if (CKEDITOR.instances.price_exclude.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.price_exclude') }} field is required.';
                    addErrorValidateForm($('#validate_price_exclude'), msg);
                }
                if (CKEDITOR.instances.cancel_by_customer.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.cancel_by_customer') }} field is required.';
                    addErrorValidateForm($('#validate_cancel_by_customer'), msg);
                }
                if (CKEDITOR.instances.cancel_by_weather.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.cancel_by_weather') }} field is required.';
                    addErrorValidateForm($('#validate_cancel_by_weather'), msg);
                }
                if (CKEDITOR.instances.cancel_by_weather.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.children_policy') }} field is required.';
                    addErrorValidateForm($('#validate_children_policy'), msg);
                }

                validateForm(e);
            });
        });
    </script>
@endpush
