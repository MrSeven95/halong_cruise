@extends('layouts.admin')

@section('title', __('backend/cruise.additional'))

@section('content')
{{ Breadcrumbs::render('cruise-additional', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <h3 class="card-title text-center">{{ __('backend/cruise.additional') }}</h3>

            @if ($data)

            <form class="mt-3" action="{{ route('backend.cruise.additional.update', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">

                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="services">{{ __('backend/cruise.services') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="services" class="form-control form-control-lg @error('services') is-invalid @enderror"
                                              name="services">{{ old('services') ? old('services') : $data['services'] }}</textarea>

                                    @error('services')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_services" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="activities">{{ __('backend/cruise.activities') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="activities" class="form-control form-control-lg @error('activities') is-invalid @enderror"
                                              name="activities">{{ old('activities') ? old('activities') : $data['activities'] }}</textarea>

                                    @error('activities')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_activities" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="departure_info">{{ __('backend/cruise.departure_info') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="departure_info" class="form-control form-control-lg @error('departure_info') is-invalid @enderror"
                                              name="departure_info">{{ old('departure_info') ? old('departure_info') : $data['departure_info'] }}</textarea>

                                    @error('departure_info')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_departure_info" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="activities_on_request">{{ __('backend/cruise.activities_on_request') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="activities_on_request" class="form-control form-control-lg @error('activities_on_request') is-invalid @enderror"
                                              name="activities_on_request">{{ old('activities_on_request') ? old('activities_on_request') : $data['activities_on_request'] }}</textarea>

                                    @error('activities_on_request')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_activities_on_request" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                    </div>
                </div>
            </form>

            @else
                <p>{{ __('web.not-found', ['name' => __('backend.cruise')]) }}</p>
            @endif

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

    <script>
        $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route('backend.uploadPhoto').'?_token='.csrf_token() !!}';
            CKEDITOR.config.height = '300px';
            CKEDITOR.replace(document.querySelector( '#services' ));
            CKEDITOR.replace(document.querySelector( '#activities' ));
            CKEDITOR.replace(document.querySelector( '#departure_info' ));
            CKEDITOR.replace(document.querySelector( '#activities_on_request' ));

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.services.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.services') }} field is required.';
                    addErrorValidateForm($('#validate_services'), msg);
                }
                if (CKEDITOR.instances.activities.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.activities') }} field is required.';
                    addErrorValidateForm($('#validate_activities'), msg);
                }
                if (CKEDITOR.instances.departure_info.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.departure_info') }} field is required.';
                    addErrorValidateForm($('#validate_departure_info'), msg);
                }
                if (CKEDITOR.instances.activities_on_request.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.activities_on_request') }} field is required.';
                    addErrorValidateForm($('#validate_activities_on_request'), msg);
                }

                validateForm(e);
            });
        });
    </script>
@endpush
