@extends('layouts.admin')

@section('title', __('web.editing', ['name' => __('backend.cruise')]))

@push('after-styles')
    <style>
        #preview_image_avatar, #preview_image_seo {
            max-width: 150px;
            height: auto;
        }
    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('cruise-edit', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <h3 class="card-title text-center">{{ __('web.editing', ['name' => __('backend.cruise')]) }}</h3>

            @if ($data)

            <form id="the_form" action="{{ route('backend.cruise.update', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">

                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="name">{{ __('backend/cruise.name') }} <span class="text-danger">(*)</span></label>
                                    <input id="name" type="text" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') ? old('name') : $data['name'] }}" autofocus maxlength="100"
                                           required>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="slug">{{ __('backend/cruise.slug') }} <span class="text-danger">(*)</span></label>
                                    <input id="slug" type="text" class="custom-validate form-control form-control-lg @error('slug') is-invalid @enderror"
                                           name="slug" value="{{ old('slug') ? old('slug') : $data['slug'] }}" maxlength="100"
                                           required>

                                    @error('slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-3" for="areas">{{ __('backend/cruise.area') }} <span class="text-danger">(*)</span></label>
                                    <div class="row">
                                        @for ($i = 0; $i<count($listCruiseAreas); $i=$i+max(3, round(count($listCruiseAreas)/4)))
                                            <div class="col-md-3">
                                                @for ($j = 0; $j<max(3, round(count($listCruiseAreas)/4)); $j=$j+1)
                                                    @if (($i+$j) < count($listCruiseAreas) && $listCruiseAreas[$i + $j])
                                                        <div class="form-check pl-0 mt-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input id="checkbox_area_{{ $listCruiseAreas[$i + $j]->id }}" type="checkbox"
                                                                       class="custom-control-input form-check-input" name="areas[]"
                                                                       value="{{ $listCruiseAreas[$i + $j]->id }}"{{ old('areas') ? (in_array($listCruiseAreas[$i + $j]->id, old('areas')) ? ' checked' : '') : (in_array($listCruiseAreas[$i + $j]->id, $data['areas']) ? ' checked' : '') }}>
                                                                <label for="checkbox_area_{{ $listCruiseAreas[$i + $j]->id }}"
                                                                       class="custom-control-label">{{ $listCruiseAreas[$i + $j]->name }}</label>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endfor
                                            </div>
                                        @endfor
                                    </div>
                                    @error('areas')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_cruise_areas" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3 mt-1">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="journal">{{ __('backend/cruise.journal') }} <span class="text-danger">(*)</span></label>
                                    <input id="journal" type="text"  data-name-show="{{ __('backend/cruise.journal') }}" class="custom-validate form-control form-control-lg @error('journal') is-invalid @enderror"
                                           name="journal" value="{{ old('journal') ? old('journal') : $data['journal'] }}" autofocus maxlength="255"
                                           required>

                                    @error('journal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="address">{{ __('backend/cruise.address') }} <span class="text-danger">(*)</span></label>
                                    <input id="address" type="text"  data-name-show="{{ __('backend/cruise.address') }}" class="custom-validate form-control form-control-lg @error('address') is-invalid @enderror"
                                           name="address" value="{{ old('address') ? old('address') : $data['address'] }}" autofocus maxlength="255"
                                           required>

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="star">{{ __('backend/cruise.star') }} <span class="text-danger">(*)</span></label>
                                    <select name="star" id="star"  data-name-show="{{ __('backend/cruise.star') }}" class="select2 custom-validate form-control form-control-lg @error('star') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        <option value="5"{{ old('star') ? (old('star') == 5 ? ' selected' : '') : ($data['star'] == 5 ? ' selected' : '') }}>5</option>
                                        <option value="4"{{ old('star') ? (old('star') == 4 ? ' selected' : '') : ($data['star'] == 4 ? ' selected' : '') }}>4</option>
                                        <option value="3"{{ old('star') ? (old('star') == 3 ? ' selected' : '') : ($data['star'] == 3 ? ' selected' : '') }}>3</option>
                                    </select>

                                    @error('star')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="cruise_category">{{ __('backend/cruise.cruise_category') }} <span class="text-danger">(*)</span></label>
                                    <select name="cruise_category" data-name-show="{{ __('backend/cruise.cruise_category') }}"  id="cruise_category" class="select2 custom-validate form-control form-control-lg @error('cruise_category') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        <option value="{{ __('backend/cruise.category_iron') }}"{{ old('cruise_category') ? (old('cruise_category') == __('backend/cruise.category_iron') ? ' selected' : '') : ($data['cruise_category'] == __('backend/cruise.category_iron') ? ' selected' : '') }}>{{ __('backend/cruise.category_iron') }}</option>
                                        <option value="{{ __('backend/cruise.category_wood') }}"{{ old('cruise_category') ? (old('cruise_category') == __('backend/cruise.category_wood') ? ' selected' : '') : ($data['cruise_category'] == __('backend/cruise.category_wood') ? ' selected' : '') }}>{{ __('backend/cruise.category_wood') }}</option>
                                    </select>

                                    @error('cruise_category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="no_of_cabins">{{ __('backend/cruise.no_of_cabins') }} <span class="text-danger">(*)</span></label>
                                    <select name="no_of_cabins"  data-name-show="{{ __('backend/cruise.no_of_cabins') }}" id="no_of_cabins" class="select2 custom-validate form-control form-control-lg @error('no_of_cabins') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        @for ($i = 10; $i <= 50; $i++)
                                            <option value="{{ $i }}"{{ old('no_of_cabins') ? (old('no_of_cabins') == $i ? ' selected' : '') : ($data['no_of_cabins'] == $i ? ' selected' : '') }}>{{ $i }}</option>
                                        @endfor
                                    </select>

                                    @error('no_of_cabins')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="launch_year">{{ __('backend/cruise.launch_year') }} <span class="text-danger">(*)</span></label>
                                    <select name="launch_year"  data-name-show="{{ __('backend/cruise.launch_year') }}" id="launch_year" class="select2 custom-validate form-control form-control-lg @error('launch_year') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        @for ($i = 0; $i <= 20; $i++)
                                            <option value="{{ date("Y") - $i }}"{{ old('launch_year') ? (old('launch_year') == date("Y") - $i ? ' selected' : '') : ($data['launch_year'] == date("Y") - $i ? ' selected' : '') }}>{{ date("Y") - $i }}</option>
                                        @endfor
                                    </select>

                                    @error('launch_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="address">{{ __('backend/cruise.introduction') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="introduction" class="form-control form-control-lg @error('introduction') is-invalid @enderror"
                                              name="introduction">{{ old('introduction') ? old('introduction') : $data['introduction'] }}</textarea>

                                    @error('introduction')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_introduction" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_avatar" class="control-label">{{ __('backend/cruise.image_avatar') }} (1200x900px)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" class="custom-file-input form-control form-control-lg @error('image_avatar') is-invalid @enderror"
                                                   name="image_avatar" value="" onchange="document.getElementById('output_image_avatar').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="is_no_action_image_avatar" value="true" id="is_no_action_image_avatar">
                                    <div class="card mt-2" id="preview_image_avatar">
                                        <img id="output_image_avatar" alt="{{ __('backend/cruise.image_thumbnail') }}" class="img-thumbnail" src="{{ old('image_avatar') ? old('image_avatar') : $data['image_avatar'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="btn btn-danger position-absolute" style="bottom: 10px; right: 10px;" id="remove_image_avatar"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>

                                    @error('image_avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_seo" class="control-label">{{ __('backend/cruise.image_seo') }} (1200x628px)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" class="custom-file-input form-control form-control-lg @error('image_seo') is-invalid @enderror"
                                                   name="image_seo" value="" onchange="document.getElementById('output_image_seo').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="is_no_action_image_seo" value="true" id="is_no_action_image_seo">
                                    <div class="card mt-2" id="preview_image_seo">
                                        <img id="output_image_seo" alt="{{ __('backend/cruise.image_thumbnail') }}" class="img-thumbnail" src="{{ old('image_seo') ? old('image_seo') : $data['image_seo'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="btn btn-danger position-absolute" style="bottom: 10px; right: 10px;" id="remove_image_seo"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>

                                    @error('image_seo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="meta_title">{{ __('backend/cruise.meta_title') }} <span class="text-danger">(*)</span></label>
                                    <input id="meta_title" type="text" data-name-show="{{ __('backend/cruise.meta_title') }}"  class="custom-validate form-control form-control-lg @error('meta_title') is-invalid @enderror"
                                           name="meta_title" value="{{ old('meta_title') ? old('meta_title') : $data['meta_title'] }}" maxlength="255"
                                           required>

                                    @error('meta_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="meta_description">{{ __('backend/cruise.meta_description') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="meta_description"  data-name-show="{{ __('backend/cruise.meta_description') }}" class="custom-validate form-control form-control-lg @error('meta_description') is-invalid @enderror"
                                              name="meta_description" maxlength="255"
                                              required>{{ old('meta_description') ? old('meta_description') : $data['meta_description'] }}</textarea>

                                    @error('meta_description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keyword">{{ __('backend/cruise.meta_keyword') }} <span class="text-danger">(*)</span></label>
                                    <input id="meta_keyword" type="text" data-name-show="{{ __('backend/cruise.meta_keyword') }}"  class="custom-validate form-control form-control-lg @error('meta_keyword') is-invalid @enderror"
                                           name="meta_keyword" value="{{ old('meta_keyword') ? old('meta_keyword') : $data['meta_keyword'] }}" maxlength="255"
                                           required>

                                    @error('meta_keyword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="display_ranking">{{ __('backend/cruise.display_ranking') }} <span class="text-danger">(*)</span></label>
                                    <input id="display_ranking" min="1" data-name-show="{{ __('backend/cruise.display_ranking') }}"  type="number" class="custom-validate form-control form-control-lg @error('display_ranking') is-invalid @enderror"
                                           name="display_ranking" value="{{ old('display_ranking') ? old('display_ranking') : $data['display_ranking'] }}" maxlength="255"
                                           required>

                                    @error('display_ranking')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="status">{{ __('backend/cruise.status') }} <span class="text-danger">(*)</span></label>
                                    <select name="status" id="status"  data-name-show="{{ __('backend/cruise.status') }}" class="select2 custom-validate form-control form-control-lg @error('status') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        <option value="actived"{{ old('status') ? (old('status') == 'actived' ? ' selected' : '') : ($data['status'] == 'actived' ? ' selected' : '') }}>Actived</option>
                                        <option value="in-actived"{{ old('status') ? (old('status') == 'in-actived' ? ' selected' : '') : ($data['status'] == 'in-actived' ? ' selected' : '') }}>In-Actived</option>
                                    </select>

                                    @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                    </div>
                </div>
            </form>

            @else
                <p>{{ __('web.not-found', ['name' => __('backend.cruise')]) }}</p>
            @endif

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

    <script>
        $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route('backend.uploadPhoto').'?_token='.csrf_token() !!}';
            CKEDITOR.config.height = '300px';
            CKEDITOR.replace(document.querySelector( '#introduction' ));

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            $('#preview_image_avatar').hide();
            $('#preview_image_seo').hide();

            if ($('#output_image_avatar').attr('src') != '') $('#preview_image_avatar').show();
            if ($('#output_image_seo').attr('src') != '') $('#preview_image_seo').show();

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                var checked = $("input[name='areas[]']:checked").length;
                if (checked == 0) {
                    var msg = 'The {{ __('backend.cruise-area') }} field is required.';
                    addErrorValidateForm($('#validate_cruise_areas'), msg);
                }

                if (CKEDITOR.instances.introduction.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.introduction') }} field is required.';
                    addErrorValidateForm($('#validate_introduction'), msg);
                }

                validateForm(e);
            });

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __('web.invalid-image-type') }}');
                    return false;
                }
                return true;
            }

            $('input[name=image_avatar]').change(function(e) {
                var file = $(this).val();
                if (checkImage(file)) {
                    $('#preview_image_avatar').show();
                } else {
                    $(this).val('');
                }
            });
            $('input[name=image_seo]').change(function(e) {
                var file = $(this).val();
                if (checkImage(file)) {
                    $('#preview_image_seo').show();
                } else {
                    $(this).val('');
                }
            });
            $('#remove_image_avatar').click(function(e) {
                e.preventDefault();
                $('#is_no_action_image_avatar').val('false');
                $('input[name=image_avatar]').val('');
                $('#preview_image_avatar').hide();
            });
            $('#remove_image_seo').click(function(e) {
                e.preventDefault();
                $('#is_no_action_image_seo').val('false');
                $('input[name=image_seo]').val('');
                $('#preview_image_seo').hide();
            });

            function changeToSlug(txt) {
                var title, slug;

                //Lấy text từ thẻ input title
                title = txt;

                //Đổi chữ hoa thành chữ thường
                slug = title.toLowerCase();

                //Đổi ký tự có dấu thành không dấu
                slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                slug = slug.replace(/đ/gi, 'd');
                //Xóa các ký tự đặt biệt
                slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
                //Đổi khoảng trắng thành ký tự gạch ngang
                slug = slug.replace(/ /gi, "-");
                //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
                //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
                slug = slug.replace(/\-\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-/gi, '-');
                slug = slug.replace(/\-\-/gi, '-');
                //Xóa các ký tự gạch ngang ở đầu và cuối
                slug = '@' + slug + '@';
                slug = slug.replace(/\@\-|\-\@|\@/gi, '');
                //In slug ra textbox có id “slug”

                return slug;
            }

            $('#name').keyup(function (e) {
                $('#slug').val(changeToSlug($(this).val()));
            });
        });
    </script>
@endpush
