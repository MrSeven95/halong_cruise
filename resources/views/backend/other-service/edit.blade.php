@extends('layouts.admin')

@section('title', __('web.editing', ['name' => __('backend.other-service')]))

@section('content')
{{ Breadcrumbs::render('other-service-edit') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body container">
            <h3 class="text-center">{{ __('web.editing', ['name' => __('backend.other-service')]) }}</h3>

            @if ($data)
                <form class="mt-3" action="{{ route('backend.other-service.update', ['id' => $data['id']]) }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row pt-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">{{ __('backend/other-service.name') }} <span class="text-danger">(*)</span></label>
                                <input id="name" type="text"  data-name-show="{{ __('backend/other-service.name') }}" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                       name="name" value="{{ old('name') ? old('name') : $data['name'] }}" autofocus maxlength="160"
                                       required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="price">{{ __('backend/other-service.price') }} <span class="text-danger">(*)</span></label>
                                <input id="price" type="number"  data-name-show="{{ __('backend/other-service.price') }}" step="0.01" class="custom-validate form-control form-control-lg @error('price') is-invalid @enderror"
                                       name="price" value="{{ old('price') ? old('price') : $data['price'] }}" min="0"
                                       required>

                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="text-center mt-3">
                        <button type="submit" class="btn btn-primary">{{ __('web.save') }}</button>
                    </div>
                </form>
            @else
                <p>{{ __('web.not-found', ['name' => __('backend.other-service')]) }}</p>
            @endif

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                validateForm(e);
            });
        });
    </script>
@endpush
