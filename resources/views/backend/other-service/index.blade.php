@extends('layouts.admin')

@section('title', __('backend.other-service'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('other-service') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6"><h3>{{ __('backend.other-service') }}</h3></div>
                <div class="col-6 text-right">
                    <a href="{{ route('backend.other-service.create') }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/other-service.name') }}</th>
                        <th>{{ __('backend/other-service.price') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}
    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: "{{ route('backend.other-service.index') }}?ajax",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'price', name: 'price', width: '120px', searchable: false,  render: function (data, type, row) {
                            var price = new String(row['price']);
                            price = price.split('.');
                            if (price[1] && price[1] == '00') price = price[0];
                            else price = row['price'];
                            return 'USD $'+ price;
                        }},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ]
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.other-service') }}');
        });
    </script>
@endpush