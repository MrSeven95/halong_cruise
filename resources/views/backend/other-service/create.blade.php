@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend.other-service')]))

@section('content')
{{ Breadcrumbs::render('other-service-create') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body container">
            <h3 class="text-center">{{ __('web.creating', ['name' => __('backend.other-service')]) }}</h3>

            <form class="mt-3" action="{{ route('backend.other-service.store') }}" method="post">
                @csrf

                <div class="row pt-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">{{ __('backend/other-service.name') }} <span class="text-danger">(*)</span></label>
                            <input id="name" type="text" data-name-show="{{ __('backend/other-service.name') }}"  class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" autofocus maxlength="160"
                                   required>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="price">{{ __('backend/other-service.price') }} <span class="text-danger">(*)</span></label>
                            <input id="price" type="number" step="0.01" data-name-show="{{ __('backend/other-service.price') }}"  class="custom-validate form-control form-control-lg @error('price') is-invalid @enderror"
                                   name="price" value="{{ old('price') }}" min="0"
                                   required>

                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-primary">{{ __('web.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                validateForm(e);
            });
        });
    </script>
@endpush
