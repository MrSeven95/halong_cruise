@extends('layouts.admin')

@section('title', __('Dashboard'))

@push('after-styles')
    {!! Html::style('xtreme-admin/assets/libs/chartist/dist/chartist.min.css') !!}
    {!! Html::style('xtreme-admin/assets/extra-libs/c3/c3.min.css') !!}
    {!! Html::style('xtreme-admin/css/datatables.min.css') !!}

    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.css') }}
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.date.css') }}
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.time.css') }}

    <style>
        .dataTables_filter {
            display: none;
        }
        #sale_chart .btn.active {
            background: grey;
            color: white;
            font-weight: bold;
        }

         .select2-selection__rendered {
             line-height: 31px !important;
         }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .select2-selection__arrow {
            height: 34px !important;
        }

        .picker .picker__holder {
            top: 43px !important;
            bottom: unset !important;
        }
    </style>

@endpush

@section('content')
    {{ Breadcrumbs::render('dashboard') }}

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Sales chart -->
        <!-- ============================================================== -->
        <div class="row" id="sale_chart">
            <div class="col-12">
                <div class="card">
                    <div class="card-body pb-1">
                        <div class="col-4 float-left">
                            <div class="row">
                                <div class="col">
                                    <label class="control-label" for="time_send">From</label>
                                    <div class="input-group col pl-0">
                                        <input id="sale_chart_filter_from" value="" name="sale_chart_filter_from" class="form-control form-control-lg datepicker" type="text" placeholder="Default Date" >
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="control-label" for="time_send">To</label>
                                    <div class="input-group col pl-0">
                                        <input id="sale_chart_filter_to" value="" name="sale_chart_filter_to" class="form-control form-control-lg datepicker" type="text" placeholder="Default Date" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-8 col-md-4 float-right">
                            <div class="row">
                                <div class="col">
                                    <span class="float-right mb-2 mr-2">View By</span>
                                </div>
                                <div class="w-100"></div>
                                <div class="col btn-group">
                                    <button class="col btn btn-light active" data-view="day" id="btn_switch_chart_day">Day</button>
                                    <button class="col btn btn-light" data-view="week" id="btn_switch_chart_week">Week</button>
                                    <button class="col btn btn-light" data-view="month" id="btn_switch_chart_month">Month</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="campaign ct-charts"></div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Info Box -->
                    <!-- ============================================================== -->
                    <div class="card-body border-top">
                        <div class="row">
                            <!-- col -->
                            <div class="col-12 col-md-4">
                                <div class="d-flex align-items-center justify-content-center">
                                    <div class="m-r-10"><span class="text-orange display-5"><i class="mdi mdi-wallet"></i></span></div>
                                    <div><span>Bookings</span>
                                        <h3 class="font-medium m-b-0">567</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- col -->
                            <!-- col -->
                            <div class="col-12 col-md-4">
                                <div class="d-flex align-items-center justify-content-center">
                                    <div class="m-r-10"><span class="text-cyan display-5"><i class="mdi mdi-star-circle"></i></span></div>
                                    <div><span>Customers</span>
                                        <h3 class="font-medium m-b-0">769 adults, 150 children</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- col -->
                            <!-- col -->
                            <div class="col-12 col-md-4">
                                <div class="d-flex align-items-center justify-content-center">
                                    <div class="m-r-10"><span class="text-primary display-5"><i class="mdi mdi-currency-usd"></i></span></div>
                                    <div><span>Income (USD)</span>
                                        <h3 class="font-medium m-b-0">$23,568.90</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Table -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">List Bookings</h4>
                        <div class="row mt-3 mb-3">
                            <div class="col-sm-12 col-md-4 pr-0">
                                <label for="searchText" class="sr-only">Search</label>
                                <input type="text" id="list_bookings_search_input" class="form-control" placeholder="Search by customer name, email, order">
                            </div>
                            <div class="col-sm-12 col-md-4 pr-0">
                                <select name="search_booking_status"
                                        class="select2 form-control" id="list_bookings_search_select">
                                    <option></option>
                                    <option value="new">{{ __('backend/booking.status_new') }}</option>
                                    <option value="deposited">{{ __('backend/booking.status_deposited') }}</option>
                                    <option value="full_payment">{{ __('backend/booking.status_full_payment') }}</option>
                                    <option value="completed">{{ __('backend/booking.status_completed') }}</option>
                                    <option value="cancel">{{ __('backend/booking.status_cancel') }}</option>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-4 pl-1">
                                <button id="btn_search" class="btn btn-success ml-md-2">Search</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table no-wrap v-middle table-striped table-bordered display" id="table_list_bookings">
                                <thead>
                                <tr>
                                    <th>{{ __('No.') }}</th>
                                    <th>{{ __('backend/booking.full_name') }}</th>
                                    <th>{{ __('backend/booking.email') }} - {{ __('backend/booking.phone_number') }}</th>
                                    <th>{{ __('backend/booking.country') }}</th>
                                    <th>{{ __('backend/booking.time_start') }}</th>
                                    <th>{{ __('backend/booking.cruise') }}</th>
                                    <th>{{ __('backend/booking.cabin') }}</th>
                                    <th>{{ __('backend/booking.passengers') }}</th>
                                    <th>{{ __('backend/booking.amount') }}</th>
                                    <th>{{ __('backend/booking.status') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-scripts')

    {!! Html::script('xtreme-admin/assets/libs/chartist/dist/chartist.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') !!}
    {!! Html::script('xtreme-admin/assets/extra-libs/c3/d3.min.js') !!}
    {!! Html::script('xtreme-admin/assets/extra-libs/c3/c3.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/chart.js/dist/Chart.min.js') !!}

    {!! Html::script('xtreme-admin/pickadate/lib/picker.js') !!}
    {!! Html::script('xtreme-admin/pickadate/lib/picker.date.js') !!}
    {!! Html::script('xtreme-admin/pickadate/lib/picker.time.js') !!}

    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}

    <script>
        function saleChartFilterInit()
        {
            //
        }

        function chartistInit(data)
        {
            var chart = new Chartist.Line('.campaign', {
                labels: data['labels'],
                series: [
                    data['series']
                ]
            }, {
                low: data['low'],
                high: data['high'],

                showArea: true,
                fullWidth: true,
                plugins: [
                    Chartist.plugins.tooltip()
                ],
                axisY: {
                    onlyInteger: true,
                    scaleMinSpace: 40,
                    offset: 20,
                    labelInterpolationFnc: function(value) {
                        return (value / 1) + 'k';
                    }
                },
            });

            // Offset x1 a tiny amount so that the straight stroke gets a bounding box
            // Straight lines don't get a bounding box
            // Last remark on -> http://www.w3.org/TR/SVG11/coords.html#ObjectBoundingBox
            chart.on('draw', function(ctx) {
                if (ctx.type === 'area') {
                    ctx.element.attr({
                        x1: ctx.x1 + 0.001
                    });
                }
            });

            // Create the gradient definition on created event (always after chart re-render)
            chart.on('created', function(ctx) {
                var defs = ctx.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'gradient',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(255, 255, 255, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(64, 196, 255, 1)'
                });
            });
        }

        $(function () {
            "use strict";

            saleChartFilterInit();

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0');
            var yyyy = today.getFullYear();
            var current_time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0') + ":" + String(today.getSeconds()).padStart(2, '0');
            today = dd + '/' + mm + '/' + yyyy;

            var sale_chart_filter_from = $('#sale_chart_filter_from').pickadate({
                today: '',
                clear: '',
                close: '',
                format: 'dd/mm/yyyy'
            });
            if (sale_chart_filter_from.val() == '') {
                sale_chart_filter_from.val(today);
            }

            var sale_chart_filter_to = $('#sale_chart_filter_to').pickadate({
                today: '',
                clear: '',
                close: '',
                format: 'dd/mm/yyyy'
            });
            if (sale_chart_filter_to.val() == '') {
                sale_chart_filter_to.val(today);
            }

            var chart;
            $.ajax({
                url: '{{ route('backend.booking.ajaxDashboardChartist') }}?ajax',
                type: 'GET',
                dataType: 'json',
                data: {
                    'fromDate': $('#sale_chart_filter_from').val(),
                    'endDate': $('#sale_chart_filter_to').val(),
                    'typeView': $('#sale_chart .active').data('view')
                },
                success: function(data) {
                    if (data.status === 'success') {
                        chartistInit(data.dataChartist);
                    } else {
                        console.log(data);
                    }
                }
            });

            $('#sale_chart .btn').click(function() {
                $('#sale_chart .btn.active').removeClass('active');
                $(this).toggleClass('active');
                var typeView = $('#sale_chart .active').data('view'); // day, week, month

                switch (typeView) {
                    case 'day':
                        break;

                    case 'week':
                        break;

                    case 'month':
                        break;
                }
            });

            $('#list_bookings_search_select').select2({
                placeholder: "Select status",
            });

            var table = $('#table_list_bookings').DataTable({
                processing: false,
                serverSide: true,
                lengthChange: false,
                ajax: "{{ route('backend.booking.ajaxDashboardTable') }}?ajax",
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        width: '30px',
                        className: 'text-center',
                        orderable: false,
                        searchable: false
                    },
                    {data: 'full_name', name: 'full_name', searchable: true},
                    {data: 'email_phone_number', name: 'email_phone_number', searchable: true},
                    {data: 'country', name: 'country', searchable: false},
                    {data: 'time_start', name: 'time_start', searchable: true},
                    {data: 'cruise_name', name: 'cruise_name', searchable: true},
                    {data: 'cabin_name', name: 'cabin_name', searchable: true},
                    {data: 'passenger', name: 'passenger', searchable: false},
                    {data: 'amount', className: 'text-center', name: 'amount', searchable: false,  render: function (data, type, row) {
                            var price = new String(row['amount']);
                            price = price.split('.');
                            if (price[1] && price[1] == '00') price = price[0];
                            else price = row['price'];
                            return 'USD $'+ price;
                        }},
                    {data: 'status', name: 'status', className: 'text-center', width: '120px', searchable: true, render: function (data, type, row) {
                            switch (row['status']) {
                                case 'new':
                                    return '<span class="label label-primary">{{ __('backend/booking.status_new') }}</span>';
                                    break;
                                case 'deposited':
                                    return '<span class="label label-warning">{{ __('backend/booking.status_deposited') }}</span>';
                                    break;
                                case 'full_payment':
                                    return '<span class="label label-success">{{ __('backend/booking.status_full_payment') }}</span>';
                                    break;
                                case 'completed':
                                    return '<span class="label label-info">{{ __('backend/booking.status_completed') }}</span>';
                                    break;
                                case 'cancel':
                                    return '<span class="label label-danger">{{ __('backend/booking.status_cancel') }}</span>';
                                    break;
                            }
                            return '';
                        }},
                ]
            });

            $('#btn_search').click(function (e) {
                table.columns(9).search($('#list_bookings_search_select').val()).draw();
                table.search( this.value ).draw();
            });
        });
    </script>
@endpush