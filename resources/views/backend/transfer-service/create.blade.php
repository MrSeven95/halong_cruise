@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend.transfer-service')]))

@section('content')
{{ Breadcrumbs::render('transfer-service-create') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body container">
            <h3 class="text-center">{{ __('web.creating', ['name' => __('backend.transfer-service')]) }}</h3>

            <form class="mt-3" action="{{ route('backend.transfer-service.store') }}" method="post">
                @csrf

                <div class="row pt-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">{{ __('backend/transfer-service.name') }} <span class="text-danger">(*)</span></label>
                            <input id="name" type="text" data-name-show="{{ __('backend/transfer-service.name') }}"  class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" autofocus maxlength="160"
                                   required>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row pt-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="type_transfer">{{ __('backend/transfer-service.type_transfer') }} <span class="text-danger">(*)</span></label>
                            <select name="type_transfer" id="type_transfer"  data-name-show="{{ __('backend/transfer-service.type_transfer') }}" class="select2 custom-validate form-control form-control-lg @error('type_transfer') is-invalid @enderror" required>
                                <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                <option value="{{ __('backend/transfer-service.type_transfer_1') }}"{{ old('type_transfer') == __('backend/transfer-service.type_transfer_1') ? ' selected' : '' }}>{{ __('backend/transfer-service.type_transfer_1') }}</option>
                                <option value="{{ __('backend/transfer-service.type_transfer_2') }}"{{ old('type_transfer') == __('backend/transfer-service.type_transfer_1') ? ' selected' : '' }}>{{ __('backend/transfer-service.type_transfer_2') }}</option>
                                <option value="{{ __('backend/transfer-service.type_transfer_3') }}"{{ old('type_transfer') == __('backend/transfer-service.type_transfer_1') ? ' selected' : '' }}>{{ __('backend/transfer-service.type_transfer_3') }}</option>
                            </select>

                            <input type="hidden" name="type_price" id="type_price" value="{{ old('type_price') }}">

                            @error('type_transfer')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="price"></label>
                            <input id="price" type="number" step="0.01"  data-name-show="{{ __('backend/transfer-service.price') }}" class="custom-validate form-control form-control-lg @error('price') is-invalid @enderror"
                                   name="price" value="{{ old('price') }}" min="0"
                                   required>

                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-primary">{{ __('web.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            function doSelectPrice() {
                var txt = ' <span class="text-danger">(*)</span>';
                if ($('#type_transfer').val()) {
                    if ($('#type_transfer').val() == '{{ __('backend/transfer-service.type_transfer_1') }}') {
                        $('#type_price').val('{{ __('backend/transfer-service.type_price_1') }}');
                        $('label[for=price]').html('{{ __('backend/transfer-service.price_1') }}'+ txt);
                    } else if ($('#type_transfer').val() == '{{ __('backend/transfer-service.type_transfer_2') }}') {
                        $('#type_price').val('{{ __('backend/transfer-service.type_price_2') }}');
                        $('label[for=price]').html('{{ __('backend/transfer-service.price_2') }}'+ txt);
                    } else if ($('#type_transfer').val() == '{{ __('backend/transfer-service.type_transfer_3') }}') {
                        $('#type_price').val('{{ __('backend/transfer-service.type_price_2') }}');
                        $('label[for=price]').html('{{ __('backend/transfer-service.price_2') }}'+ txt);
                    }
                } else {
                    $('#type_price').val('');
                    $('label[for=price]').html('{{ __('backend/transfer-service.price') }}'+ txt);
                }
            }
            doSelectPrice();

            $('#type_transfer').change(function (e) {
                doSelectPrice();
            });

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                validateForm(e);
            });
        });
    </script>
@endpush
