@extends('layouts.admin')

@section('title', __('backend.bed-type'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('bed-type') }}

<div class="container-fluid">
    <div class="modal modal-create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('web.creating', ['name' => __('backend.bed-type')]) }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body container">
                    <form class="mt-3" action="{{ route('backend.bed-type.store') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="name">{{ __('backend/bed-type.name') }} <span class="text-danger">(*)</span></label>
                            <input id="name" type="text" data-name-show="{{ __('backend/bed-type.name') }}" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" autofocus maxlength="160"
                                   required>

                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </div>

                        <div class="text-center mt-3">
                            <button class="btn btn-primary" type="submit">{{ __('web.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('web.editing', ['name' => __('backend.bed-type')]) }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body container">
                    <form class="mt-3" action="" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="name">{{ __('backend/bed-type.name') }} <span class="text-danger">(*)</span></label>
                            <input id="name" type="text" data-name-show="{{ __('backend/bed-type.name') }}" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                   name="name" value="" autofocus maxlength="100"
                                   required>

                            <span class="invalid-feedback d-none" role="alert">
                                <strong></strong>
                            </span>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-primary" type="submit">{{ __('web.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6"><h3>{{ __('backend.bed-type') }}</h3></div>
                <div class="col-6 text-right">
                    <a href="{{ route('backend.bed-type.create') }}" class="btn btn-primary" data-toggle="modal" data-target=".modal-create">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/bed-type.name') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: "{{ route('backend.bed-type.index') }}?ajax",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ]
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.bed-type') }}');

            listenValidateForm();

            initModal(table, '{{ __('backend.bed-type') }}');
        });
    </script>
@endpush