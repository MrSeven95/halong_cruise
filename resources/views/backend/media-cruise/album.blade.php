@extends('layouts.admin')

@section('title', __('backend/cruise.album'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/sweetalert2.min.css') }}
    {{ Html::style('xtreme-admin/assets/libs/magnific-popup/dist/magnific-popup.css') }}
    <style>
        .inputDnD {
            justify-content: center;
            border-bottom: 2px dashed rgb(54, 190, 166);
        }
        .inputDnD .form-control-file {
            position: relative;
            width: 100%;
            height: 100%;
            min-height: 7em;
            outline: none;
            visibility: hidden;
            cursor: pointer;
            background-color: #c61c23;
            box-shadow: 0 0 5px solid currentColor;
        }
        .inputDnD .form-control-file:before {
            content: attr(data-title);
            position: absolute;
            top: 0.5em;
            left: 0;
            width: 100%;
            min-height: 7em;
            line-height: 2em;
            padding-top: 1.5em;
            opacity: 1;
            visibility: visible;
            text-align: center;
            border: 0.25em dashed currentColor;
            transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
            overflow: hidden;
        }
        .inputDnD .form-control-file:hover:before {
            border-style: solid;
            box-shadow: inset 0px 0px 0px 0.25em currentColor;
        }
        .inputDnD img {
            position: absolute;
            height: 70px;
            margin-top: 10px;
        }

        .item .card {
            height: 300px;
            overflow: hidden;
            background: #ddd;
            justify-content: center;
        }
        .item:hover {
            cursor: pointer !important;
        }
    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('cruise-album', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">

            @if ($data)

                <div class="row mb-3">
                    <div class="col-12"><h3 class="text-center">{{ __('backend/cruise.album') }} {{ $data['name'] }}</h3></div>
                    <div class="col-12">
                        <div class="row m-b-1">
                            <div class="col-sm-6 offset-sm-3">
                                <form id="form_upload" class="" action="{{ route('backend.media-cruise.album.store', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group inputDnD d-flex">
                                        <label class="sr-only" for="inputFile">File Upload</label>
                                        <img src="/images/drag_image.png" alt="Upload image">
                                        <input type="file" name="image[]" multiple class="form-control-file text-success font-weight-bold" id="inputFile" accept="image/*">
                                        <input type="hidden" name="cruise_id" value="{{ $data['id']}}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center container-fluid">

                    @if ($medias->count() > 0)
                        <div class="row el-element-overlay" id="list_images" style="width: 100%;">
                            @foreach ($medias as $media)
                                <div class="col-lg-3 col-md-6 item" data-remote-delete="{{ route('backend.media-cruise.destroy', ['id' => $media->id]) }}" data-id="{{ $media->id }}" data-display-ranking="{{ $media->display_ranking }}">
                                    <div class="card">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{!! $media->url !!}" alt="Image {{ $loop->index }}">
                                                <div class="el-overlay">
                                                    <ul class="list-style-none el-info">
                                                        <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="{!! $media->url !!}"><i class="icon-magnifier"></i></a></li>
                                                        <li class="el-item"><a class="btn-remove-image btn default btn-outline el-link" data-media-id="{{ $media->id }}" href="javascript:void(0);"><i class="ti-trash"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <p>{{ __('backend/cruise.album-not-found') }}</p>
                    @endif
                </div>

            @else
                <p>{{ __('web.not-found', ['name' => __('backend/cruise.album')]) }}</p>
            @endif
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/sweetalert2.all.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/magnific-popup/meg.init.js') !!}
    {!! Html::script('js/Sortable.js') !!}

    <script>
        $(document).ready(function () {

            @if ($medias->count() > 0)

            new Sortable(document.getElementById('list_images'), {
                swap: true,
                swapClass: 'highlight',
                animation: 150,
                onEnd: function (evt) {
                    var itemEl = evt.item;
                    var pre = evt.oldIndex + 1;
                    var after = evt.newIndex + 1;
                    pre = $('#list_images > div:nth-child('+ pre +')');
                    after = $('#list_images > div:nth-child('+ after +')');
                    preIdMedia = pre.data('id');
                    preDisplayRankingMedia = pre.data('display-ranking');
                    afterIdMedia = after.data('id');
                    afterDisplayRankingMedia = after.data('display-ranking');

                    $.ajax({
                        url: '{{ route('backend.media-cruise.album.swap-rank', ['id' => $data['id']]) }}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'preId': preIdMedia,
                            'afterId': afterIdMedia,
                            'preDisplayRanking': preDisplayRankingMedia,
                            'afterDisplayRanking': afterDisplayRankingMedia
                        },
                        success: function (data) {
                            if (data.status === 'success') {
                                toastr.success(convertMsg("Edit {{ __('backend/cruise.display_ranking') }} successful!"));
                            } else {
                                toastr.error(convertMsg("Edit {{ __('backend/cruise.display_ranking') }} successful!"));
                            }
                        },
                    });
                }
            });

            @endif

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __('web.invalid-image-type') }}');
                    return false;
                }
                return true;
            }

            $('#inputFile').change(function(e) {
                var files = $('#inputFile')[0].files;
                var upload_count = files.length;
                if (upload_count > 5) {
                    alert('{{ __('web.max-upload-album', ['max' => 5]) }}');
                    $(this).val('');
                    return false;
                }

                for (var i=0, f; f=files[i]; i++) {
                    var filename = f.name;
                    if (!checkImage(filename)) {
                        $(this).val('');
                        return false;
                    }
                }

                $('#form_upload').submit();
            });

            $('.btn-remove-image').click(function(e) {
                e.preventDefault();

                swal.fire({
                    title: '{{ __('web.confirm-delete') }}',
                    text: '{{ __('web.confirm-delete-detail') }}',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('web.accept') }}',
                    cancelButtonText: '{{ __('web.cancel') }}'
                }).then((result) => {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        var item = $(this).parents('.item');

                        if (item.data('id') > 0) {
                            $.ajax({
                                url: item.data('remote-delete'),
                                type: 'DELETE',
                                dataType: 'json',
                                data: {id: item.data('id')},
                                success: function (data) {
                                    if (data.status === 'success') {
                                        toastr.success(convertMsg("Delete image successful!"));
                                        item.remove();
                                    } else {
                                        toastr.error(convertMsg("Delete image failed!"));
                                    }
                                },
                            });
                        }
                    }
                });
            });
        });
    </script>
@endpush