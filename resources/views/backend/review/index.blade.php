@extends('layouts.admin')

@section('title', __('backend.review'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
    {{ Html::style('xtreme-admin/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}

    <style>
        input[type=range]::-webkit-slider-runnable-track {
            background: #2962ff !important;
        }
        input[type=range]:focus::-webkit-slider-runnable-track {
            background: #2962ff !important;
        }
        input[type=range]::-moz-range-track {
            background: #2962ff !important;
        }

    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('review') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6"><h3>{{ __('backend.review') }}</h3></div>
                <div class="col-6 text-right">
                    <a href="{{ route('backend.review.create') }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/review.name') }}</th>
                        <th>{{ __('backend/review.country') }}</th>
                        <th>{{ __('backend.cruise') }}</th>
                        <th>{{ __('backend/review.title') }}</th>
                        <th>{{ __('backend/review.time_send') }}</th>
                        <th>{{ __('backend/review.traveler_rating') }}</th>
                        <th>{{ __('backend/review.display_ranking') }}</th>
                        <th>{{ __('backend/review.status') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: {
                    url: "{{ route('backend.review.index') }}?ajax"
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'country', name: 'country'},
                    {data: 'cruise_name', name: 'cruise_name'},
                    {data: 'title', name: 'title'},
                    {data: 'time_send', name: 'time_send', width: '100px', className: 'text-center'},
                    {data: 'traveler_rating', className: 'text-center', name: 'traveler_rating', width: '30px', render: function(data, type, row) {
                            return row['traveler_rating'] +'/10';
                        }},
                    {data: 'display_ranking', className: 'text-center', name: 'display_ranking', width: '50px', searchable: false},
                    {data: 'col_status', className: 'px-0 text-center', name: 'col_status', orderable: false, width: '170px', searchable: false},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ],
                drawCallback: function (setting, json) {
                    setupBtSwitchDatatable();
                }
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.review') }}');
        });
    </script>
@endpush