@extends('layouts.admin')

@section('title', __('web.editing', ['name' => __('backend.review')]))

@push('after-styles')
    {!! Html::style('country-select-js-master/build/css/countrySelect.css') !!}

    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.css') }}
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.date.css') }}
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.time.css') }}

    <style>
        input[type=radio] {
            height: 1em !important;
            width: 1em !important;
        }
        .country-select {
            display: block !important;
        }
        #rating label:not(.control-label) {
            font-weight: 400;
        }
    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('review-edit') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">

            @if ($data)

            <form class="mt-3" action="{{ route('backend.review.update', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">

                <h3 class="card-title text-center">{{ __('web.editing', ['name' => __('backend.review')]) }}</h3>
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="cruise_id">{{ __('backend.cruise') }} <span class="text-danger">(*)</span></label>
                                    <select name="cruise_id" id="cruise_id" data-name-show="{{ __('backend.cruise') }}"  class="select2 custom-validate form-control form-control-lg @error('cruise_id') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        @foreach ($listCruises as $cruise)
                                            <option value="{{ $cruise->id }}"{{ old('cruise_id') ? (old('cruise_id') == $cruise->id ? ' selected' : '') : ($data['cruise_id'] == $cruise->id ? ' selected' : '') }}>{{ $cruise->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('cruise_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="display_ranking">{{ __('backend/review.display_ranking') }} <span class="text-danger">(*)</span></label>
                                    <input id="display_ranking" type="number" min="0" data-name-show="{{ __('backend/review.display_ranking') }}"  class="custom-validate form-control form-control-lg @error('display_ranking') is-invalid @enderror"
                                           name="display_ranking" value="{{ old('display_ranking') ? old('display_ranking') : $data['display_ranking'] }}" maxlength="100"
                                           required>

                                    @error('display_ranking')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="name">{{ __('backend/review.name') }} <span class="text-danger">(*)</span></label>
                                    <input id="name" type="text"  data-name-show="{{ __('backend/review.name') }}" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') ? old('name') : $data['name'] }}" maxlength="100"
                                           required>


                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="country">{{ __('backend/review.country') }} <span class="text-danger">(*)</span></label>
                                    <input type="text"  data-name-show="{{ __('backend/review.country') }}" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                           id="country" value="{{ old('country') ? old('country') : $data['country'] }}" name="country" required>

                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label class="control-label" for="time_send">Date <span class="text-danger">(*)</span></label>
                                            <div class="input-group col pl-0">
                                                {{--<div class="input-group-prepend">
                                                    <span class="input-group-text" id="btn_time_send_date">
                                                        <span class="ti-calendar"></span>
                                                    </span>
                                                </div>--}}
                                                <input id="time_send_date" value="{{ old('time_send_date') ? old('time_send_date') : \DateTime::createFromFormat('Y-m-d H:i:s', $data['time_send'])->format('d/m/Y') }}" name="time_send_date" class="form-control form-control-lg datepicker" type="text" placeholder="Default Date" >
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label class="control-label" for="time_send">Time <span class="text-danger">(*)</span></label>
                                            <div class="input-group col pl-0">
                                               {{-- <div class="input-group-prepend">
                                                    <span class="input-group-text" id="btn_time_send_hour">
                                                        <span class="ti-alarm-clock"></span>
                                                    </span>
                                                </div>--}}
                                                <input id="time_send_hour" value="{{ old('time_send_hour') ? old('time_send_hour') : \DateTime::createFromFormat('Y-m-d H:i:s', $data['time_send'])->format('H:i:s') }}" name="time_send_hour" type="text" class="form-control form-control-lg pickatime" placeholder="Default Hour">
                                            </div>
                                        </div>
                                    </div>

                                    @error('time_send')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">{{ __('backend/review.title') }} <span class="text-danger">(*)</span></label>
                                    <input id="title" type="text" data-name-show="{{ __('backend/review.title') }}"  class="custom-validate form-control form-control-lg @error('title') is-invalid @enderror"
                                           name="title" value="{{ old('title') ? old('title') : $data['title'] }}" maxlength="100"
                                           required>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="message">{{ __('backend/review.message') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="message" data-name-show="{{ __('backend/review.message') }}"  class="custom-validate form-control form-control-lg @error('message') is-invalid @enderror"
                                              name="message" required rows="6">{{ old('message') ? old('message') : $data['message'] }}</textarea>

                                    @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3" id="rating">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label for="traveler_rating" class="control-label ml-2 pl-0 col-md-6">{{ __('backend/review.traveler_rating') }}</label>
                                        <input type="hidden" name="traveler_rating">
                                        <label for="" class="control-label offset-md-2 col-md-2" id="traveler_rating_show"></label>
                                    </div>

                                    @error('traveler_rating')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="point_cruise_quality">{{ __('backend/review.point_cruise_quality') }}</label>
                                    <div class="row">
                                        <input type="range" min="0" max="10" step="1"  data-name-show="{{ __('backend/review.point_cruise_quality') }}" class="custom-validate ml-2 col-md-6 custom-range @error('point_cruise_quality') is-invalid @enderror" id="point_cruise_quality" name="point_cruise_quality" value="{{ old('point_cruise_quality') ? old('point_cruise_quality') : $data['point_cruise_quality'] }}">
                                        <label for="" class="offset-md-2 col-md-2 value_range"></label>
                                    </div>

                                    @error('point_cruise_quality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="point_food_and_drink">{{ __('backend/review.point_food_and_drink') }}</label>
                                    <div class="row">
                                        <input type="range" min="0" max="10" step="1" data-name-show="{{ __('backend/review.point_food_and_drink') }}"  class="custom-validate ml-2 col-md-6 custom-range @error('point_food_and_drink') is-invalid @enderror" id="point_food_and_drink" name="point_food_and_drink" value="{{ old('point_food_and_drink') ? old('point_food_and_drink') : $data['point_food_and_drink'] }}">
                                        <label for="" class="offset-md-2 col-md-2 value_range"></label>
                                    </div>

                                    @error('point_food_and_drink')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="point_cabin_quality">{{ __('backend/review.point_cabin_quality') }}</label>
                                    <div class="row">
                                        <input type="range" min="0" max="10" step="1" data-name-show="{{ __('backend/review.point_cabin_quality') }}"  class="custom-validate ml-2 col-md-6 custom-range @error('point_cabin_quality') is-invalid @enderror" id="point_cabin_quality" name="point_cabin_quality" value="{{ old('point_cabin_quality') ? old('point_cabin_quality') : $data['point_cabin_quality'] }}">
                                        <label for="" class="offset-md-2 col-md-2 value_range"></label>
                                    </div>

                                    @error('point_cabin_quality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="point_customer_service">{{ __('backend/review.point_customer_service') }}</label>
                                    <div class="row">
                                        <input type="range" min="0" max="10" step="1" data-name-show="{{ __('backend/review.point_customer_service') }}"  class="custom-validate ml-2 col-md-6 custom-range @error('point_customer_service') is-invalid @enderror" id="point_customer_service" name="point_customer_service" value="{{ old('point_customer_service') ? old('point_customer_service') : $data['point_customer_service'] }}">
                                        <label for="" class="offset-md-2 col-md-2 value_range"></label>
                                    </div>

                                    @error('point_customer_service')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="point_activities">{{ __('backend/review.point_activities') }}</label>
                                    <div class="row">
                                        <input type="range" min="0" max="10" step="1" data-name-show="{{ __('backend/review.point_activities') }}"  class="custom-validate ml-2 col-md-6 custom-range @error('point_activities') is-invalid @enderror" id="point_activities" name="point_activities" value="{{ old('point_activities') ? old('point_activities') : $data['point_activities'] }}">
                                        <label for="" class="offset-md-2 col-md-2 value_range"></label>
                                    </div>

                                    @error('point_activities')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <label class="control-label" for="traveler_type">{{ __('backend/review.traveler_type') }} <span class="text-danger">(*)</span></label>
                                    <div class="w-100"></div>
                                    <div class="row mt-2 ml-0">
                                        <div class="form-check form-check-inline mb-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="traveler_type_1" class="custom-control-input" value="Mature couples" name="traveler_type"{{ old('traveler_type') ? (old('traveler_type') == 'Mature couples' ? ' checked' : 'Mature couples') : ($data['traveler_type'] == 'Mature couples' ? ' checked' : '') }}>
                                                <label class="custom-control-label" for="traveler_type_1">Mature couples</label>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="form-check form-check-inline mb-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="traveler_type_2" class="custom-control-input" value="Solo traveler" name="traveler_type"{{ old('traveler_type') ? (old('traveler_type') == 'Solo traveler' ? ' checked' : 'Solo traveler') : ($data['traveler_type'] == 'Solo traveler' ? ' checked' : '') }}>
                                                <label class="custom-control-label" for="traveler_type_2">Solo traveler</label>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="form-check form-check-inline mb-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="traveler_type_3" class="custom-control-input" value="Family with childrens" name="traveler_type"{{ old('traveler_type') ? (old('traveler_type') == 'Family with childrens' ? ' checked' : 'Family with childrens') : ($data['traveler_type'] == 'Family with childrens' ? ' checked' : '') }}>
                                                <label class="custom-control-label" for="traveler_type_3">Family with childrens</label>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="form-check form-check-inline mb-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="traveler_type_4" class="custom-control-input" value="Group of friends" name="traveler_type"{{ old('traveler_type') ? (old('traveler_type') == 'Group of friends' ? ' checked' : 'Group of friends') : ($data['traveler_type'] == 'Group of friends' ? ' checked' : '') }}>
                                                <label class="custom-control-label" for="traveler_type_4">Group of friends</label>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="form-check form-check-inline mb-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="traveler_type_5" class="custom-control-input" value="Young couples" name="traveler_type"{{ old('traveler_type') ? (old('traveler_type') == 'Young couples' ? ' checked' : 'Young couples') : ($data['traveler_type'] == 'Young couples' ? ' checked' : '') }}>
                                                <label class="custom-control-label" for="traveler_type_5">Young couples</label>
                                            </div>
                                        </div>
                                    </div>

                                    @error('traveler_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_traveler_type" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                    </div>
                </div>
            </form>
            @else
                <p>{{ __('web.not-found', ['name' => __('backend.review')]) }}</p>
            @endif

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/pickadate/lib/picker.js') !!}
    {!! Html::script('xtreme-admin/pickadate/lib/picker.date.js') !!}
    {!! Html::script('xtreme-admin/pickadate/lib/picker.time.js') !!}

    {!! Html::script('country-select-js-master/build/js/countrySelect.min.js') !!}

    <script>
        $(document).ready(function () {
            var time_send_date = $('#time_send_date').pickadate({
                today: '',
                clear: '',
                close: '',
                format: 'dd/mm/yyyy'
            });

            var time_send_hour = $('#time_send_hour').pickatime({
                format: 'H:i:00',
                interval: 15,
                clear: '',
                close: '',
            });

            $('#btn_time_send_date').click(function(e) {
                $('#time_send_date').pickadate('open');
            });
            $('#btn_time_send_hour').click(function(e) {
                $('#time_send_hour').pickatime('open');
            });

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            $("#country").countrySelect();

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                var checked = $("input[name=traveler_type]:checked").length;
                if (checked == 0) {
                    var msg = 'The {{ __('backend/review.traveler_type') }} field is required.';
                    addErrorValidateForm($('#validate_traveler_type'), msg);
                }

                validateForm(e);
            });

            function calScore() {
                var score = 0;
                $.each($('.value_range'), function (i, e) {
                    var val = parseInt($(e).parent().find('input').val());
                    $(e).html(val + '/10');
                    score += val;
                });

                score = score/5;
                score = Math.round( score * 10 ) / 10;

                $('input[name=traveler_rating]').val(score);
                $('#traveler_rating_show').html(score + '/10');
            }
            calScore();

            $('input[type=range]').change(function() {
                calScore();
            });
        });
    </script>
@endpush
