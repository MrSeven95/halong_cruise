@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend.cruise-promotion')]))

@push('after-styles')
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.css') }}
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.date.css') }}
    {{ Html::style('xtreme-admin/pickadate/lib/themes/classic.time.css') }}
@endpush

@section('content')
    {{ Breadcrumbs::render('cruise-promotion-edit') }}

    <div class="container-fluid">
        <div class="card">
            <div class="card-body container">
                <h3 class="card-title text-center">{{ __('web.editing', ['name' => __('backend.cruise-promotion')]) }}</h3>

                @if ($data)

                <form class="mt-3" action="{{ route('backend.cruise-coupon.update', $data['id']) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row pt-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="name">{{ __('backend/cruise-promotion.cruise') }} <span class="text-danger">(*)</span></label>
                                <select name="cruise_id[]" multiple="multiple" id="cruise_id" class="select2 form-control form-control-lg @error('cruise_id') is-invalid @enderror" required>
                                    <option value="" disabled>{{ __('web.select-an-option') }}</option>
                                    <option value="-1">{{ __('backend/cruise-promotion.all-cruise') }}</option>
                                    @foreach ($listCruises as $cruise)
                                        <option value="{{ $cruise->id }}"{{ old('cruise_id') ? old('cruise_id') == $cruise->id ? ' selected' : '' : $data['cruise_id'] == $cruise->id ? ' selected' : '' }}>{{ $cruise->name }}</option>
                                    @endforeach
                                </select>

                                @error('cruise_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                                <span class="invalid-feedback d-none" id="validate_cruise_id" role="alert">
                                <strong></strong>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="coupon_id">{{ __('backend/cruise-promotion.coupon') }} <span class="text-danger">(*)</span></label>
                                <div class="w-100"></div>
                                @foreach ($listCoupons as $coupon)
                                    <div class="form-check form-check-inline mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="coupon_id_{{ $coupon->id }}" class="custom-control-input" value="{{ $coupon->id }}" name="coupon_id"{{ old('coupon_id') ? old('coupon_id') == $coupon->id ? ' checked' : '' : $data['coupon_id'] == $coupon->id ? ' checked' : '' }}>
                                            <label class="custom-control-label" for="coupon_id_{{ $coupon->id }}">{{ $coupon->name }}</label>
                                        </div>
                                    </div>
                                    <div class="w-100"></div>
                                @endforeach

                                @error('coupon_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                                <span class="invalid-feedback d-none" id="validate_coupon_id" role="alert">
                                <strong></strong>
                            </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="discount">{{ __('backend/cruise-promotion.discount') }} <span class="text-danger">(*)</span></label>
                                <input id="discount" type="number" min="0" max="100" data-name-show="{{ __('backend/cruise-promotion.discount') }}"  class="custom-validate form-control form-control-lg @error('discount') is-invalid @enderror"
                                       name="discount" value="{{ old('discount') ? old('discount') : $data['discount'] }}"
                                       required>

                                @error('discount')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="from">{{ __('backend/cruise-promotion.from') }} <span class="text-danger">(*)</span></label>

                                <div class="input-group col pl-0">
                                    <input id="from" value="{{ old('from') ? old('from') : \DateTime::createFromFormat('Y-m-d', $data['from'])->format('d/m/Y') }}" name="from" class="form-control form-control-lg datepicker @error('from') is-invalid @enderror" type="text" placeholder="Default Date" >
                                </div>

                                @error('from')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback d-none" id="validate_from" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="to">{{ __('backend/cruise-promotion.to') }} <span class="text-danger">(*)</span></label>

                                <div class="input-group col pl-0">
                                    <input id="to" value="{{ old('to') ? old('to') : \DateTime::createFromFormat('Y-m-d', $data['to'])->format('d/m/Y') }}" name="to" class="form-control form-control-lg datepicker @error('to') is-invalid @enderror" type="text" placeholder="Default Date" >
                                </div>

                                @error('to')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback d-none" id="validate_to" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="text-center mt-3">
                        <button type="submit" class="btn btn-primary">{{ __('web.save') }}</button>
                    </div>
                </form>

                @else
                    <p>{{ __('web.not-found', ['name' => __('backend.cruise-promotion')]) }}</p>
                @endif

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/pickadate/lib/picker.js') !!}
    {!! Html::script('xtreme-admin/pickadate/lib/picker.date.js') !!}
    {!! Html::script('xtreme-admin/pickadate/lib/picker.time.js') !!}

    <script>
        $(document).ready(function () {
            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            $('#cruise_id').on('select2:select', function(e) {
                var arr = $(this).val();
                if (arr.includes("-1")) {
                    $(this).val(-1);
                    $(this).trigger('change');
                }
            });

            listenValidateForm();

            $('button[type=submit]').click(function (e) {
                var checked = $("input[name=coupon_id]:checked").length;
                if (checked == 0) {
                    var msg = 'The {{ __('backend/cruise-promotion.coupon') }} field is required.';
                    addErrorValidateForm($('#validate_coupon_id'), msg);
                }

                var selected = $("#cruise_id").val().length;
                if (selected == 0) {
                    var msg = 'The {{ __('backend/cruise-promotion.cruise') }} field is required.';
                    addErrorValidateForm($('#validate_cruise_id'), msg);
                }

                if ($('#cruise_id').val().includes("-1")) {
                    var allCruises = [];
                    $.each($('#cruise_id option'), function(i,e) {
                        var val = $(e).attr('value');
                        if (val != -1) allCruises.push(val);
                    });
                    $('#cruise_id').val(allCruises);
                }

                if ($('#from').val() != '' && $('#to').val() != '') {
                    var partsFrom = $('#from').val().split('/');
                    var partsTo = $('#to').val().split('/');

                    var fromDate = new Date(partsFrom[2], partsFrom[1] - 1, partsFrom[0]);
                    var toDate = new Date(partsTo[2], partsTo[1] - 1, partsTo[0]);

                    if (fromDate > toDate) {
                        var msg = 'The to date field must be greater than the from date field.';
                        addErrorValidateForm($('#validate_to'), msg);
                    }
                }

                validateForm(e);
            });

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0');
            var yyyy = today.getFullYear();
            var current_time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0') + ":" + String(today.getSeconds()).padStart(2, '0');
            today = dd + '/' + mm + '/' + yyyy;

            function getYesterdaysDate() {
                var date = new Date();
                date.setDate(date.getDate()-1);
                return date;
            }

            var from = $('#from').pickadate({
                today: '',
                clear: '',
                close: '',
                format: 'dd/mm/yyyy',
                disable: [
                    { from: -10000, to: getYesterdaysDate() }
                ]
            });
            if (from.val() == '') {
                from.val(today);
            }

            var to = $('#to').pickadate({
                today: '',
                clear: '',
                close: '',
                format: 'dd/mm/yyyy',
                disable: [
                    { from: -10000, to: getYesterdaysDate() }
                ]
            });
            if (to.val() == '') {
                to.val(today);
            }
        });
    </script>
@endpush
