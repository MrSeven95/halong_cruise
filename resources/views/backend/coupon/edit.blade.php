@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend.coupon')]))

@section('content')
{{ Breadcrumbs::render('coupon-edit') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body container">
            <h3 class="card-title text-center">{{ __('web.editing', ['name' => __('backend.coupon')]) }}</h3>

            @if ($data)
                <form class="mt-3" action="{{ route('backend.coupon.update', ['id' => $data['id']]) }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row pt-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">{{ __('backend/coupon.name') }} <span class="text-danger">(*)</span></label>
                                <select name="name" id="name"  data-name-show="{{ __('backend/coupon.name') }}" class="select2 custom-validate form-control form-control-lg @error('name') is-invalid @enderror" required>
                                    <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                    <option value="{{ __('backend/coupon.type_early_bird') }}"{{ old('name') ? old('name') == __('backend/coupon.type_early_bird') ? ' selected' : '' : $data['name'] == __('backend/coupon.type_early_bird') ? ' selected' : '' }}>{{ __('backend/coupon.type_early_bird') }}</option>
                                    <option value="{{ __('backend/coupon.type_last_minute') }}"{{ old('name') ? old('name') == __('backend/coupon.type_last_minute') ? ' selected' : '' : $data['name'] == __('backend/coupon.type_last_minute') ? ' selected' : '' }}>{{ __('backend/coupon.type_last_minute') }}</option>
                                    <option value="{{ __('backend/coupon.type_summer_sale') }}"{{ old('name') ? old('name') == __('backend/coupon.type_summer_sale') ? ' selected' : '' : $data['name'] == __('backend/coupon.type_summer_sale') ? ' selected' : '' }}>{{ __('backend/coupon.type_summer_sale') }}</option>
                                </select>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                        {{--<div class="col-md-6">
                            <div class="form-group">
                                <label for="value_coupon">{{ __('backend/coupon.value') }} <span class="text-danger">(*)</span></label>
                                <input id="value_coupon" type="number" class="custom-validate form-control form-control-lg @error('value_coupon') is-invalid @enderror"
                                       name="value_coupon" value="{{ old('value_coupon') ? old('value_coupon') : $data['value_coupon'] }}" min="1" max="100"
                                       required>

                                @error('value_coupon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>--}}
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                    </div>
                </form>
            @else
                <p>{{ __('web.not-found', ['name' => __('backend.coupon')]) }}</p>
            @endif
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            listenValidateForm();

            $('button[type=submit]').click(function (e) {
                validateForm(e);
            });
        });
    </script>
@endpush
