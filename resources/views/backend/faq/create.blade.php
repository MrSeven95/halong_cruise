@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend.faq')]))

@section('content')
{{ Breadcrumbs::render('faq-create') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <form class="mt-3" action="{{ route('backend.faq.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <h3 class="card-title text-center">{{ __('web.creating', ['name' => __('backend.faq')]) }}</h3>
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="question">{{ __('backend/faq.question') }} <span class="text-danger">(*)</span></label>
                                    <input id="question" type="text" data-name-show="{{ __('backend/faq.question') }}"  class="custom-validate form-control form-control-lg @error('question') is-invalid @enderror"
                                           name="question" value="{{ old('question') }}" maxlength="100"
                                           required>

                                    @error('question')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="subject">{{ __('backend/faq.subject') }} <span class="text-danger">(*)</span></label>
                                    <select name="subject" data-name-show="{{ __('backend/faq.subject') }}" id="subject" data-name-show="{{ __('backend/faq.subject') }}"  class="select2 custom-validate form-control form-control-lg @error('subject') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        <option value="Billing and Payments"{{ old('subject') == 'Billing and Payments' ? ' selected' : '' }}>Billing and Payments</option>
                                        <option value="Booking and Reservation"{{ old('subject') == 'Booking and Reservation' ? ' selected' : '' }}>Booking and Reservation</option>
                                        <option value="Cancellation and Refund"{{ old('subject') == 'Cancellation and Refund' ? ' selected' : '' }}>Cancellation and Refund</option>
                                        <option value="Getting on board"{{ old('subject') == 'Getting on board' ? ' selected' : '' }}>Getting on board</option>
                                        <option value="Plan your trip"{{ old('subject') == 'Plan your trip' ? ' selected' : '' }}>Plan your trip</option>
                                        <option value="Transportation"{{ old('subject') == 'Transportation' ? ' selected' : '' }}>Transportation</option>
                                    </select>

                                    @error('subject')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="answer">{{ __('backend/faq.answer') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="answer" name="answer" required
                                              class="form-control form-control-lg">{{ old('answer') }}&nbsp;</textarea>

                                    @error('answer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_answer" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="card-body text-center">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

    <script>
        $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route('backend.uploadPhoto').'?_token='.csrf_token() !!}';
            CKEDITOR.config.height = '300px';
            CKEDITOR.replace(document.querySelector( '#answer' ));

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.answer.getData() == '') {
                    var msg = 'The {{ __('backend/faq.answer') }} field is required.';
                    addErrorValidateForm($('#validate_answer'), msg);
                }

                validateForm(e);
            });
        });
    </script>
@endpush
