@extends('layouts.admin')

@section('title', __('backend.faq'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
    {{ Html::style('xtreme-admin/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('faq') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-6"><h3>{{ __('backend.faq') }}</h3></div>
                <div class="col-6 text-right">
                    <a href="{{ route('backend.faq-config.edit') }}" class="btn btn-success">{{ __('backend.faq-config') }}</a>
                    <a href="{{ route('backend.faq.create') }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/faq.question') }}</th>
                        <th>{{ __('backend/faq.subject') }}</th>
                        <th>{{ __('backend/faq.status') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: "{{ route('backend.faq.index') }}?ajax",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'question', name: 'question', orderable: false},
                    {data: 'subject', name: 'subject', width: '200px'},
                    {data: 'col_status', name: 'col_status', className: 'px-0 text-center', width: '170px', searchable: false},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ],
                drawCallback: function (setting, json) {
                    setupBtSwitchDatatable();
                }
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.faq') }}');
        });
    </script>
@endpush