@extends('layouts.admin')

@section('title', __('backend/cruise.journal'))

@section('content')
{{ Breadcrumbs::render('cruise-journal', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body container">

            <div class="row">
                <div class="col-6"><h3>{{ __('backend/cruise.journal') }}</h3></div>
                <div class="btn-create col-6 text-right @if (($data['has_journal_type1']== 1 && $data['has_journal_type2']== 1)) d-none @endif">
                    <a href="{{ route('backend.cruise.journal.create', ['id' => $data['id']]) }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>

                <div class="table-responsive mt-3">
                    <table id="journalList" class="table table-striped table-bordered display" style="width:100%">
                        <thead>
                        <tr>
                            <th>{{ __('backend/cruise.journal') }}</th>
                            <th width="180px">{{ __('web.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if ($data['has_journal_type1']== 1)
                            <tr id="type1">
                                <td>{{ __('backend/cruise.journal_type1') }}</td>
                                <td>
                                    <a href="{{ route('backend.cruise.journal.edit-type-1', ['id' => $data['id']]) }}"
                                               class="btn btn-primary btn-sm">{{ __('web.edit') }}</a>
                                    <a href="#" data-remote="{{ route('backend.cruise.journal.destroy-type-1', ['id' => $data['id']]) }}"
                                                class="btn btn-dark btn-sm btn-delete text-white">{{ __('web.delete') }}</a>
                                </td>
                            </tr>
                        @endif
                        @if ($data['has_journal_type2']== 1)
                            <tr id="type2">
                                <td>{{ __('backend/cruise.journal_type2') }}</td>
                                <td>
                                    <a href="{{ route('backend.cruise.journal.edit-type-2', ['id' => $data['id']]) }}"
                                       class="btn btn-primary btn-sm">{{ __('web.edit') }}</a>
                                    <a href="#" data-remote="{{ route('backend.cruise.journal.destroy-type-2', ['id' => $data['id']]) }}"
                                       class="btn btn-dark btn-sm btn-delete text-white">{{ __('web.delete') }}</a>
                                </td>
                            </tr>
                        @endif

                        <tr id="type0" class="@if ($data['has_journal_type1']== 1 || $data['has_journal_type2']== 1) d-none @endif">
                            <td class="text-center">
                                No record found.
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(function () {
            "use strict";

            $(document).on('click', '.btn-delete', function(e){
                e.preventDefault();

                swal.fire({
                    title: '{{ __('web.confirm-delete') }}',
                    text: '{{ __('web.confirm-delete-detail') }}',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('web.accept') }}',
                    cancelButtonText: '{{ __('web.cancel') }}'
                }).then((result) => {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        let url = $(this).data('remote');
                        var $row = $(this).parents('tr');

                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            success: function (data) {
                                if (data.status === 'success') {
                                    swal('{{ __('web.deleted') }}', '{{ __('web.delete-successed', ['name' => __('backend/cruise.journal')]) }}', "success");
                                    $row.remove();

                                    var len = $('#type1').length + $('#type2').length;
                                    if (len == 0) {
                                        $('#type0').removeClass('d-none').show();
                                    }
                                    if (len == 2) {
                                        $('.btn-create').removeClass('d-block').addClass('d-none').hide();
                                    } else {
                                        $('.btn-create').removeClass('d-none').addClass('d-block').show();
                                    }
                                } else {
                                    swal('{{ __('web.cancelled') }}', '{{ __('web.delete-errored', ['name' => __('backend/cruise.journal')]) }}', "error");
                                }
                            },
                        });
                    }
                });
            });
        });
    </script>
@endpush