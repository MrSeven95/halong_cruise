@extends('layouts.admin')

@section('title', __('web.editing', ['name' => __('backend/cruise.journal')]))

@push('after-styles')
    <style>
        h3.text-left {
            margin-left: 10px;
            font-size: 16.5px;
            font-weight: 600;
        }
    </style>
@endpush

@section('content')
    {{ Breadcrumbs::render('cruise-journal-edit', $data['id'], $data['name']) }}

    <div class="container-fluid">
        <div class="card">
            <div class="card-body mx-5">
                <h3 class="card-title text-center">{{ __('web.editing', ['name' => __('backend/cruise.journal')]) }}</h3>

                @if ($data)

                    <form class="mt-3" action="{{ route('backend.cruise.journal.update-type-2', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-body">
                            <div class="card-body pt-0">

                                <div class="row"><h3 class="text-left">{{ __('backend/cruise.journey_day1') }}</h3></div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day1_title">{{ __('backend/cruise.journey_type2_day1_title') }} <span class="text-danger">(*)</span></label>
                                            <input id="journey_type2_day1_title" type="text"  data-name-show="{{ __('backend/cruise.journey_type2_day1_title') }}" class="custom-validate form-control form-control-lg @error('journey_type2_day1_title') is-invalid @enderror"
                                                   name="journey_type2_day1_title" value="{{ old('journey_type2_day1_title') ? old('journey_type2_day1_title') : $data['journey_type2_day1_title'] }}" autofocus maxlength="255"
                                                   required>

                                            @error('journey_type2_day1_title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day1_detail">{{ __('backend/cruise.journey_type2_day1_detail') }} <span class="text-danger">(*)</span></label>
                                            <textarea rows="6" id="journey_type2_day1_detail" class="form-control form-control-lg @error('journey_type2_day1_detail') is-invalid @enderror"
                                                      name="journey_type2_day1_detail" required>{{ old('journey_type2_day1_detail') ? old('journey_type2_day1_detail') : $data['journey_type2_day1_detail'] }}</textarea>

                                            @error('journey_type2_day1_detail')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                            <span class="invalid-feedback d-none" id="validate_journey_type2_day1_detail" role="alert">
                                        <strong></strong>
                                    </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type2_day1_image') }}</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" accept="image/*" class="type2_day1_album_upload custom-file-input form-control form-control-lg @error('type2_day1_album') is-invalid @enderror"
                                                           name="type2_day1_album[]" value="" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100"></div>

                                    <div class="card col-md-2 preview_image_album_type2_day1_old" data-image="0">
                                        <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day1_image_1'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day1_image_album_0" value="true" id="is_no_action_type2_day1_image_album_0">
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day1_old" data-image="1">
                                        <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day1_image_2'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day1_image_album_1" value="true" id="is_no_action_type2_day1_image_album_1">
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day1_old" data-image="2">
                                        <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day1_image_3'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day1_image_album_2" value="true" id="is_no_action_type2_day1_image_album_2">
                                    </div>

                                    <div class="card col-md-2 preview_image_album_type2_day1_new" data-image="0">
                                        <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day1_new" data-image="1">
                                        <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day1_new" data-image="2">
                                        <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mt-3"><h3 class="text-left">{{ __('backend/cruise.journey_day2') }}</h3></div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day2_title">{{ __('backend/cruise.journey_type2_day2_title') }} <span class="text-danger">(*)</span></label>
                                            <input id="journey_type2_day2_title" type="text"  data-name-show="{{ __('backend/cruise.journey_type2_day2_title') }}" class="custom-validate form-control form-control-lg @error('journey_type2_day2_title') is-invalid @enderror"
                                                   name="journey_type2_day2_title" value="{{ old('journey_type2_day2_title') ? old('journey_type2_day2_title') : $data['journey_type2_day2_title'] }}" autofocus maxlength="255"
                                                   required>

                                            @error('journey_type2_day2_title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day2_detail">{{ __('backend/cruise.journey_type2_day2_detail') }} <span class="text-danger">(*)</span></label>
                                            <textarea rows="6" id="journey_type2_day2_detail" class="form-control form-control-lg @error('journey_type2_day2_detail') is-invalid @enderror"
                                                      name="journey_type2_day2_detail" required>{{ old('journey_type2_day2_detail') ? old('journey_type2_day2_detail') : $data['journey_type2_day2_detail'] }}</textarea>

                                            @error('journey_type2_day2_detail')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                            <span class="invalid-feedback d-none" id="validate_journey_type2_day2_detail" role="alert">
                                        <strong></strong>
                                    </span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type2_day2_image') }}</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" accept="image/*" class="type2_day2_album_upload custom-file-input form-control form-control-lg @error('type2_day2_album') is-invalid @enderror"
                                                           name="type2_day2_album[]" value="" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100"></div>

                                    <div class="card col-md-2 preview_image_album_type2_day2_old" data-image="0">
                                        <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day2_image_1'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day2_image_album_0" value="true" id="is_no_action_type2_day2_image_album_0">
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day2_old" data-image="1">
                                        <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day2_image_2'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day2_image_album_1" value="true" id="is_no_action_type2_day2_image_album_1">
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day2_old" data-image="2">
                                        <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day2_image_3'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day2_image_album_2" value="true" id="is_no_action_type2_day2_image_album_2">
                                    </div>

                                    <div class="card col-md-2 preview_image_album_type2_day2_new" data-image="0">
                                        <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day2_new" data-image="1">
                                        <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day2_new" data-image="2">
                                        <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                </div>



                                <div class="row mt-3"><h3 class="text-left">{{ __('backend/cruise.journey_day3') }}</h3></div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day3_title">{{ __('backend/cruise.journey_type2_day3_title') }} <span class="text-danger">(*)</span></label>
                                            <input id="journey_type2_day3_title" type="text" data-name-show="{{ __('backend/cruise.journey_type2_day3_title') }}"  class="custom-validate form-control form-control-lg @error('journey_type2_day3_title') is-invalid @enderror"
                                                   name="journey_type2_day3_title" value="{{ old('journey_type2_day3_title') ? old('journey_type2_day3_title') : $data['journey_type2_day3_title'] }}" autofocus maxlength="255"
                                                   required>

                                            @error('journey_type2_day3_title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day3_detail">{{ __('backend/cruise.journey_type2_day3_detail') }} <span class="text-danger">(*)</span></label>
                                            <textarea rows="6" id="journey_type2_day3_detail" class="form-control form-control-lg @error('journey_type2_day3_detail') is-invalid @enderror"
                                                      name="journey_type2_day3_detail" required>{{ old('journey_type2_day3_detail') ? old('journey_type2_day3_detail') : $data['journey_type2_day3_detail'] }}</textarea>

                                            @error('journey_type2_day3_detail')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                            <span class="invalid-feedback d-none" id="validate_journey_type2_day3_detail" role="alert">
                                        <strong></strong>
                                    </span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type2_day3_image') }}</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" accept="image/*" class="type2_day3_album_upload custom-file-input form-control form-control-lg @error('type2_day3_album') is-invalid @enderror"
                                                           name="type2_day3_album[]" value="" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100"></div>

                                    <div class="card col-md-2 preview_image_album_type2_day3_old" data-image="0">
                                        <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day3_image_1'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day3_image_album_0" value="true" id="is_no_action_type2_day3_image_album_0">
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day3_old" data-image="1">
                                        <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day3_image_2'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day3_image_album_1" value="true" id="is_no_action_type2_day3_image_album_1">
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day3_old" data-image="2">
                                        <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src="{{ $data['journey_type2_day3_image_3'] }}"/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                        <input type="hidden" name="is_no_action_type2_day3_image_album_2" value="true" id="is_no_action_type2_day3_image_album_2">
                                    </div>

                                    <div class="card col-md-2 preview_image_album_type2_day3_new" data-image="0">
                                        <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day3_new" data-image="1">
                                        <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="card col-md-2 preview_image_album_type2_day3_new" data-image="2">
                                        <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src=""/>
                                        <div class="card-action position-relative">
                                            <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="card-body text-center">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                            </div>
                        </div>
                    </form>

                @else
                    <p>{{ __('web.not-found', ['name' => __('backend.cruise')]) }}</p>
                @endif

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

    <script>
        var type2_day1_old_list = [];
        var type2_day1_new_list = [];

        var type2_day2_old_list = [];
        var type2_day2_new_list = [];

        var type2_day3_old_list = [];
        var type2_day3_new_list = [];

        $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route('backend.uploadPhoto').'?_token='.csrf_token() !!}';
            CKEDITOR.config.height = '300px';

            CKEDITOR.replace(document.querySelector( '#journey_type2_day1_detail' ));
            CKEDITOR.replace(document.querySelector( '#journey_type2_day2_detail' ));
            CKEDITOR.replace(document.querySelector( '#journey_type2_day3_detail' ));

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.journey_type2_day1_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type2_day1_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type2_day1_detail'), msg);
                }
                if (CKEDITOR.instances.journey_type2_day2_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type2_day2_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type2_day2_detail'), msg);
                }
                if (CKEDITOR.instances.journey_type2_day3_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type2_day3_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type2_day3_detail'), msg);
                }

                if (type2_day1_new_list.length > 0) {
                    let list = new DataTransfer();
                    $.each(type2_day1_new_list, function (i, e) {
                        list.items.add(e);
                    });

                    $('.type2_day1_album_upload')[0].files = list.files;
                }
                if (type2_day2_new_list.length > 0) {
                    let list = new DataTransfer();
                    $.each(type2_day2_new_list, function (i, e) {
                        list.items.add(e);
                    });

                    $('.type2_day2_album_upload')[0].files = list.files;
                }
                if (type2_day3_new_list.length > 0) {
                    let list = new DataTransfer();
                    $.each(type2_day3_new_list, function (i, e) {
                        list.items.add(e);
                    });

                    $('.type2_day3_album_upload')[0].files = list.files;
                }

                validateForm(e);
            });

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __('web.invalid-image-type') }}');
                    return false;
                }
                return true;
            }

            // day

            initAlbum('type2_day1_', type2_day1_old_list, type2_day1_new_list);
            initAlbum('type2_day2_', type2_day2_old_list, type2_day2_new_list);
            initAlbum('type2_day3_', type2_day3_old_list, type2_day3_new_list);

            function initAlbum(type, old_list, new_list) {

                $.each($('.preview_image_album_'+ type +'old'), function (i, e) {
                    var src = $(e).find('img').attr('src');
                    if (src != '') {
                        $(e).show();
                        old_list.push(src);
                    } else {
                        $(e).hide();
                    }
                });
                $.each($('.preview_image_album_'+ type +'new'), function (i, e) {
                    $(e).hide();
                });
                $('.preview_image_album_'+ type +'old .remove_image_album').click(function (e) {
                    e.preventDefault();

                    var index = $(e.currentTarget).parents('.preview_image_album_'+ type +'old').data('image');

                    old_list.splice(index, 1);
                    $('#is_no_action_'+ type +'image_album_' + index.toString()).val('false');

                    $(e.currentTarget).parents('.preview_image_album_'+ type +'old').hide();
                });
                $('.preview_image_album_'+ type +'new .remove_image_album').click(function (e) {
                    e.preventDefault();

                    var index = $(e.currentTarget).parents('.preview_image_album_'+ type +'new').data('image');
                    new_list.splice(index, 1);

                    $(e.currentTarget).parents('.preview_image_album_'+ type +'new').hide();
                });

                $('.'+ type +'album_upload').change(function (e) {
                    var upload_count = $(e.currentTarget)[0].files.length;
                    if (upload_count + old_list.length + new_list.length > 3) {
                        alert('{{ __('web.max-upload-album', ['max' => 3]) }}');
                        $(this).val('');
                        return false;
                    }

                    $.each($('.preview_image_album_'+ type +'new'), function (i, e) {
                        $(e).hide();
                    });

                    var files = $('.'+ type +'album_upload')[0].files;
                    var count = 0;
                    for (var i = 0, f; f = files[i]; i++) {
                        var filename = f.name;
                        if (checkImage(filename)) {
                            new_list.push(f);
                        } else {
                            return false;
                        }
                    }

                    for (var i = 0, f; f = new_list[i]; i++) {
                        var $currentImage = $('.preview_image_album_'+ type +'new[data-image=' + i + ']');
                        $currentImage.find('img').attr('src', window.URL.createObjectURL(f));
                        $currentImage.show();
                    }
                });
            }
        });
    </script>
@endpush
