@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend/cruise.journal')]))

@push('after-styles')
    <style>
        h3.text-left {
            margin-left: 10px;
            font-size: 16.5px;
            font-weight: 600;
        }
    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('cruise-journal-create', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <h3 class="card-title text-center">{{ __('web.creating', ['name' => __('backend/cruise.journal')]) }}</h3>

            @if ($data)
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div id="type1">
                            <form class="mt-3" action="{{ route('backend.cruise.journal.store', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                                @csrf

                            <div class="row pt-3 mb-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="type_journal">{{ __('backend/cruise.journal') }} <span class="text-danger">(*)</span></label>
                                        <select name="type_journal" data-name-show="{{ __('backend/cruise.journal') }}"  class="select2 custom-validate form-control form-control-lg" required>
                                            <option value="1" selected>{{ __('backend/cruise.journal_type1') }}</option>
                                            <option value="2">{{ __('backend/cruise.journal_type2') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row"><h3 class="text-left">{{ __('backend/cruise.journey_day1') }}</h3></div>
                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="journey_type1_day1_title">{{ __('backend/cruise.journey_type1_day1_title') }} <span class="text-danger">(*)</span></label>
                                        <input id="journey_type1_day1_title" type="text" data-name-show="{{ __('backend/cruise.journey_type1_day1_title') }}"  class="custom-validate form-control form-control-lg @error('journey_type1_day1_title') is-invalid @enderror"
                                               name="journey_type1_day1_title" value="{{ old('journey_type1_day1_title') ? old('journey_type1_day1_title') : '' }}" autofocus maxlength="255"
                                               required>

                                        @error('journey_type1_day1_title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="journey_type1_day1_detail">{{ __('backend/cruise.journey_type1_day1_detail') }} <span class="text-danger">(*)</span></label>
                                        <textarea rows="6" id="journey_type1_day1_detail" class="form-control form-control-lg @error('journey_type1_day1_detail') is-invalid @enderror"
                                                  name="journey_type1_day1_detail" required>{{ old('journey_type1_day1_detail') ? old('journey_type1_day1_detail') : '' }}</textarea>

                                        @error('journey_type1_day1_detail')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <span class="invalid-feedback d-none" id="validate_journey_type1_day1_detail" role="alert">
                                            <strong></strong>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type1_day1_image') }}</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" accept="image/*" class="type1_day1_album_upload custom-file-input form-control form-control-lg @error('type1_day1_album') is-invalid @enderror"
                                                       name="type1_day1_album[]" value="" multiple>
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="w-100"></div>

                            <div class="card col-md-2 preview_image_album_type1_day1" data-image="0">
                                <img alt="{{ __('backend/cruise.journey_type1_day1_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                            <div class="card col-md-2 preview_image_album_type1_day1" data-image="1">
                                <img alt="{{ __('backend/cruise.journey_type1_day1_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                            <div class="card col-md-2 preview_image_album_type1_day1" data-image="2">
                                <img alt="{{ __('backend/cruise.journey_type1_day1_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>


                            <div class="row mt-3"><h3 class="text-left">{{ __('backend/cruise.journey_day2') }}</h3></div>
                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="journey_type1_day2_title">{{ __('backend/cruise.journey_type1_day2_title') }} <span class="text-danger">(*)</span></label>
                                        <input id="journey_type1_day2_title" type="text"  data-name-show="{{ __('backend/cruise.journey_type1_day2_title') }}" class="custom-validate form-control form-control-lg @error('journey_type1_day2_title') is-invalid @enderror"
                                               name="journey_type1_day2_title" value="{{ old('journey_type1_day2_title') ? old('journey_type1_day2_title') : '' }}" autofocus maxlength="255"
                                               required>

                                        @error('journey_type1_day2_title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="journey_type1_day2_detail">{{ __('backend/cruise.journey_type1_day2_detail') }} <span class="text-danger">(*)</span></label>
                                        <textarea rows="6" id="journey_type1_day2_detail" class="form-control form-control-lg @error('journey_type1_day2_detail') is-invalid @enderror"
                                                  name="journey_type1_day2_detail" required>{{ old('journey_type1_day2_detail') ? old('journey_type1_day2_detail') : ''}}</textarea>

                                        @error('journey_type1_day2_detail')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <span class="invalid-feedback d-none" id="validate_journey_type1_day2_detail" role="alert">
                                            <strong></strong>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row pt-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type1_day2_image') }}</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" accept="image/*" class="type1_day2_album_upload custom-file-input form-control form-control-lg @error('type1_day2_album') is-invalid @enderror"
                                                       name="type1_day2_album[]" value="" multiple>
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-100"></div>

                                <div class="card col-md-2 preview_image_album_type1_day2" data-image="0">
                                    <img alt="{{ __('backend/cruise.journey_type1_day2_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type1_day2" data-image="1">
                                    <img alt="{{ __('backend/cruise.journey_type1_day2_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type1_day2" data-image="2">
                                    <img alt="{{ __('backend/cruise.journey_type1_day2_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <div class="card-body text-center">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                                </div>
                            </div>
                            </form>
                        </div>

                        <div id="type2">
                            <form class="mt-3" action="{{ route('backend.cruise.journal.store', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="row pt-3 mb-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="type_journal">{{ __('backend/cruise.journal') }} <span class="text-danger">(*)</span></label>
                                            <select name="type_journal"  data-name-show="{{ __('backend/cruise.journal') }}" class="select2 custom-validate form-control form-control-lg" required>
                                                <option value="1">{{ __('backend/cruise.journal_type1') }}</option>
                                                <option value="2" selected>{{ __('backend/cruise.journal_type2') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row"><h3 class="text-left">{{ __('backend/cruise.journey_day1') }}</h3></div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day1_title">{{ __('backend/cruise.journey_type2_day1_title') }} <span class="text-danger">(*)</span></label>
                                            <input id="journey_type2_day1_title" type="text"  data-name-show="{{ __('backend/cruise.journey_type2_day1_title') }}" class="custom-validate form-control form-control-lg @error('journey_type2_day1_title') is-invalid @enderror"
                                                   name="journey_type2_day1_title" value="{{ old('journey_type2_day1_title') ? old('journey_type2_day1_title') : ''}}" autofocus maxlength="255"
                                                   required>

                                            @error('journey_type2_day1_title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day1_detail">{{ __('backend/cruise.journey_type2_day1_detail') }} <span class="text-danger">(*)</span></label>
                                            <textarea rows="6" id="journey_type2_day1_detail" class="form-control form-control-lg @error('journey_type2_day1_detail') is-invalid @enderror"
                                                      name="journey_type2_day1_detail" required>{{ old('journey_type2_day1_detail') ? old('journey_type2_day1_detail') : '' }}</textarea>

                                            @error('journey_type2_day1_detail')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            <span class="invalid-feedback d-none" id="validate_journey_type2_day1_detail" role="alert">
                                            <strong></strong>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type2_day1_image') }}</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" accept="image/*" class="type2_day1_album_upload custom-file-input form-control form-control-lg @error('type2_day1_album') is-invalid @enderror"
                                                           name="type2_day1_album[]" value="" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-100"></div>

                                <div class="card col-md-2 preview_image_album_type2_day1" data-image="0">
                                    <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type2_day1" data-image="1">
                                    <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type2_day1" data-image="2">
                                    <img alt="{{ __('backend/cruise.journey_type2_day1_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>



                                <div class="row"><h3 class="text-left">{{ __('backend/cruise.journey_day2') }}</h3></div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day2_title">{{ __('backend/cruise.journey_type2_day2_title') }} <span class="text-danger">(*)</span></label>
                                            <input id="journey_type2_day2_title" type="text"  data-name-show="{{ __('backend/cruise.journey_type2_day2_title') }}" class="custom-validate form-control form-control-lg @error('journey_type2_day2_title') is-invalid @enderror"
                                                   name="journey_type2_day2_title" value="{{ old('journey_type2_day2_title') ? old('journey_type2_day2_title') : ''}}" autofocus maxlength="255"
                                                   required>

                                            @error('journey_type2_day2_title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day2_detail">{{ __('backend/cruise.journey_type2_day2_detail') }} <span class="text-danger">(*)</span></label>
                                            <textarea rows="6" id="journey_type2_day2_detail" class="form-control form-control-lg @error('journey_type2_day2_detail') is-invalid @enderror"
                                                      name="journey_type2_day2_detail" required>{{ old('journey_type2_day2_detail') ? old('journey_type2_day2_detail') : '' }}</textarea>

                                            @error('journey_type2_day2_detail')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <span class="invalid-feedback d-none" id="validate_journey_type2_day2_detail" role="alert">
                                                <strong></strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type2_day2_image') }}</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" accept="image/*" class="type2_day2_album_upload custom-file-input form-control form-control-lg @error('type2_day2_album') is-invalid @enderror"
                                                           name="type2_day2_album[]" value="" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-100"></div>

                                <div class="card col-md-2 preview_image_album_type2_day2" data-image="0">
                                    <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type2_day2" data-image="1">
                                    <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type2_day2" data-image="2">
                                    <img alt="{{ __('backend/cruise.journey_type2_day2_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>



                                <div class="row"><h3 class="text-left">{{ __('backend/cruise.journey_day3') }}</h3></div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day3_title">{{ __('backend/cruise.journey_type2_day3_title') }} <span class="text-danger">(*)</span></label>
                                            <input id="journey_type2_day3_title" type="text"  data-name-show="{{ __('backend/cruise.journey_type2_day3_title') }}" class="custom-validate form-control form-control-lg @error('journey_type2_day3_title') is-invalid @enderror"
                                                   name="journey_type2_day3_title" value="{{ old('journey_type2_day3_title') ? old('journey_type2_day3_title') : ''}}" autofocus maxlength="255"
                                                   required>

                                            @error('journey_type2_day3_title')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="journey_type2_day3_detail">{{ __('backend/cruise.journey_type2_day3_detail') }} <span class="text-danger">(*)</span></label>
                                            <textarea rows="6" id="journey_type2_day3_detail" class="form-control form-control-lg @error('journey_type2_day3_detail') is-invalid @enderror"
                                                      name="journey_type2_day3_detail" required>{{ old('journey_type2_day3_detail') ? old('journey_type2_day3_detail') : '' }}</textarea>

                                            @error('journey_type2_day3_detail')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <span class="invalid-feedback d-none" id="validate_journey_type2_day3_detail" role="alert">
                                                    <strong></strong>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image_album" class="control-label">{{ __('backend/cruise.journey_type2_day3_image') }}</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" accept="image/*" class="type2_day3_album_upload custom-file-input form-control form-control-lg @error('type2_day3_album') is-invalid @enderror"
                                                           name="type2_day3_album[]" value="" multiple>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-100"></div>

                                <div class="card col-md-2 preview_image_album_type2_day3" data-image="0">
                                    <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type2_day3" data-image="1">
                                    <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                                <div class="card col-md-2 preview_image_album_type2_day3" data-image="2">
                                    <img alt="{{ __('backend/cruise.journey_type2_day3_image') }}" class="img-thumbnail output_image_album" src="#"/>
                                    <div class="card-action position-relative">
                                        <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="card-body text-center">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @else
                <p>{{ __('web.not-found', ['name' => __('backend/cruise.journal')]) }}</p>
            @endif

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

    <script>
        $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route('backend.uploadPhoto').'?_token='.csrf_token() !!}';
            CKEDITOR.config.height = '300px';

            CKEDITOR.replace(document.querySelector( '#journey_type1_day1_detail' ));
            CKEDITOR.replace(document.querySelector( '#journey_type1_day2_detail' ));
            CKEDITOR.replace(document.querySelector( '#journey_type2_day1_detail' ));
            CKEDITOR.replace(document.querySelector( '#journey_type2_day2_detail' ));
            CKEDITOR.replace(document.querySelector( '#journey_type2_day3_detail' ));

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            function initCkeditor(type) {
                if (type == '1') {
                    $('#type1').show();
                    $('#type2').hide();
                } else if (type == '2') {
                    $('#type1').hide();
                    $('#type2').show();
                }
            }

            initCkeditor('1');

            $('select[name=type_journal]').change(function () {
                initCkeditor($(this).val());
            });

            listenValidateForm();
            $('#type1 button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.journey_type1_day1_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type1_day1_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type1_day1_detail'), msg);
                }
                if (CKEDITOR.instances.journey_type1_day2_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type1_day2_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type1_day2_detail'), msg);
                }
                validateForm(e);
            });
            $('#type2 button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.journey_type2_day1_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type2_day1_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type2_day1_detail'), msg);
                }
                if (CKEDITOR.instances.journey_type2_day2_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type2_day2_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type2_day2_detail'), msg);
                }
                if (CKEDITOR.instances.journey_type2_day3_detail.getData() == '') {
                    var msg = 'The {{ __('backend/cruise.journey_type2_day3_detail') }} field is required.';
                    addErrorValidateForm($('#validate_journey_type2_day3_detail'), msg);
                }
                validateForm(e);
            });

            initAlbum('type1_day1');
            initAlbum('type1_day2');

            initAlbum('type2_day1');
            initAlbum('type2_day2');
            initAlbum('type2_day3');

            function initAlbum(type) {
                $.each($('.preview_image_album_'+ type +''), function (i, e) {
                    $(e).hide();
                });
                $('.'+ type +'_album_upload').change(function (e) {
                    var upload_count = $('.'+ type +'_album_upload')[0].files.length;

                    if (upload_count > 3) {
                        alert('{{ __('web.max-upload-album', ['max' => 3]) }}');
                        $(this).val('');
                        return false;
                    }

                    $.each($('.preview_image_album_'+ type +''), function (i, e) {
                        $(e).hide();
                    });

                    var files = $('.'+ type +'_album_upload')[0].files;

                    var count = 0;
                    for (var i = 0, f; f = files[i]; i++) {
                        var filename = f.name;
                        if (checkImage(filename)) {
                            $currentImage = $('.preview_image_album_'+ type +'[data-image=' + i + ']');
                            $currentImage.find('img').attr('src', window.URL.createObjectURL(f));
                            $currentImage.show();
                            count++;
                        } else {
                            return false;
                        }
                    }
                });
                $('.preview_image_album_'+ type +' .remove_image_album').click(function (e) {
                    e.preventDefault();

                    var files = $('.'+ type +'_album_upload')[0].files;
                    var index = $(e.currentTarget).parents('.preview_image_album_'+ type +'').data('image');

                    var newFileList = Array.from(files);
                    newFileList.splice(index, 1);

                    let list = new DataTransfer();
                    $.each(newFileList, function (i, e) {
                        list.items.add(e);
                    });

                    $('.'+ type +'_album_upload')[0].files = list.files;

                    $(e.currentTarget).parents('.preview_image_album_'+ type +'').hide();
                });

                function checkImage(filename) {
                    var fileType = filename.split('.').pop().trim().toLowerCase();
                    var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                    if ($.inArray(fileType, validImageTypes) < 0) {
                        alert('{{ __('web.invalid-image-type') }}');
                        return false;
                    }
                    return true;
                }
            }
        });
    </script>
@endpush
