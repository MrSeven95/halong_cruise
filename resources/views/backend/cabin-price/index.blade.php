@extends('layouts.admin')

@section('title', __('backend.cabin-price'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('cabin-price', $data['id'], $data['name'], $cruise['id'], $cruise['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body px-4">
            <div class="row">
                <div class="col-12 text-center"><h3>{{ __('backend.cabin-price') }}</h3></div>
                <div class="w-100"></div>
                <form class="col-12 mt-3" action="{{ route('backend.cabin-price.store', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="cabin_id" value="{{ $data['id'] }}">

                    <div class="row pt-2">
                        <div class="offset col-md-2">
                            <div class="form-group">
                                <label class="control-label" for="type">{{ __('backend/cabin-price.type') }} <span class="text-danger">(*)</span></label>
                                <select name="type" id="type"  data-name-show="{{ __('backend/cabin-price.type') }}" class="select2 custom-validate form-control form-control-lg @error('type') is-invalid @enderror" required>
                                    <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                    <option value="{{ __('backend/cabin-price.type_single') }}"{{ old('type') == __('backend/cabin-price.type_single') ? ' selected' : '' }}>{{ __('backend/cabin-price.type_single') }}</option>
                                    <option value="{{ __('backend/cabin-price.type_double') }}"{{ old('type') == __('backend/cabin-price.type_double') ? ' selected' : '' }}>{{ __('backend/cabin-price.type_double') }}</option>
                                    <option value="{{ __('backend/cabin-price.type_twin') }}"{{ old('type') == __('backend/cabin-price.type_twin') ? ' selected' : '' }}>{{ __('backend/cabin-price.type_twin') }}</option>
                                </select>

                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback d-none" id="validate_type" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label" for="price">{{ __('backend/cabin-price.price') }} <span class="text-danger">(*)</span></label>
                                <input id="price" type="number"  data-name-show="{{ __('backend/cabin-price.price') }}" class="custom-validate form-control form-control-lg @error('price') is-invalid @enderror"
                                       name="price" value="{{ old('price') }}" min="1" step="0.01"
                                       required>

                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback d-none" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label" for="name">{{ __('backend/cabin-price.less_than_one_person') }} <span class="text-danger">(*)</span></label>
                                <input id="less_than_one_person" type="number" data-name-show="{{ __('backend/cabin-price.less_than_one_person') }}"  class="custom-validate form-control form-control-lg @error('less_than_one_person') is-invalid @enderror"
                                       name="less_than_one_person" data-name-show="less than one person" value="{{ old('less_than_one_person') }}" min="0" max="100"
                                       required>

                                @error('less_than_one_person')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback d-none" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="surchange">{{ __('backend/cabin-price.surchange') }} <span class="text-danger">(*)</span></label>
                                <input id="surchange" type="text"  data-name-show="{{ __('backend/cabin-price.surchange') }}" class="custom-validate form-control form-control-lg @error('surchange') is-invalid @enderror"
                                       name="surchange" value="{{ old('surchange') }}"
                                       required>

                                @error('surchange')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <span class="invalid-feedback d-none" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2 d-flex align-items-start mt-4">
                            <div class="form-group" style="width: 100%;">
                                <button type="submit" class="form-control form-control-lg btn btn-success" style="margin-top: 5px;"> <i class="fa fa-check"></i> {{ __('web.save') }} or Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/cabin-price.type') }}</th>
                        <th>{{ __('backend/cabin-price.price') }}</th>
                        <th>{{ __('backend/cabin-price.less_than_one_person') }}</th>
                        <th>{{ __('backend/cabin-price.surchange') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: {
                    url: "{{ route('backend.cabin-price.index', ['id' => $data['id']]) }}?ajax"
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'type', name: 'type', width: '120px'},
                    {data: 'price', name: 'price', width: '120px', className: 'text-center',  render: function (data, type, row) {
                            var price = new String(row['price']);
                            price = price.split('.');
                            if (price[1] && price[1] == '00') price = price[0];
                            else price = row['price'];
                            return 'USD $'+ price;
                        }},
                    {data: 'less_than_one_person', name: 'less_than_one_person', width: '120px', searchable: false, className: "text-center"},
                    {data: 'surchange', name: 'surchange', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ]
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, 'cabin price');

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            listenValidateForm();
            $('button[type=submit].btn-success').click(function (e) {
                validateForm(e);
            });
        });
    </script>
@endpush