@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('backend.cabin')]))

@push('after-styles')
    <style>
        input[type=radio] {
            height: 1em !important;
        }
        #preview_image_avatar {
            height: auto;
        }
        .form-check {
            padding-left: 0!important;
        }
    </style>
@endpush

@section('content')
{{ Breadcrumbs::render('cabin-create', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <form class="mt-3" action="{{ route('backend.cruise.cabin.store', ['id' => $data['id']]) }}" method="post" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="cruise_id" value="{{ $data['id'] }}">

                <h3 class="card-title text-center">{{ __('web.creating', ['name' => __('backend.cabin')]) }}</h3>
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="name">{{ __('backend/cabin.name') }} <span class="text-danger">(*)</span></label>
                                    <input id="name" type="text" data-name-show="{{ __('backend/cabin.name') }}" class="custom-validate form-control form-control-lg @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}" autofocus maxlength="100"
                                           required>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="cabin_size">{{ __('backend/cabin.cabin_size') }} (m2) <span class="text-danger">(*)</span></label>
                                    <input id="cabin_size" type="number" min="0" data-name-show="{{ __('backend/cabin.cabin_size') }}" class="custom-validate form-control form-control-lg @error('cabin_size') is-invalid @enderror"
                                           name="cabin_size" value="{{ old('cabin_size') }}" min="1"
                                           required>

                                    @error('cabin_size')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="name">{{ __('backend/cabin.bed_type') }} <span class="text-danger">(*)</span></label>
                                    <select name="bed_type_id" id="bed_type_id" data-name-show="{{ __('backend/cabin.bed_type') }}" class="select2 custom-validate form-control form-control-lg @error('bed_type_id') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        @foreach ($bedTypes as $bedType)
                                            <option value="{{ $bedType->id }}"{{ old('bed_type_id') == $bedType->id ? ' selected' : '' }}>{{ $bedType->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('bed_type_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="capacity_adult">{{ __('backend/cabin.capacity_adult') }} <span class="text-danger">(*)</span></label>
                                    <select name="capacity_adult" id="capacity_adult" data-name-show="{{ __('backend/cabin.capacity_adult') }}" class="select2 custom-validate form-control form-control-lg @error('capacity_adult') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        @for ($i = 1; $i <= 4; $i++)
                                            <option value="{{ $i }}"{{ old('capacity_adult') == $i ? ' selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>

                                    @error('capacity_adult')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="capacity_child">{{ __('backend/cabin.capacity_child') }} <span class="text-danger">(*)</span></label>
                                    <select name="capacity_child" id="capacity_child" data-name-show="{{ __('backend/cabin.capacity_child') }}" class="select2 custom-validate form-control form-control-lg @error('capacity_child') is-invalid @enderror" required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>
                                        @for ($i = 1; $i <= 4; $i++)
                                            <option value="{{ $i }}"{{ old('capacity_child') == $i ? ' selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>

                                    @error('capacity_child')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="address">{{ __('backend/cabin.introduction') }} <span class="text-danger">(*)</span></label>
                                    <textarea id="introduction" data-name-show="{{ __('backend/cabin.introduction') }}" class="custom-validate form-control form-control-lg @error('introduction') is-invalid @enderror"
                                           name="introduction" required rows="6">{{ old('introduction') }}</textarea>

                                    @error('introduction')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <h4 class="card-title pl-2">Facilities</h4>
                            <div class="w-100"></div>
                            <div class="col-md-6">
                                <div class="card-body pl-0">
                                    <h4 class="card-title">Bathroom</h4>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="bathroom_towel" name="bathroom_towel"{{ old('bathroom_towel') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="bathroom_towel">{{ __('backend/cabin.bathroom_towel') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="bathroom_dryer" name="bathroom_dryer"{{ old('bathroom_dryer') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="bathroom_dryer">{{ __('backend/cabin.bathroom_dryer') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="bathroom_supplies" name="bathroom_supplies"{{ old('bathroom_supplies') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="bathroom_supplies">{{ __('backend/cabin.bathroom_supplies') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="bathroom_bathtub" name="bathroom_bathtub"{{ old('bathroom_bathtub') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="bathroom_bathtub">{{ __('backend/cabin.bathroom_bathtub') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pl-0">
                                    <h4 class="card-title">Food & drinks</h4>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="food_and_drinks_mini_bar" name="food_and_drinks_mini_bar"{{ old('food_and_drinks_mini_bar') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="food_and_drinks_mini_bar">{{ __('backend/cabin.food_and_drinks_mini_bar') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="food_and_drinks_fridge" name="food_and_drinks_fridge"{{ old('food_and_drinks_fridge') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="food_and_drinks_fridge">{{ __('backend/cabin.food_and_drinks_fridge') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="food_and_drinks_drinks" name="food_and_drinks_drinks"{{ old('food_and_drinks_drinks') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="food_and_drinks_drinks">{{ __('backend/cabin.food_and_drinks_drinks') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="food_and_drinks_foods" name="food_and_drinks_foods"{{ old('food_and_drinks_foods') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="food_and_drinks_foods">{{ __('backend/cabin.food_and_drinks_foods') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card-body pl-0">
                                    <h4 class="card-title">Cabin facilities</h4>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_set_of_bed_sheets" name="cabin_facilities_set_of_bed_sheets"{{ old('cabin_facilities_set_of_bed_sheets') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_set_of_bed_sheets">{{ __('backend/cabin.cabin_facilities_set_of_bed_sheets') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_hairdryer" name="cabin_facilities_hairdryer"{{ old('cabin_facilities_hairdryer') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_hairdryer">{{ __('backend/cabin.cabin_facilities_hairdryer') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_tivi" name="cabin_facilities_tivi"{{ old('cabin_facilities_tivi') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_tivi">{{ __('backend/cabin.cabin_facilities_tivi') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_air_conditional" name="cabin_facilities_air_conditional"{{ old('cabin_facilities_air_conditional') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_air_conditional">{{ __('backend/cabin.cabin_facilities_air_conditional') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_balcony" name="cabin_facilities_balcony"{{ old('cabin_facilities_balcony') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_balcony">{{ __('backend/cabin.cabin_facilities_balcony') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_wifi" name="cabin_facilities_wifi"{{ old('cabin_facilities_wifi') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_wifi">{{ __('backend/cabin.cabin_facilities_wifi') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_slippers" name="cabin_facilities_slippers"{{ old('cabin_facilities_slippers') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_slippers">{{ __('backend/cabin.cabin_facilities_slippers') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_wardrobe" name="cabin_facilities_wardrobe"{{ old('cabin_facilities_wardrobe') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_wardrobe">{{ __('backend/cabin.cabin_facilities_wardrobe') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_sofa" name="cabin_facilities_sofa"{{ old('cabin_facilities_sofa') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_sofa">{{ __('backend/cabin.cabin_facilities_sofa') }}</label>
                                                </div>
                                            </div>
                                            <div class="w-100"></div>
                                            <div class="form-check">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="1" class="custom-control-input" id="cabin_facilities_smoking" name="cabin_facilities_smoking"{{ old('cabin_facilities_smoking') == 1 ? ' checked' : '' }}>
                                                    <label class="custom-control-label" for="cabin_facilities_smoking">{{ __('backend/cabin.cabin_facilities_smoking') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_avatar" class="control-label">{{ __('backend/cabin.image_avatar') }} (1200x900px)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" class="custom-file-input form-control form-control-lg @error('image_avatar') is-invalid @enderror"
                                                   name="image_avatar" value="" onchange="document.getElementById('output_image_avatar').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>

                                    @error('image_avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="card mt-2 col-md-2" id="preview_image_avatar">
                                <img id="output_image_avatar" alt="{{ __('backend/cabin.image_thumbnail') }}" class="img-thumbnail" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="btn btn-danger position-absolute" style="bottom: 10px; right: 10px;" id="remove_image_avatar"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_album" class="control-label">Album ảnh (1200x900px)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" class="album_upload custom-file-input form-control form-control-lg @error('image_album') is-invalid @enderror"
                                                   name="image_album[]" value="" multiple>
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="w-100"></div>

                            <div class="card col-md-2 preview_image_album" data-image="0">
                                <img alt="{{ __('backend/cabin.image_avatar') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                            <div class="card col-md-2 preview_image_album" data-image="1">
                                <img alt="{{ __('backend/cabin.image_avatar') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                            <div class="card col-md-2 preview_image_album" data-image="2">
                                <img alt="{{ __('backend/cabin.image_avatar') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                            <div class="card col-md-2 preview_image_album" data-image="3">
                                <img alt="{{ __('backend/cabin.image_avatar') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                            <div class="card col-md-2 preview_image_album" data-image="4">
                                <img alt="{{ __('backend/cabin.image_avatar') }}" class="img-thumbnail output_image_album" src="#"/>
                                <div class="card-action position-relative">
                                    <button class="remove_image_album btn btn-danger position-absolute" style="bottom: 10px; right: 10px;"><i class="ti-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ __('web.save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $('#preview_image_avatar').hide();
            $.each($('.preview_image_album'), function (i, e) {
                $(e).hide();
            });
            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __('web.select-an-option') }}'
                });
            });

            listenValidateForm();
            $('button[type=submit].btn-success').click(function (e) {
                validateForm(e);
            });

            $('.album_upload').change(function(e) {
                var upload_count = $('.album_upload')[0].files.length;

                if (upload_count > 5) {
                    alert('{{ __('web.max-upload-album', ['max' => 5]) }}');
                    $(this).val('');
                    return false;
                }

                sortImage();
            });

            function sortImage() {
                $.each($('.preview_image_album'), function (i, e) {
                    $(e).hide();
                });

                var files = $('.album_upload')[0].files;

                var count = 0;
                for (var i=0, f; f=files[i]; i++) {
                    var filename = f.name;
                    if (checkImage(filename)) {
                        $currentImage = $('.preview_image_album[data-image=' + i + ']');
                        $currentImage.find('img').attr('src', window.URL.createObjectURL(f));
                        $currentImage.show();
                        count++;
                    } else {
                        return false;
                    }
                }
            };

            $('.remove_image_album').click(function(e) {
                e.preventDefault();

                var files = $('.album_upload')[0].files;
                var index = $(e.currentTarget).parents('.preview_image_album').data('image');

                var newFileList = Array.from(files);
                newFileList.splice(index,1);

                let list = new DataTransfer();
                $.each(newFileList, function(i, e) {
                    list.items.add(e);
                });

                $('.album_upload')[0].files = list.files;

                $(e.currentTarget).parents('.preview_image_album').hide();
            });

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __('web.invalid-image-type') }}');
                    return false;
                }
                return true;
            }

            $('input[name=image_avatar]').change(function(e) {
                var file = $(this).val();

                if (checkImage(file)) {
                    $('#preview_image_avatar').show();
                } else {
                    $(this).val('');
                }
            });
            $('#remove_image_avatar').click(function(e) {
                e.preventDefault();
                $('input[name=image_avatar]').val('');
                $('#preview_image_avatar').hide();
            });
        });
    </script>
@endpush
