@extends('layouts.admin')

@section('title', __('backend.cabin'))

@push('after-styles')
    {{ Html::style('xtreme-admin/css/datatables.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('cabin', $data['id'], $data['name']) }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6"><h3>{{ __('backend.cabin') }}</h3></div>
                <div class="col-6 text-right">
                    <a href="{{ route('backend.cruise.cabin.create', ['id' => $data['id']]) }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('backend/cabin.image_avatar') }}</th>
                        <th>{{ __('backend/cabin.name') }}</th>
                        <th>{{ __('backend/cabin.cabin_size') }}</th>
                        <th>{{ __('backend/cabin.bed_type') }}</th>
                        <th>{{ __('backend/cabin.capacity') }}</th>
                        <th>{{ __('backend.cabin-price') }}</th>
                        <th>{{ __('web.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    {!! Html::script('xtreme-admin/js/datatables.min.js') !!}

    <script>
        $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: {
                    url: "{{ route('backend.cruise.cabin.index', ['id' => $data['id']]) }}?ajax"
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'image', name: 'image', className: 'text-center', orderable: false, searchable: false, width: '160px', render: function (data, type, row) {
                        var url = data;
                        if (url == null) url = '{{ url('/images/no-image-found.jpg') }}';
                        return '<img src="' + url +'" class="img-thumbnail" alt="{{ __('backend/cabin.image_thumbnail') }} '+ row['name'] +'" width="150px" height="auto"/>';
                        }},
                    {data: 'name', name: 'name'},
                    {data: 'cabin_size', name: 'cabin_size', width: '80px', searchable: false, className: "text-center"},
                    {data: 'bed_type', name: 'bed_type', width: '80px', searchable: false},
                    {data: 'capacity', name: 'capacity_adult', width: '120px', searchable: false, className: "text-center", render: function (data, type, row) {
                        var txt = row['capacity_adult'] + (row['capacity_adult'] > 1 ? ' adults - ': ' adult - ');
                        txt += row['capacity_child'] + (row['capacity_child'] > 1 ? ' children': ' child');
                        return txt;
                        }},
                    {data: 'cabin_price', name: 'cabin_price', orderable: false, className: 'text-center', searchable: false, width: '120px'},
                    {data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false, width: '150px'},
                ]
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('backend.cabin') }}');
        });
    </script>
@endpush