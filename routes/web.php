<?php

Route::redirect('/home', '/')->name('home');

Route::middleware('web')
    ->name('frontend.')
    ->group(function() {
        Route::get('/', 'HomeController@index')->name('home');
        Route::redirect('/index.html', '/');

        Route::get('/luxury.html', 'HomeController@luxury')->name('luxury');
        Route::get('/book.html', 'HomeController@book')->name('book');
        Route::get('/{slug}.html', 'HomeController@detail')->name('detail');
    });

$baseAdminUrl = 'superadmin';

Route::group(['prefix' => $baseAdminUrl], function () {
    Route::middleware('web')
        ->group(function () {
            Auth::routes();
        });

    Route::middleware(['web', 'auth'])
        ->name('backend.')
        ->namespace('Backend')
        ->group(function() {
            //Route::get('remove-image', 'DashboardController@removeImageUpload');
            Route::redirect('/', '/superadmin/dashboard');
            Route::resource('dashboard', 'DashboardController');

            Route::resource('booking', 'BookingController');
            Route::any('booking/ajax/dashboardTable', 'BookingController@ajaxDashboardTable')->name('booking.ajaxDashboardTable');
            Route::any('booking/ajax/dashboardChartist', 'BookingController@ajaxDashboardChartist')->name('booking.ajaxDashboardChartist');

            Route::resource('coupon', 'CouponController');
            Route::resource('cruise-area', 'CruiseAreaController');
            Route::resource('bed-type', 'BedTypeController');

            Route::get('cruise-config', 'CruiseController@editConfig')->name('cruise-config.edit');
            Route::put('cruise-config', 'CruiseController@updateConfig')->name('cruise-config.update');

            Route::resource('cruise', 'CruiseController');
            Route::get('cruise/{id}/album', 'MediaCruiseController@album')->name('media-cruise.album');
            Route::post('cruise/{id}/album/store', 'MediaCruiseController@store')->name('media-cruise.album.store');
            Route::post('cruise/{id}/album/swap-rank', 'MediaCruiseController@swapRank')->name('media-cruise.album.swap-rank');
            Route::delete('media-cruise/{id}/destroy', 'MediaCruiseController@destroy')->name('media-cruise.destroy');

            Route::post('cruise/{id}/is-best-seller', 'CruiseController@isBestSeller')->name('cruise.is-best-seller');

            Route::get('cruise/{id}/cabin/create', 'CabinController@create')->name('cruise.cabin.create');
            Route::get('cruise/{id}/cabin', 'CabinController@index')->name('cruise.cabin.index');
            Route::post('cruise/{id}/cabin/store', 'CabinController@store')->name('cruise.cabin.store');
            Route::get('cabin/{id}', 'CabinController@edit')->name('cabin.edit');
            Route::put('cabin/{id}', 'CabinController@update')->name('cabin.update');
            Route::delete('cabin/{id}/destroy', 'CabinController@destroy')->name('cabin.destroy');

            Route::get('cruise/{id}/policy', 'CruiseController@editPolicy')->name('cruise.policy.edit');
            Route::put('cruise/{id}/policy', 'CruiseController@updatePolicy')->name('cruise.policy.update');
            Route::get('cruise/{id}/additional', 'CruiseController@editAdditional')->name('cruise.additional.edit');
            Route::put('cruise/{id}/additional', 'CruiseController@updateAdditional')->name('cruise.additional.update');

            Route::get('cruise/{id}/journal', 'CruiseJournalController@index')->name('cruise.journal.index');
            Route::get('cruise/{id}/journal/create', 'CruiseJournalController@create')->name('cruise.journal.create');
            Route::post('cruise/{id}/journal/store', 'CruiseJournalController@store')->name('cruise.journal.store');
            Route::get('cruise/{id}/journal_type1', 'CruiseJournalController@editType1')->name('cruise.journal.edit-type-1');
            Route::get('cruise/{id}/journal_type2', 'CruiseJournalController@editType2')->name('cruise.journal.edit-type-2');
            Route::put('cruise/{id}/journal_type1', 'CruiseJournalController@updateType1')->name('cruise.journal.update-type-1');
            Route::put('cruise/{id}/journal_type2', 'CruiseJournalController@updateType2')->name('cruise.journal.update-type-2');
            Route::delete('cruise/{id}/journal_type1', 'CruiseJournalController@destroyType1')->name('cruise.journal.destroy-type-1');
            Route::delete('cruise/{id}/journal_type2', 'CruiseJournalController@destroyType2')->name('cruise.journal.destroy-type-2');

            Route::resource('review', 'ReviewController');
            Route::post('review/{id}/status', 'ReviewController@status')->name('review.status');

            Route::resource('free-service', 'FreeServiceController');
            Route::resource('transfer-service', 'TransferServiceController');
            Route::resource('other-service', 'OtherServiceController');

            Route::resource('faq', 'FaqController');
            Route::put('faq/{id}/status', 'FaqController@status')->name('faq.status');

            Route::get('faq-config', 'FaqController@editConfig')->name('faq-config.edit');
            Route::put('faq-config', 'FaqController@updateConfig')->name('faq-config.update');

            Route::post('uploadPhoto', 'MediaCruiseController@uploadPhoto')->name('uploadPhoto');

            Route::resource('cruise-coupon', 'CruiseCouponController');
            Route::resource('cruise-free-service', 'CruiseFreeServiceController');

            Route::get('cabin/{id}/cabin-price', 'CabinPriceController@index')->name('cabin-price.index');
            //Route::get('cabin/{id}/cabin-price/create', 'CabinPriceController@create')->name('cabin-price.create');
            Route::post('cabin/{id}/cabin-price/store', 'CabinPriceController@store')->name('cabin-price.store');
            Route::get('cabin-price/{id}', 'CabinPriceController@edit')->name('cabin-price.edit');
            Route::put('cabin-price/{id}', 'CabinPriceController@update')->name('cabin-price.update');
            Route::delete('cabin-price/{id}/destroy', 'CabinPriceController@destroy')->name('cabin-price.destroy');

        });
});

Route::get('/home', 'HomeController@index')->name('home');
