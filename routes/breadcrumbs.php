<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('frontend.home'));
});

// Home > Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->parent('home');
    $trail->push('Dashboard', route('backend.dashboard.index'));
});

// Home > Cruise
Breadcrumbs::for('cruise', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.cruise'), route('backend.cruise.index'));
});
// Home > Cruise > Show
Breadcrumbs::for('cruise-show', function ($trail, $id, $name) {
    $trail->parent('cruise' );
    $trail->push($name, route('backend.cruise.show', $id));
});
// Home > Cruise > Create
Breadcrumbs::for('cruise-create', function ($trail) {
    $trail->parent('cruise' );
    $trail->push(__('web.creating', ['name' => __('backend.cruise')]), route('backend.cruise.index'));
});
// Home > Cruise > Edit
Breadcrumbs::for('cruise-edit', function ($trail, $id, $name) {
    $trail->parent('cruise-show', $id, $name);
    $trail->push(__('web.editing', ['name' => __('backend.cruise')]), route('backend.cruise.index'));
});
// Home > Cruise > Additional
Breadcrumbs::for('cruise-additional', function ($trail, $id, $name) {
    $trail->parent('cruise-show', $id, $name);
    $trail->push(__('web.editing', ['name' => __('backend/cruise.additional')]), route('backend.cruise.index'));
});
// Home > Cruise > Policy
Breadcrumbs::for('cruise-policy', function ($trail, $id, $name) {
    $trail->parent('cruise-show', $id, $name);
    $trail->push(__('web.editing', ['name' => __('backend/cruise.policy')]), route('backend.cruise.index'));
});

// Home > Cruise > Config
Breadcrumbs::for('cruise-config', function ($trail) {
    $trail->parent('cruise');
    $trail->push(__('backend.cruise-config'), route('backend.cruise.index'));
});

// Home > CruiseArea
Breadcrumbs::for('cruise-area', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.cruise-area'), route('backend.cruise-area.index'));
});
// Home > CruiseArea > Create
Breadcrumbs::for('cruise-area-create', function ($trail) {
    $trail->parent('cruise-area');
    $trail->push(__('web.creating', ['name' => __('backend.cruise-area')]), route('backend.cruise-area.index'));
});
// Home > CruiseArea > Edit
Breadcrumbs::for('cruise-area-edit', function ($trail) {
    $trail->parent('cruise-area');
    $trail->push(__('web.editing', ['name' => __('backend.cruise-area')]), route('backend.cruise-area.index'));
});

// Home > Cruise > CruiseJournal
Breadcrumbs::for('cruise-journal', function ($trail, $id, $name) {
    $trail->parent('cruise-show', $id, $name);
    $trail->push(__('backend/cruise.journal'), route('backend.cruise.journal.index', $id));
});
// Home > Cruise > CruiseJournal > Create
Breadcrumbs::for('cruise-journal-create', function ($trail, $id, $name) {
    $trail->parent('cruise-journal', $id, $name);
    $trail->push(__('web.creating', ['name' => __('backend/cruise.journal')]), route('backend.cruise.journal.index', $id));
});
// Home > Cruise > CruiseJournal > Edit
Breadcrumbs::for('cruise-journal-edit', function ($trail, $id, $name) {
    $trail->parent('cruise-journal', $id, $name);
    $trail->push(__('web.editing', ['name' => __('backend/cruise.journal')]), route('backend.cruise.journal.index', $id));
});

// Home > Cruise > Cabin
Breadcrumbs::for('cabin', function ($trail, $id, $name) {
    $trail->parent('cruise-show', $id, $name);
    $trail->push(__('backend.cabin'), route('backend.cruise.cabin.index', $id));
});
// Home > Cruise > Cabin > Create
Breadcrumbs::for('cabin-create', function ($trail, $id, $name) {
    $trail->parent('cabin', $id, $name);
    $trail->push(__('web.creating', ['name' => __('backend.cabin')]), route('backend.cruise.cabin.index', $id));
});
// Home > Cruise > Cabin > Edit
Breadcrumbs::for('cabin-edit', function ($trail, $id, $name) {
    $trail->parent('cabin', $id, $name);
    $trail->push(__('web.editing', ['name' => __('backend.cabin')]), route('backend.cruise.cabin.index', $id));
});

// Home > Cruise > CabinPrice
Breadcrumbs::for('cabin-price', function ($trail, $id, $name, $cruise_id, $cruise_name) {
    $trail->parent('cruise-show', $cruise_id, $cruise_name);
    $trail->push($name, route('backend.cruise.cabin.index', $cruise_id));
    $trail->push(__('backend.cabin-price'), route('backend.cruise.cabin.index', $id));
});
// Home > Cruise > CabinPrice > Edit
Breadcrumbs::for('cabin-price-edit', function ($trail, $id, $name, $cruise_id, $cruise_name) {
    $trail->parent('cruise-show', $cruise_id, $cruise_name);
    $trail->push($name, route('backend.cruise.cabin.index', $cruise_id));
    $trail->push(__('backend.cabin-price'), route('backend.cruise.cabin-price.index', $id));
    $trail->push(__('web.editing', ['name' => __('backend.cabin-price')]), route('backend.cruise.cabin.index', $id));
});

// Home > Cruise > Album
Breadcrumbs::for('cruise-album', function ($trail, $id, $name) {
    $trail->parent('cruise-show', $id, $name);
    $trail->push(__('backend/cruise.album'), route('backend.cruise.index'));
});

// Home > BedType
Breadcrumbs::for('bed-type', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.bed-type'), route('backend.bed-type.index'));
});
// Home > BedType > Create
Breadcrumbs::for('bed-type-create', function ($trail) {
    $trail->parent('bed-type');
    $trail->push(__('web.creating', ['name' => __('backend.bed-type')]), route('backend.bed-type.index'));
});
// Home > BedType > Edit
Breadcrumbs::for('bed-type-edit', function ($trail) {
    $trail->parent('bed-type');
    $trail->push(__('web.editing', ['name' => __('backend.bed-type')]), route('backend.bed-type.index'));
});

// Home > FAQ
Breadcrumbs::for('faq', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.faq'), route('backend.faq.index'));
});
// Home > FAQ > Edit
Breadcrumbs::for('faq-edit', function ($trail) {
    $trail->parent('faq');
    $trail->push(__('web.editing', ['name' => __('backend.faq')]), route('backend.faq.index'));
});
// Home > FAQ > Create
Breadcrumbs::for('faq-create', function ($trail) {
    $trail->parent('faq');
    $trail->push(__('web.creating', ['name' => __('backend.faq')]), route('backend.faq.index'));
});

// Home > CruisePromotion
Breadcrumbs::for('cruise-promotion', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.cruise-promotion'), route('backend.cruise-coupon.index'));
});
// Home > CruisePromotion > Edit
Breadcrumbs::for('cruise-promotion-edit', function ($trail) {
    $trail->parent('cruise-promotion');
    $trail->push(__('web.editing', ['name' => __('backend.cruise-promotion')]), route('backend.cruise-coupon.index'));
});
// Home > CruisePromotion > Create
Breadcrumbs::for('cruise-promotion-create', function ($trail) {
    $trail->parent('cruise-promotion');
    $trail->push(__('web.creating', ['name' => __('backend.cruise-promotion')]), route('backend.cruise-coupon.index'));
});

// Home > CruiseFreeService
Breadcrumbs::for('cruise-free-service', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.cruise-free-service'), route('backend.cruise-free-service.index'));
});
// Home > CruiseFreeService > Edit
Breadcrumbs::for('cruise-free-service-edit', function ($trail) {
    $trail->parent('cruise-free-service');
    $trail->push(__('web.editing', ['name' => __('backend.cruise-free-service')]), route('backend.cruise-free-service.index'));
});
// Home > CruiseFreeService > Create
Breadcrumbs::for('cruise-free-service-create', function ($trail) {
    $trail->parent('cruise-free-service');
    $trail->push(__('web.creating', ['name' => __('backend.cruise-free-service')]), route('backend.cruise-free-service.index'));
});

// Home > FAQ > Config
Breadcrumbs::for('faq-config', function ($trail) {
    $trail->parent('faq');
    $trail->push(__('backend.faq-config'), route('backend.faq.index'));
});

// Home > Review
Breadcrumbs::for('review', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.review'), route('backend.review.index'));
});
// Home > Review > Create
Breadcrumbs::for('review-create', function ($trail) {
    $trail->parent('review');
    $trail->push(__('web.creating', ['name' => __('backend.review')]), route('backend.review.index'));
});
// Home > Review > Edit
Breadcrumbs::for('review-edit', function ($trail) {
    $trail->parent('review');
    $trail->push(__('web.editing', ['name' => __('backend.review')]), route('backend.review.index'));
});

// Home > FreeService
Breadcrumbs::for('free-service', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.free-service'), route('backend.free-service.index'));
});
// Home > FreeService > Create
Breadcrumbs::for('free-service-create', function ($trail) {
    $trail->parent('free-service');
    $trail->push(__('web.creating', ['name' => __('backend.free-service')]), route('backend.free-service.index'));
});
// Home > FreeService > Edit
Breadcrumbs::for('free-service-edit', function ($trail) {
    $trail->parent('free-service');
    $trail->push(__('web.editing', ['name' => __('backend.free-service')]), route('backend.free-service.index'));
});

// Home > OtherService
Breadcrumbs::for('other-service', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.other-service'), route('backend.other-service.index'));
});
// Home > OtherService > Create
Breadcrumbs::for('other-service-create', function ($trail) {
    $trail->parent('other-service');
    $trail->push(__('web.creating', ['name' => __('backend.other-service')]), route('backend.other-service.index'));
});
// Home > OtherService > Edit
Breadcrumbs::for('other-service-edit', function ($trail) {
    $trail->parent('other-service');
    $trail->push(__('web.editing', ['name' => __('backend.other-service')]), route('backend.other-service.index'));
});

// Home > TransferService
Breadcrumbs::for('transfer-service', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.transfer-service'), route('backend.transfer-service.index'));
});
// Home > TransferService > Create
Breadcrumbs::for('transfer-service-create', function ($trail) {
    $trail->parent('transfer-service');
    $trail->push(__('web.creating', ['name' => __('backend.transfer-service')]), route('backend.transfer-service.index'));
});
// Home > TransferService > Edit
Breadcrumbs::for('transfer-service-edit', function ($trail) {
    $trail->parent('transfer-service');
    $trail->push(__('web.editing', ['name' => __('backend.transfer-service')]), route('backend.transfer-service.index'));
});

// Home > Coupon
Breadcrumbs::for('coupon', function ($trail) {
    $trail->parent('home');
    $trail->push(__('backend.coupon'), route('backend.coupon.index'));
});
// Home > Coupon > Create
Breadcrumbs::for('coupon-create', function ($trail) {
    $trail->parent('coupon');
    $trail->push(__('web.creating', ['name' => __('backend.coupon')]), route('backend.coupon.index'));
});
// Home > Coupon > Edit
Breadcrumbs::for('coupon-edit', function ($trail) {
    $trail->parent('coupon');
    $trail->push(__('web.editing', ['name' => __('backend.coupon')]), route('backend.coupon.index'));
});