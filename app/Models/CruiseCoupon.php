<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\SoftDeletes;

class CruiseCoupon extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    protected $table = 'coupon_cruise';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new class implements Scope
        {
            public function apply(Builder $builder, Model $model)
            {
                $builder
                    ->whereHas('cruise', function ($q) {
                        $q->whereNull('deleted_at');
                    })->whereHas('coupon', function ($q) {
                        $q->whereNull('deleted_at');
                    });
            }
        });
    }

    public function cruise() {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function coupon() {
        return $this->belongsTo('App\Models\Coupon');
    }
}
