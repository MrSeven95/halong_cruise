<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\SoftDeletes;

class CabinPrice extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new class implements Scope
        {
            public function apply(Builder $builder, Model $model)
            {
                $builder
                    ->whereHas('cabin', function ($q) {
                        $q->whereNull('deleted_at');
                    });
            }
        });
    }

    public function cabin() {
        return $this->belongsTo('App\Models\Cabin');
    }

    public function cruise() {
        return $this->belongsTo('App\Models\Cruise');
    }
}
