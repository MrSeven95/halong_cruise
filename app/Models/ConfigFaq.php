<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigFaq extends Model
{
    use SoftDeletes;
    protected $guarded = array();
}
