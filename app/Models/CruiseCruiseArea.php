<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\SoftDeletes;

class CruiseCruiseArea extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    protected $table = 'cruise_cruise_area';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new class implements Scope
        {
            public function apply(Builder $builder, Model $model)
            {
                $builder
                    ->whereHas('cruise', function ($q) {
                        $q->whereNull('deleted_at');
                    })->whereHas('cruiseArea', function ($q) {
                        $q->whereNull('deleted_at');
                    });
            }
        });
    }

    public function cruise() {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function cruiseArea() {
        return $this->belongsTo('App\Models\CruiseArea');
    }
}
