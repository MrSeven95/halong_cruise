<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cabin extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    public function cruise() {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function bedType() {
        return $this->belongsTo('App\Models\BedType');
    }

    public function cabinPrice() {
        return $this->hasMany('App\Models\CabinPrice');
    }
}
