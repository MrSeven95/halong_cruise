<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferService extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    public function booking() {
        return $this->belongsToMany('App\Models\Booking');
    }
}
