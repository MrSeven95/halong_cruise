<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FreeService extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    public function cruise() {
        return $this->belongsToMany('App\Models\Cruise');
    }
}
