<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\SoftDeletes;

class CruiseFreeService extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    protected $table = 'cruise_free_service';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new class implements Scope
        {
            public function apply(Builder $builder, Model $model)
            {
                $builder
                    ->whereHas('cruise', function ($q) {
                        $q->whereNull('deleted_at');
                    })->whereHas('freeService', function ($q) {
                        $q->whereNull('deleted_at');
                    });
            }
        });
    }

    public function cruise() {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function freeService() {
        return $this->belongsTo('App\Models\FreeService');
    }
}
