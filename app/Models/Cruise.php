<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cruise extends Model
{
    use SoftDeletes;
    protected $guarded = array();

    public function review() {
        return $this->hasMany('App\Models\Review');
    }

    public function cabin() {
        return $this->hasMany('App\Models\Cabin');
    }

    public function coupon() {
        return $this->belongsToMany('App\Models\Coupon');
    }

    public function couponCruise() {
        return $this->hasMany('App\Models\CruiseCoupon');
    }

    public function freeService() {
        return $this->belongsToMany('App\Models\FreeService');
    }

    public function cruiseFreeService() {
        return $this->hasMany('App\Models\CruiseFreeService');
    }

    public function cruiseArea() {
        return $this->belongsToMany('App\Models\CruiseArea');
    }

    public function cruiseCruiseArea() {
        return $this->hasMany('App\Models\CruiseCruiseArea');
    }

    public function cabinPrice() {
        return $this->hasManyThrough('App\Models\CabinPrice', 'App\Models\Cabin');
    }

    public function mediaCruise() {
        return $this->hasMany('App\Models\MediaCruise');
    }
}