<?php

use Illuminate\Support\Str;

function generate_random_letters($length) {
    $random = '';
    for ($i = 0; $i < $length; $i++) {
        $random .= chr(rand(ord('a'), ord('z')));
    }
    return $random;
}

if (! function_exists('upload_image')) {

    function upload_image($file, $slug = "image")
    {
        $newFileName = $slug .'-'. generate_random_letters(8) . '.' . $file->getClientOriginalExtension();

        $destinationPath = public_path('uploads');

        $file->move($destinationPath, $newFileName);

        return '/uploads/' . $newFileName;
    }
}

if (! function_exists('exist_image')) {
    function exist_image($file_public)
    {
        if ($file_public != '') {
            $folder = public_path('uploads');
            $file_name = substr($file_public, 8, strlen($file_public));
            $path = realpath($folder . $file_name);

            if (file_exists($path)) {
                return true;
            }
        }
        return false;
    }
}

if (! function_exists('remove_image')) {

    function remove_image($file_public)
    {
        $folder = public_path('uploads');
        $file_name = substr($file_public, 8, strlen($file_public));
        $path = realpath($folder . $file_name);

        if (file_exists($path)) {
            try {
                unlink($path);
            } catch (\Exception $exception) {
                return $path;
            }
        }
        return $path;
    }
}

if (! function_exists('make_thumb')) {

    function make_thumb($thumbnail_width, $thumbnail_height, $src)
    {
        $thumbpath = substr(public_path('uploads'), 0, strlen(public_path('uploads')) - 7) .'/uploads/thumb-'. basename($src);
        $thumbpathRes = '/uploads/thumb-'. basename($src);
        $src = substr(public_path('uploads'), 0, strlen(public_path('uploads')) - 7) . $src;

        list($width, $height, $original_type) = getimagesize($src);
        $thumb_create = imagecreatetruecolor($thumbnail_width, $thumbnail_height);

        if ($original_type === 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        } else if ($original_type === 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        } else if ($original_type === 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        } else {
            return false;
        }

        $source = $imgcreatefrom($src);
        imagecopyresized($thumb_create,$source,0,0,0,0,$thumbnail_width,$thumbnail_height,$width,$height);
        $imgt($thumb_create,$thumbpath,100);

        return $thumbpathRes;
    }
}