<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

if (! function_exists('clearXSS')) {
    /**
     * @param  $file
     *
     * @return string
     */
    function clearXSS($string)
    {
        $string = nl2br($string);
        $string = trim(strip_tags($string));
        $string = removeScripts($string);

        return $string;
    }
}

if (! function_exists('removeScripts')) {
    function removeScripts($str)
    {
        $regex =
            '/(<link[^>]+rel="[^"]*stylesheet"[^>]*>)|' .
            '<script[^>]*>.*?<\/script>|' .
            '<style[^>]*>.*?<\/style>|' .
            '<!--.*?-->/is';

        return preg_replace($regex, '', $str);
    }
}