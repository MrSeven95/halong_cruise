<?php


namespace App\Providers;


use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class BackendProvider extends ServiceProvider
{
    protected $providers =[
        \App\Models\ModelProvider::class
    ];

    public function boot()
    {
        $this->registerMiddleware();
        $this->registerViewComposerData();
    }

    public function register()
    {
        $this->registerProviders();
    }

    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            App::register($provider);
        }
    }

    protected function registerMiddleware()
    {
        //
    }

    public function registerViewComposerData()
    {
        //
    }
}