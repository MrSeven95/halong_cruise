<?php

namespace App\Http\Controllers;

use App\Models\Cabin;
use App\Models\CabinPrice;
use App\Models\Cruise;
use App\Models\CruiseArea;
use App\Models\CruiseFreeService;
use App\Models\FreeService;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function index()
    {
        $listCruises = Cruise::query()
            ->with('cruiseCruiseArea.cruiseArea')
            ->with('couponCruise')
            ->with('cruiseFreeService.freeService')
            ->with('cabinPrice')
            ->with('review')
            ->orderBy('display_ranking', 'asc')
            ->limit(10)
            ->get();

        return view('frontend.index')
            ->with([
                'listCruises' => $listCruises
            ]);
    }

    public function detail(Request $request, $slug)
    {
        $cruise = Cruise::query()
            ->where('slug', '=', $slug)
            ->with('cruiseCruiseArea.cruiseArea')
            ->with('cabinPrice')
            ->with('couponCruise')
            ->with('cruiseFreeService.freeService')
            ->with('review')
            ->with(['mediaCruise' => function ($q) {
                $q->orderBy('display_ranking', 'asc');
            }])
            ->firstOrFail();

        $listCabins = Cabin::query()
            ->where('cruise_id', '=', $cruise->id)
            ->with('bedType')
            ->with('cruise.couponCruise')
            ->with('cabinPrice')
            ->get();

        $listReviews = Review::query()
            ->where('cruise_id', '=', $cruise->id)
            ->paginate(10);

        return view('frontend.detail')
            ->with([
                'cruise' => $cruise,
                'listCabins' => $listCabins,
                'listReviews' => $listReviews
            ]);
    }

    public function book(Request $request)
    {
        if ($request->has('cabin_id')) {
            $cabin = Cabin::query()
                ->where('id', '=', $request->cabin_id)
                ->with('cruise')
                ->first();

            return view('frontend.book')
                ->with([
                    'cabin' => $cabin,
                ]);
        }
    }

    public function luxury(Request $request)
    {
        $perPage = 10;

        if ($request->ajax() && $request->has('ajax')) {
            try {
                $areas = [];
                if ($request->has('dataFilterArea')) {
                    if (!is_array($areas)) {
                        return response()->json([
                            'status' => 'error',
                            'data' => $areas,
                            'exception' => 'Wrong data ' . __('backend/cruise.area')
                        ]);
                    }
                    $areas = $request->dataFilterArea;
                }

                $stars = [];
                if ($request->has('dataFilterStar')) {
                    if (!is_array($stars)) {
                        return response()->json([
                            'status' => 'error',
                            'data' => $stars,
                            'exception' => 'Wrong data ' . __('backend/cruise.star')
                        ]);
                    }
                    $stars = $request->dataFilterStar;
                }

                $dataFilterPriceMin = 0;
                $dataFilterPriceMax = 0;
                if ($request->has('dataFilterPriceMin') && $request->has('dataFilterPriceMax')) {
                    $dataFilterPriceMin = $request->dataFilterPriceMin;
                    $dataFilterPriceMax = $request->dataFilterPriceMax;
                }

                if ($dataFilterPriceMin == 0 && $dataFilterPriceMax == 0) {
                    $listCruises = Cruise::query()
                        ->whereHas('cruiseArea', function ($q) use ($areas) {
                            if ($areas && count($areas) > 0) {
                                $q->whereIn('cruise_areas.id', $areas);
                            } else {
                                $q;
                            }
                        })
                        ->where(function ($q) use ($stars) {
                            if ($stars && count($stars) > 0) {
                                $q->whereIn('star', $stars);
                            } else {
                                $q;
                            }
                        })
                        ->with('cruiseCruiseArea.cruiseArea')
                        ->with('cabinPrice')
                        ->with('couponCruise')
                        ->with('cruiseFreeService.freeService')
                        ->with('review')
                        ->orderBy('display_ranking', 'asc')
                        ->Paginate($perPage);
                } else {
                    $listCruises = Cruise::query()
                        ->whereHas('cruiseArea', function ($q) use ($areas) {
                            if ($areas && count($areas) > 0) {
                                $q->whereIn('cruise_areas.id', $areas);
                            } else {
                                $q;
                            }
                        })
                        ->where(function ($q) use ($stars) {
                            if ($stars && count($stars) > 0) {
                                $q->whereIn('star', $stars);
                            } else {
                                $q;
                            }
                        })
                        ->whereHas('cabinPrice', function ($q) use ($dataFilterPriceMin, $dataFilterPriceMax) {
                            $q->whereBetween('price', [$dataFilterPriceMin, $dataFilterPriceMax]);
                        })
                        ->with('cruiseCruiseArea.cruiseArea')
                        ->with('cabinPrice')
                        ->with('couponCruise')
                        ->with('cruiseFreeService.freeService')
                        ->with('review')
                        ->orderBy('display_ranking', 'asc')
                        ->Paginate($perPage);
                }

                return response()->json([
                    'status' => 'success',
                    'html' => view('frontend.includes.luxury_cruise_item')
                        ->with([
                            'listCruises' => $listCruises,
                        ])->render(),
                    'data' => $listCruises,
                    'areas' => $areas,
                    'stars' => $stars,
                    'dataFilterPriceMin' => $dataFilterPriceMin,
                    'dataFilterPriceMax' => $dataFilterPriceMax,
                    'count' => $listCruises->count()
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $request,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        $listCruises = Cruise::query()
            ->with('cruiseCruiseArea.cruiseArea')
            ->with('couponCruise')
            ->with('cruiseFreeService.freeService')
            ->with('cabinPrice')
            ->with('review')
            ->orderBy('display_ranking', 'asc')
            ->Paginate($perPage);

        $listAreas = CruiseArea::query()->get();

        $minPrice = CabinPrice::query()->min('price');
        $maxPrice = CabinPrice::query()->max('price');

        return view('frontend.luxury')
            ->with([
                'listCruises' => $listCruises,
                'listAreas' => $listAreas,
                'minPrice' => $minPrice,
                'maxPrice' => $maxPrice,
            ]);
    }
}
