<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\BedType;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BedTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.bed-type.index');
    }

    public function datatable()
    {
        return DataTables::of(BedType::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.bed-type.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm btn-edit" data-id="'. $row->id .'" data-name="'. $row->name .'" 
                        data-remote="' . route('backend.bed-type.update', ['id' => $row->id]) . '">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.bed-type.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.bed-type.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->except('ajax');

        $inputs['name'] = clearXSS($request->name);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:bed_types,name,NULL,id,deleted_at,NULL',
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                BedType::query()->create($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            BedType::query()->create($inputs);

            return redirect()->route('backend.bed-type.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.bed-type')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.create-errored', ['name' => __('backend.bed-type')]));
        }
    }

    public function show(BedType $BedType)
    {
        //
    }

    public function edit($id)
    {
        $data = BedType::query()->findOrFail($id);

        return view('backend.bed-type.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except('ajax');

        $inputs['name'] = clearXSS($request->name);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:bed_types,name,'. $id .',id,deleted_at,NULL',
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                BedType::query()->findOrFail($id)->update($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            BedType::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.bed-type.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.bed-type')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend.bed-type')]));
        }
    }

    public function destroy($id)
    {
        try {
            BedType::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
