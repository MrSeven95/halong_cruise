<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\FreeService;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FreeServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.free-service.index');
    }

    public function datatable()
    {
        return DataTables::of(FreeService::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.free-service.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm btn-edit" data-id="'. $row->id .'" data-name="'. $row->name .'" 
                        data-remote="' . route('backend.free-service.update', ['id' => $row->id]) . '">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.free-service.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.free-service.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->except('ajax');

        $inputs['name'] = clearXSS($request->name);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:free_services,name,NULL,id,deleted_at,NULL',
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                FreeService::query()->create($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            FreeService::query()->create($inputs);

            return redirect()->route('backend.free-service.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.free-service')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.create-errored', ['name' => __('backend.free-service')]));
        }
    }

    public function show(FreeService $FreeService)
    {
        //
    }

    public function edit($id)
    {
        $data = FreeService::query()->findOrFail($id);

        return view('backend.free-service.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except('ajax');

        $inputs['name'] = clearXSS($request->name);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:free_services,name,'. $id .',id,deleted_at,NULL',
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                FreeService::query()->findOrFail($id)->update($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->ajax() && $request->has('ajax')) {
            try {
                FreeService::query()->findOrFail($id)->update($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }

        try {
            FreeService::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.free-service.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.free-service')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('backend.free-service')]))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            FreeService::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
