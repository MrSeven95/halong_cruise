<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OtherService;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OtherServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.other-service.index');
    }

    public function datatable()
    {
        return DataTables::of(OtherService::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.other-service.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.other-service.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.other-service.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->all();

        $inputs['name'] = clearXSS($request->name);
        $inputs['price'] = clearXSS($request->price);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:other_services,name,NULL,id,deleted_at,NULL',
            'price' => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            OtherService::query()->create($inputs);

            return redirect()->route('backend.other-service.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.other-service')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.other-service')]))
                ->withInput();
        }
    }

    public function show(Otherervice $OtherService)
    {
        //
    }

    public function edit($id)
    {
        $data = OtherService::query()->findOrFail($id);

        return view('backend.other-service.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['name'] = clearXSS($request->name);
        $inputs['price'] = clearXSS($request->price);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:other_services,name,'. $id .',id,deleted_at,NULL',
            'price' => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            OtherService::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.other-service.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.other-service')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('backend.other-service')]))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            OtherService::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
