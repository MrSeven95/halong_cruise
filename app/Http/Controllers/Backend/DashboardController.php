<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('backend.dashboard.index');
    }

    /*public function removeImageUpload(Request $request)
    {
        $dir = public_path('uploads/');
        $i = 0;
        foreach(glob($dir.'*.*') as $v){
            $i++;
            unlink($v);
        }
        return $i;
    }*/
}
