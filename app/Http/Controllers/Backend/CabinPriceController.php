<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\BedType;
use App\Models\Cabin;
use App\Models\CabinPrice;
use App\Models\Cruise;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CabinPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable($id);
        }

        $data = Cabin::query()->findOrFail($id);
        $cruise = Cruise::query()->findOrFail($data->cruise_id);

        return view('backend.cabin-price.index')
            ->with('data', $data)
            ->with('cruise', $cruise);
    }

    public function datatable($cabin_id)
    {
        return DataTables::of(CabinPrice::where(['cabin_id' => $cabin_id])->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
        /*        $btn = '<a href="' . route('backend.cabin-price.edit', ['id' => $row->id]) . '"
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';*/

                $btn = '<a href="#" data-remote="' . route('backend.cabin-price.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['cabin_id'] = clearXSS($request->cabin_id);
        $inputs['type'] = clearXSS($request->type);
        $inputs['price'] = clearXSS($request->price);
        $inputs['surchange'] = clearXSS($request->surchange);
        $inputs['less_than_one_person'] = clearXSS($request->less_than_one_person);

        $validator = Validator::make($inputs, [
            'cabin_id' => 'required|numeric|min:1',
            'type' => 'required|max:255',
            'price' => 'required|numeric|min:1',
            'surchange' => 'required|max:255',
            'less_than_one_person' => 'required|numeric|min:0|max:100',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $exist = CabinPrice::query()->where('cabin_id', '=', $id)->where('type', '=', $inputs['type'])->get('id');
            if ($exist && $exist->count() > 0) {
                $id_cabin_price = $exist->first()->id;
                CabinPrice::query()->findOrFail($id_cabin_price)->update($inputs);

                return redirect()->route('backend.cabin-price.index', ['id' => $id])
                    ->with('success', __('web.edit-successed', ['name' => 'cabin price']));
            } else {
                CabinPrice::query()->create($inputs);

                return redirect()->route('backend.cabin-price.index', ['id' => $id])
                    ->with('success', __('web.create-successed', ['name' => 'cabin price']));
            }
        } catch (\Exception $exception) {
            return back()
                ->with('error', __('web.create-errored', ['name' => __('backend.cabin-price')]))
                ->with('exception', $exception->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function show(Cabin $cabin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            CabinPrice::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
