<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ConfigCruise;
use App\Models\Cruise;
use App\Models\CruiseArea;
use App\Models\CruiseCruiseArea;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CruiseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.cruise.index');
    }


    public function show(Request $request, $id)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->dataCruise($id);
        }

        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise.show')
            ->with([
                'data' => $data
            ]);
    }

    public function dataCruise($id) {
        return DataTables::of(Cruise::where('id', '=', $id)->get())
            ->addColumn('image', function ($row) {
                $image = $row->image_thumbnail;
                if ($image) return $image;
                return null;
            })
            ->addColumn('more', function ($row) {
                $res = '<div class="row" style="line-height: 2.2;">';
                $res .= '<ul class="list-unstyled ml-2 mr-2 mb-0">';
                $res .= '<li class="mb-2 mr-2"><a href="'. route('backend.cruise.cabin.index', ['id' => $row->id]) .'" 
                        class="">'. __('backend.cabin') .'</a></li>';

                $res .= '<li class="mb-2 mr-2"><a href="'. route('backend.cruise.policy.edit', ['id' => $row->id]) .'" 
                        class="">'. __('backend/cruise.policy') .'</a></li>';

                $res .= '<li class="mb-2 mr-2"><a href="'. route('backend.cruise.journal.index', ['id' => $row->id]) .'" 
                        class="">'. __('backend/cruise.journal') .'</a></li>';
                $res .= '</ul>';
                $res .= '<ul class="list-unstyled ml-2 mr-2">';
                $res .= '<li class="mb-2"><a href="'. route('backend.media-cruise.album', ['id' => $row->id]) .'"
                        class="">'. __('backend/cruise.album') .'</a></li>';

                $res .= '<li class="mb-2"><a href="'. route('backend.cruise.additional.edit', ['id' => $row->id]) .'"
                        class="">'. __('backend/cruise.additional') .'</a></li>';

                $res .= '<li class=""><a href="#"
                        class="">'. __('backend/cruise.price_management') .'</a></li>';

                $res .= '</ul></div>';

                return $res;
            })
            ->addColumn('col_best_seller', function ($row) {
                $col = 'is_best_seller';
                $checked = ($row[$col] == 1 ? ' checked' : '');
                $res = '<div class="bt-switch" data-remote="'. route('backend.cruise.is-best-seller', $row->id) .'" 
                data-column="'. $col .'" data-name="'. __('backend/cruise.status') .'"><input type="checkbox"'. $checked .' data-id="'. $row['id'] .'" 
                data-on-color="success" data-off-color="info" data-on-text="'. __('backend/cruise.is_best_seller_on') .'" data-off-text="'. __('backend/cruise.is_best_seller_off') .'"></div>';

                return $res;
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.cruise.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.cruise.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->rawColumns(['more', 'col_best_seller', 'action'])
            ->make(true);
    }

    public function datatable()
    {
        return DataTables::of(Cruise::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('image', function ($row) {
                $image = $row->image_thumbnail;
                if ($image) return $image;
                return null;
            })
            ->addColumn('more', function ($row) {
                $res = '<div class="row" style="line-height: 2.2;">';
                $res .= '<ul class="list-unstyled ml-2 mr-2 mb-0">';
                $res .= '<li class="mb-2 mr-2"><a href="'. route('backend.cruise.cabin.index', ['id' => $row->id]) .'" 
                        class="">'. __('backend.cabin') .'</a></li>';

                $res .= '<li class="mb-2 mr-2"><a href="'. route('backend.cruise.policy.edit', ['id' => $row->id]) .'" 
                        class="">'. __('backend/cruise.policy') .'</a></li>';

                $res .= '<li class="mb-2 mr-2"><a href="'. route('backend.cruise.journal.index', ['id' => $row->id]) .'" 
                        class="">'. __('backend/cruise.journal') .'</a></li>';
                $res .= '</ul>';
                $res .= '<ul class="list-unstyled ml-2 mr-2">';
                $res .= '<li class="mb-2"><a href="'. route('backend.media-cruise.album', ['id' => $row->id]) .'"
                        class="">'. __('backend/cruise.album') .'</a></li>';

                $res .= '<li class="mb-2"><a href="'. route('backend.cruise.additional.edit', ['id' => $row->id]) .'"
                        class="">'. __('backend/cruise.additional') .'</a></li>';

                $res .= '<li class=""><a href="#"
                        class="">'. __('backend/cruise.price_management') .'</a></li>';

                $res .= '</ul></div>';

                return $res;
            })
            ->addColumn('col_best_seller', function ($row) {
                $col = 'is_best_seller';
                $checked = ($row[$col] == 1 ? ' checked' : '');
                $res = '<div class="bt-switch" data-remote="'. route('backend.cruise.is-best-seller', $row->id) .'" 
                data-column="'. $col .'" data-name="'. __('backend/cruise.status') .'"><input type="checkbox"'. $checked .' data-id="'. $row['id'] .'" 
                data-on-color="success" data-off-color="info" data-on-text="'. __('backend/cruise.is_best_seller_on') .'" data-off-text="'. __('backend/cruise.is_best_seller_off') .'"></div>';

                return $res;
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.cruise.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.cruise.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->rawColumns(['name_link', 'more', 'col_best_seller', 'action'])
            ->make(true);
    }

    public function create()
    {
        $listCruiseAreas = CruiseArea::query()->select('id', 'name')->get();
        $maxRanking = Cruise::query()->max('display_ranking');

        return view('backend.cruise.create')
            ->with('maxRanking', $maxRanking + 1)
            ->with('listCruiseAreas', $listCruiseAreas);
    }

    public function store(Request $request)
    {
        try {
            $inputs = $request->except('areas');

            $inputs['name'] = clearXSS($request->name);
            $inputs['slug'] = clearXSS($request->slug);
            $inputs['journal'] = clearXSS($request->journal);
            $inputs['address'] = clearXSS($request->address);
            $inputs['star'] = clearXSS($request->star);
            $inputs['cruise_category'] = clearXSS($request->cruise_category);
            $inputs['no_of_cabins'] = clearXSS($request->no_of_cabins);
            $inputs['launch_year'] = clearXSS($request->launch_year);
            $inputs['introduction'] = removeScripts(trim($inputs['introduction']));
            $inputs['meta_title'] = clearXSS($request->meta_title);
            $inputs['meta_description'] = clearXSS($request->meta_description);
            $inputs['meta_keyword'] = clearXSS($request->meta_keyword);
            $inputs['display_ranking'] = clearXSS($request->display_ranking);
            $inputs['status'] = clearXSS($request->status);

            $validator = Validator::make($inputs, [
                'name' => 'required|max:100|unique:cruises,name,NULL,id,deleted_at,NULL',
                'slug' => 'required|max:100',
                'journal' => 'required|max:255',
                'address' => 'required|max:255',
                'star' => 'required|numeric|min:1|max:5',
                'cruise_category' => 'required|max:100',
                'no_of_cabins' => 'required|numeric|min:10|max:50',
                'launch_year' => 'required|numeric',
                'introduction' => 'required',
                'meta_title' => 'required|max:255',
                'meta_description' => 'required|max:255',
                'meta_keyword' => 'required|max:255',
                'display_ranking' => 'required|numeric|min:1',
                'status' => 'required|max:30',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $slug = $inputs['slug'];

            if ($request->has('image_avatar')) {
                $inputs['image_avatar'] = upload_image($request->file('image_avatar'), $slug);
                $inputs['image_thumbnail'] = make_thumb('600', '400', $inputs['image_avatar']);
            }
            if ($request->has('image_seo')) {
                $inputs['image_seo'] = upload_image($request->file('image_seo'), $slug);
            }

            $areas = $request->areas;
            if (!is_array($areas)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend/cruise.area') .'!')
                    ->withInput();
            }

            DB::transaction(function () use ($inputs, $areas) {
                $id = Cruise::query()->create($inputs)->id;
                foreach ($areas as $area_id) {
                    CruiseCruiseArea::query()->create([
                        'cruise_id' => $id,
                        'cruise_area_id' => $area_id
                    ]);
                }
            });

            return redirect()->route('backend.cruise.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.cruise')]));
        } catch (\Exception $exception) {
            if (array_key_exists('image_avatar', $inputs) && exist_image($inputs['image_avatar'])) remove_image($inputs['image_avatar']);
            if (array_key_exists('image_thumbnail', $inputs) && exist_image($inputs['image_thumbnail'])) remove_image($inputs['image_thumbnail']);
            if (array_key_exists('image_seo', $inputs) && exist_image($inputs['image_seo'])) remove_image($inputs['image_seo']);

            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.cruise')]))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $listCruiseAreas = CruiseArea::query()->select('id', 'name')->get();
        $data = Cruise::query()->findOrFail($id);
        $temp = [];
        $dataAreas = CruiseCruiseArea::query()->where(['cruise_id' => $id])->get('cruise_area_id');
        foreach ($dataAreas as $dataArea) {
            array_push($temp, $dataArea->cruise_area_id);
        }
        $data['areas'] = $temp;

        return view('backend.cruise.edit')
            ->with([
                'listCruiseAreas' => $listCruiseAreas,
                'data' => $data
            ]);
    }

    public function isBestSeller(Request $request, $id)
    {
        $value = $request->is_best_seller;
        if ($value == 'true') $value = 1;
        elseif ($value == 'false') $value = 0;
        else {
            return response()->json([
                'status' => 'error'
            ]);
        }

        try {
            Cruise::query()->findOrFail($id)->update([
                'is_best_seller' => $value
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except(['is_no_action_image_avatar', 'is_no_action_image_seo', 'areas']);

        $inputs['name'] = clearXSS($request->name);
        $inputs['slug'] = clearXSS($request->slug);
        $inputs['journal'] = clearXSS($request->journal);
        $inputs['address'] = clearXSS($request->address);
        $inputs['star'] = clearXSS($request->star);
        $inputs['cruise_category'] = clearXSS($request->cruise_category);
        $inputs['no_of_cabins'] = clearXSS($request->no_of_cabins);
        $inputs['launch_year'] = clearXSS($request->launch_year);
        $inputs['introduction'] = removeScripts(trim($inputs['introduction']));
        $inputs['meta_title'] = clearXSS($request->meta_title);
        $inputs['meta_description'] = clearXSS($request->meta_description);
        $inputs['meta_keyword'] = clearXSS($request->meta_keyword);
        $inputs['display_ranking'] = clearXSS($request->display_ranking);
        $inputs['status'] = clearXSS($request->status);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:100|unique:cruises,name,'. $id .',id,deleted_at,NULL',
            'slug' => 'required|max:100',
            'journal' => 'required|max:255',
            'address' => 'required|max:255',
            'star' => 'required|numeric|min:1|max:5',
            'cruise_category' => 'required|max:255',
            'no_of_cabins' => 'required|numeric|min:10|max:50',
            'launch_year' => 'required|numeric',
            'introduction' => 'required',
            'meta_title' => 'required|max:255',
            'meta_description' => 'required|max:255',
            'meta_keyword' => 'required|max:255',
            'display_ranking' => 'required|numeric|min:1',
            'status' => 'required|max:30',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $arr_delete = [];
        try {
            $cruise = Cruise::query()->findOrFail($id);
            $slug = $cruise->slug;

            if ($request->has('image_avatar')) {
                $inputs['image_avatar'] = upload_image($request->file('image_avatar'), $slug);
                $inputs['image_thumbnail'] = make_thumb('600', '400', $inputs['image_avatar']);

                array_push($arr_delete, $cruise->image_avatar);
                array_push($arr_delete, $cruise->image_thumbnail);
            } else if ($request->has('is_no_action_image_avatar') && $request->input('is_no_action_image_avatar') == 'false') {
                $inputs['image_avatar'] = null;
                $inputs['image_thumbnail'] = null;

                array_push($arr_delete, $cruise->image_avatar);
                array_push($arr_delete, $cruise->image_thumbnail);
            }

            if ($request->has('image_seo')) {
                $inputs['image_seo'] = upload_image($request->file('image_seo'), $slug);

                array_push($arr_delete, $cruise->image_seo);
            } else if ($request->has('is_no_action_image_seo') && $request->input('is_no_action_image_seo') == 'false') {
                $inputs['image_seo'] = null;

                array_push($arr_delete, $cruise->image_seo);
            }

            $areas = $request->areas;
            if (!is_array($areas)) {
                return back()
                    ->with('exception', 'Wrong data journal.')
                    ->withInput();
            }

            DB::transaction(function () use ($inputs, $areas, $id, $cruise) {
                $cruise->update($inputs);
                CruiseCruiseArea::query()->where(['cruise_id' => $id])->delete();

                foreach ($areas as $area_id) {
                    CruiseCruiseArea::query()->create([
                        'cruise_id' => $id,
                        'cruise_area_id' => $area_id
                    ]);
                }
            });
        } catch (\Exception $exception) {
            if (array_key_exists('image_avatar', $inputs) && exist_image($inputs['image_avatar'])) remove_image($inputs['image_avatar']);
            if (array_key_exists('image_thumbnail', $inputs) && exist_image($inputs['image_thumbnail'])) remove_image($inputs['image_thumbnail']);
            if (array_key_exists('image_seo', $inputs) && exist_image($inputs['image_seo'])) remove_image($inputs['image_seo']);

            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('backend.cruise')]))
                ->withInput();
        }

        foreach ($arr_delete as $value) {
            if (exist_image($value)) remove_image($value);
        }

        return redirect()->route('backend.cruise.index')
            ->with('success', __('web.edit-successed', ['name' => __('backend.cruise')]));
    }

    public function editPolicy($id)
    {
        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise.policy')
            ->with([
                'data' => $data
            ]);
    }

    public function updatePolicy(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['price_include'] = removeScripts(trim($inputs['price_include']));
        $inputs['price_exclude'] = removeScripts(trim($inputs['price_exclude']));
        $inputs['cancel_by_customer'] = removeScripts(trim($inputs['cancel_by_customer']));
        $inputs['cancel_by_weather'] = removeScripts(trim($inputs['cancel_by_weather']));
        $inputs['children_policy'] = removeScripts(trim($inputs['children_policy']));

        $validator = Validator::make($inputs, [
            'price_include' => 'required',
            'price_exclude' => 'required',
            'cancel_by_customer' => 'required',
            'cancel_by_weather' => 'required',
            'children_policy' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            Cruise::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.cruise.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend/cruise.policy')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend/cruise.policy')]));
        }
    }

    public function editAdditional($id)
    {
        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise.additional')
            ->with([
                'data' => $data
            ]);
    }

    public function updateAdditional(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['services'] = removeScripts(trim($inputs['services']));
        $inputs['activities'] = removeScripts(trim($inputs['activities']));
        $inputs['departure_info'] = removeScripts(trim($inputs['departure_info']));
        $inputs['activities_on_request'] = removeScripts(trim($inputs['activities_on_request']));

        $validator = Validator::make($inputs, [
            'services' => 'required',
            'activities' => 'required',
            'departure_info' => 'required',
            'activities_on_request' => 'required'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            Cruise::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.cruise.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend/cruise.additional')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend/cruise.additional')]));
        }
    }

    public function destroy($id)
    {
        try {
            Cruise::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function editConfig()
    {
        $data = ConfigCruise::query()->first();
        return view('backend.cruise.config')
            ->with('data', $data);
    }

    public function updateConfig(Request $request)
    {
        $inputs = $request->except(['is_no_action_image_avatar', 'is_no_action_image_seo', 'areas']);

        $inputs['heading'] = removeScripts(trim($inputs['heading']));
        $inputs['slug'] = removeScripts(trim($inputs['slug']));
        $inputs['meta_title'] = removeScripts(trim($inputs['meta_title']));
        $inputs['meta_description'] = removeScripts(trim($inputs['meta_description']));
        $inputs['meta_keyword'] = removeScripts(trim($inputs['meta_keyword']));

        $validator = Validator::make($inputs, [
            'heading' => 'required|max:100',
            'slug' => 'required|max:100',
            'meta_title' => 'required|max:255',
            'meta_description' => 'required',
            'meta_keyword' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $slug = 'cruise';

            if ($request->has('image_avatar')) {
                $inputs['image_avatar'] = upload_image($request->file('image_avatar'), $slug);
                //$inputs['image_thumbnail'] = make_thumb('600', '400', $inputs['image_avatar']);
            } else if ($request->has('is_no_action_image_avatar') && $request->input('is_no_action_image_avatar') == 'false') {
                $inputs['image_avatar'] = null;
            }

            if ($request->has('image_seo')) {
                $inputs['image_seo'] = upload_image($request->file('image_seo'), $slug);
            } else if ($request->has('is_no_action_image_seo') && $request->input('is_no_action_image_seo') == 'false') {
                $inputs['image_seo'] = null;
            }

            if (ConfigCruise::query()->count('heading') == 1) {
                ConfigCruise::query()->first()->update($inputs);
            } else {
                ConfigCruise::query()->create($inputs);
            }

            return redirect()->route('backend.cruise.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.cruise-config')]));
        } catch (\Exception $exception) {
            if (array_key_exists('image_avatar', $inputs) && exist_image($inputs['image_avatar'])) remove_image($inputs['image_avatar']);
            if (array_key_exists('image_seo', $inputs) && exist_image($inputs['image_seo'])) remove_image($inputs['image_seo']);

            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend.cruise-config')]));
        }
    }
}
