<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cruise;
use App\Models\MediaCruise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MediaCruiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function swapRank(Request $request, $id) {
        try {
            $preId = $request->preId;
            $preDisplayRanking = $request->preDisplayRanking;
            $afterId = $request->afterId;
            $afterDisplayRanking = $request->afterDisplayRanking;

            DB::transaction(function() use ($preId, $preDisplayRanking, $afterId, $afterDisplayRanking) {
                MediaCruise::query()->findOrFail($preId)->update(['display_ranking' => $afterDisplayRanking]);
                MediaCruise::query()->findOrFail($afterId)->update(['display_ranking' => $preDisplayRanking]);
            });

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function album(Request $request, $id) {
        if ($request->ajax()) {
            return $this->datatable();
        }

        try {
            $cruise = Cruise::query()->findOrFail($id);
            $medias = MediaCruise::query()
                ->where('type_media', '!=', 'cruise_image_thumbnail')
                ->where('cruise_id', $id)
                ->orderBy('display_ranking', 'ASC')->get();

            return view('backend.media-cruise.album')
                ->with('data', $cruise)
                ->with('medias', $medias);
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.error'));
        }
    }

    public function datatable() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $medias = [];

        if ($request->has('image') && $request->hasFile('image')) {
            try {
                $cruise = Cruise::query()->findOrFail($request->cruise_id);

                $slug = $cruise->slug;

                $i = 1;
                foreach ($request->file('image') as $file) {
                    $highestDisplayRanking = MediaCruise::query()->max('display_ranking');
                    $highestDisplayRanking += 1;

                    $url = upload_image($file, $slug);

                    array_push($medias, [
                        'cruise_id' => $request->cruise_id,
                        'type_media' => 'cruise_image',
                        'url' => $url,
                        'display_ranking' => $highestDisplayRanking
                    ]);
                }

                MediaCruise::query()->insert($medias);

                return redirect()->route('backend.media-cruise.album', ['id' => $request->cruise_id])
                    ->with('success', __('web.upload-successed', ['name' => 'image']));
            } catch (\Exception $exception) {
                foreach ($medias as $media) {
                    if (exist_image($media['url'])) {
                        remove_image($media['url']);
                    }
                }

                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput()
                    ->with('error', __('web.upload-errored', ['name' => 'image']));
            }
        }
    }

    public function uploadPhoto(Request $request) {
        if ($request->has('upload') && $request->upload != '') {
            $slug = 'image';

            $image_url = upload_image($request->upload, $slug);
            $image_name = substr($image_url, 9, strlen($image_url));

            return response()->json([
                "uploaded" => 1,
                "fileName" => $image_name,
                "url" => $image_url
            ]);
        }
        return response()->json([
            "uploaded" => 0,
            "fileName" => '',
            "url" => ''
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MediaCruise  $mediaCruise
     * @return \Illuminate\Http\Response
     */
    public function show(MediaCruise $mediaCruise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MediaCruise  $mediaCruise
     * @return \Illuminate\Http\Response
     */
    public function edit(MediaCruise $mediaCruise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MediaCruise  $mediaCruise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MediaCruise $mediaCruise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MediaCruise  $mediaCruise
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            MediaCruise::query()->findOrFail($id)->delete();
            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
