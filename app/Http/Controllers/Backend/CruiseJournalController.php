<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cruise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CruiseJournalController extends Controller
{
    public function index(Request $request, $id)
    {
        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise-journal.index')
            ->with([
                'data' => $data
            ]);
    }

    public function create($id)
    {
        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise-journal.create')
            ->with([
                'data' => $data
            ]);
    }

    public function store(Request $request, $id)
    {
        $cruise = Cruise::query()->findOrFail($id);

        if ($request->type_journal == '1') {
            if ($cruise->has_journal_type1 == 1) {
                return back()
                    ->with('exception', 'This '. __('backend/cruise.journal') .' type has be created. Please re-check.')
                    ->withInput();
            }
            $request->merge(['has_journal_type1' => 1]);

            $inputs = $request->except('type_journal',
                'type1_day1_album',
                'type1_day2_album'
            );

            try {
                $inputs['journey_type1_day1_title'] = clearXSS($request->journey_type1_day1_title);
                $inputs['journey_type1_day1_detail'] = removeScripts(trim($request->journey_type1_day1_detail));
                $inputs['journey_type1_day2_title'] = clearXSS($request->journey_type1_day2_title);
                $inputs['journey_type1_day2_detail'] = removeScripts(trim($request->journey_type1_day2_detail));

                $validator = Validator::make($inputs, [
                    'journey_type1_day1_title' => 'required|max:100',
                    'journey_type1_day1_detail' => 'required',
                    'journey_type1_day2_title' => 'required|max:100',
                    'journey_type1_day2_detail' => 'required',
                ]);

                if ($validator->fails()) {
                    return back()
                        ->withErrors($validator)
                        ->withInput();
                }

                $slug = $cruise->slug;

                if ($request->has('type1_day1_album') && $request->hasFile('type1_day1_album')) {
                    $i = 1;
                    foreach ($request->file('type1_day1_album') as $file) {
                        $inputs['journey_type1_day1_image_'. $i++] = upload_image($file, $slug);
                    }
                }

                if ($request->has('type1_day2_album') && $request->hasFile('type1_day2_album')) {
                    $i = 1;
                    foreach ($request->file('type1_day2_album') as $file) {
                        $inputs['journey_type1_day2_image_'. $i++] = upload_image($file, $slug);
                    }
                }

                $cruise->update($inputs);

                return redirect()->route('backend.cruise.journal.index', ['id' => $id])
                    ->with('success', __('web.create-successed', ['name' => __('backend/cruise.journal')]));
            } catch (\Exception $exception) {
                if (array_key_exists('journey_type1_day1_image_1', $inputs) && exist_image($inputs['journey_type1_day1_image_1'])) remove_image($inputs['journey_type1_day1_image_1']);
                if (array_key_exists('journey_type1_day1_image_2', $inputs) && exist_image($inputs['journey_type1_day1_image_2'])) remove_image($inputs['journey_type1_day1_image_2']);
                if (array_key_exists('journey_type1_day1_image_3', $inputs) && exist_image($inputs['journey_type1_day1_image_3'])) remove_image($inputs['journey_type1_day1_image_3']);

                if (array_key_exists('journey_type1_day2_image_1', $inputs) && exist_image($inputs['journey_type1_day2_image_1'])) remove_image($inputs['journey_type1_day2_image_1']);
                if (array_key_exists('journey_type1_day2_image_2', $inputs) && exist_image($inputs['journey_type1_day2_image_2'])) remove_image($inputs['journey_type1_day2_image_2']);
                if (array_key_exists('journey_type1_day2_image_3', $inputs) && exist_image($inputs['journey_type1_day2_image_3'])) remove_image($inputs['journey_type1_day2_image_3']);

                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput()
                    ->with('error', __('web.edit-errored', ['name' => __('backend/cruise.journal')]));
            }

        } elseif ($request->type_journal == '2') {
            if ($cruise->has_journal_type2 == 1) {
                return back()
                    ->with('exception', 'This '. __('backend/cruise.journal') .' type has be created. Please re-check.')
                    ->withInput();
            }
            $request->merge(['has_journal_type2' => 1]);

            $inputs = $request->except('type_journal',
                'type2_day1_album',
                'type2_day2_album',
                'type2_day3_album'
            );

            try {
                $inputs['journey_type2_day1_title'] = clearXSS($request->journey_type2_day1_title);
                $inputs['journey_type2_day1_detail'] = removeScripts(trim($request->journey_type2_day1_detail));
                $inputs['journey_type2_day2_title'] = clearXSS($request->journey_type2_day2_title);
                $inputs['journey_type2_day2_detail'] = removeScripts(trim($request->journey_type2_day2_detail));
                $inputs['journey_type2_day3_title'] = clearXSS($request->journey_type2_day3_title);
                $inputs['journey_type2_day3_detail'] = removeScripts(trim($request->journey_type2_day3_detail));

                $validator = Validator::make($inputs, [
                    'journey_type2_day1_title' => 'required|max:100',
                    'journey_type2_day1_detail' => 'required',
                    'journey_type2_day2_title' => 'required|max:100',
                    'journey_type2_day2_detail' => 'required',
                    'journey_type2_day3_title' => 'required|max:100',
                    'journey_type2_day3_detail' => 'required',
                ]);

                if ($validator->fails()) {
                    return back()
                        ->withErrors($validator)
                        ->withInput();
                }

                $slug = $cruise->slug;

                if ($request->has('type2_day1_album') && $request->hasFile('type2_day1_album')) {
                    $i = 1;
                    foreach ($request->file('type2_day1_album') as $file) {
                        $inputs['journey_type2_day1_image_'. $i++] = upload_image($file, $slug);
                    }
                }
                if ($request->has('type2_day2_album') && $request->hasFile('type2_day2_album')) {
                    $i = 1;
                    foreach ($request->file('type2_day2_album') as $file) {
                        $inputs['journey_type2_day2_image_'. $i++] = upload_image($file, $slug);
                    }
                }
                if ($request->has('type2_day3_album') && $request->hasFile('type2_day3_album')) {
                    $i = 1;
                    foreach ($request->file('type2_day3_album') as $file) {
                        $inputs['journey_type2_day3_image_'. $i++] = upload_image($file, $slug);
                    }
                }

                $cruise->update($inputs);

                return redirect()->route('backend.cruise.journal.index', ['id' => $id])
                    ->with('success', __('web.create-successed', ['name' => __('backend/cruise.journal')]));
            } catch (\Exception $exception) {
                if (array_key_exists('journey_type2_day1_image_1',
                        $inputs) && exist_image($inputs['journey_type2_day1_image_1'])) {
                    remove_image($inputs['journey_type2_day1_image_1']);
                }
                if (array_key_exists('journey_type2_day1_image_2',
                        $inputs) && exist_image($inputs['journey_type2_day1_image_2'])) {
                    remove_image($inputs['journey_type2_day1_image_2']);
                }
                if (array_key_exists('journey_type2_day1_image_3',
                        $inputs) && exist_image($inputs['journey_type2_day1_image_3'])) {
                    remove_image($inputs['journey_type2_day1_image_3']);
                }

                if (array_key_exists('journey_type2_day2_image_1',
                        $inputs) && exist_image($inputs['journey_type2_day2_image_1'])) {
                    remove_image($inputs['journey_type2_day2_image_1']);
                }
                if (array_key_exists('journey_type2_day2_image_2',
                        $inputs) && exist_image($inputs['journey_type2_day2_image_2'])) {
                    remove_image($inputs['journey_type2_day2_image_2']);
                }
                if (array_key_exists('journey_type2_day2_image_3',
                        $inputs) && exist_image($inputs['journey_type2_day2_image_3'])) {
                    remove_image($inputs['journey_type2_day2_image_3']);
                }

                if (array_key_exists('journey_type2_day3_image_1',
                        $inputs) && exist_image($inputs['journey_type2_day3_image_1'])) {
                    remove_image($inputs['journey_type2_day3_image_1']);
                }
                if (array_key_exists('journey_type2_day3_image_2',
                        $inputs) && exist_image($inputs['journey_type2_day3_image_2'])) {
                    remove_image($inputs['journey_type2_day3_image_2']);
                }
                if (array_key_exists('journey_type2_day3_image_3',
                        $inputs) && exist_image($inputs['journey_type2_day3_image_3'])) {
                    remove_image($inputs['journey_type2_day3_image_3']);
                }

                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput()
                    ->with('error', __('web.edit-errored', ['name' => __('backend/cruise.journal')]));
            }
        } else {
            return back()
                ->with('error', __('web.create-errored', ['name' => __('backend/cruise.journal')]))
                ->withInput();
        }
    }

    public function editType1($id)
    {
        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise-journal.edit-type-1')
            ->with([
                'data' => $data
            ]);
    }
    public function editType2($id)
    {
        $data = Cruise::query()->findOrFail($id);

        return view('backend.cruise-journal.edit-type-2')
            ->with([
                'data' => $data
            ]);
    }

    public function initUpdateImage($day, $inputs, $cruise, $request, $arr_delete) {
        $count = 0;
        if ($cruise['journey_'. $day .'image_1'] != '' && $request->has('is_no_action_'. $day .'image_album_0') && $request->input('is_no_action_'. $day .'image_album_0') == 'true') {
            $count++;
        }
        if ($cruise['journey_'. $day .'image_2'] != '' && $request->has('is_no_action_'. $day .'image_album_1') && $request->input('is_no_action_'. $day .'image_album_1') == 'true') {
            $count++;
        }
        if ($cruise['journey_'. $day .'image_3'] != '' && $request->has('is_no_action_'. $day .'image_album_2') && $request->input('is_no_action_'. $day .'image_album_2') == 'true') {
            $count++;
        }

        if ($request->has('is_no_action_'. $day .'image_album_0') && $request->input('is_no_action_'. $day .'image_album_0') == 'false') {
            $inputs['journey_'. $day .'image_1'] = null;

            array_push($arr_delete, $cruise['journey_'. $day .'image_1']);
        }
        if ($request->has('is_no_action_'. $day .'image_album_1') && $request->input('is_no_action_'. $day .'image_album_1') == 'false') {
            $inputs['journey_'. $day .'image_2'] = null;

            array_push($arr_delete, $cruise['journey_'. $day .'image_2']);
        }
        if ($request->has('is_no_action_'. $day .'image_album_2') && $request->input('is_no_action_'. $day .'image_album_2') == 'false') {
            $inputs['journey_'. $day .'image_3'] = null;

            array_push($arr_delete, $cruise['journey_'. $day .'image_3']);
        }

        $slug = $cruise->slug;

        if ($request->has(''. $day .'album') && $request->hasFile(''. $day .'album')) {
            $i = $count;
            foreach ($request->file(''. $day .'album') as $file) {
                $inputs['journey_'. $day .'image_'. ++$i] = upload_image($file, $slug);
            }
        }
        return array($inputs, $arr_delete);
    }

    public function updateType1(Request $request, $id)
    {
        $cruise = Cruise::query()->findOrFail($id);

        $inputs = $request->except('type_journal',
            'type1_day1_album',
            'is_no_action_type1_day1_image_album_0',
            'is_no_action_type1_day1_image_album_1',
            'is_no_action_type1_day1_image_album_2',
            'type1_day2_album',
            'is_no_action_type1_day2_image_album_0',
            'is_no_action_type1_day2_image_album_1',
            'is_no_action_type1_day2_image_album_2'
        );

        $arr_delete = [];
        try {
            $inputs['journey_type1_day1_title'] = clearXSS($request->journey_type1_day1_title);
            $inputs['journey_type1_day1_detail'] = removeScripts(trim($request->journey_type1_day1_detail));
            $inputs['journey_type1_day2_title'] = clearXSS($request->journey_type1_day2_title);
            $inputs['journey_type1_day2_detail'] = removeScripts(trim($request->journey_type1_day2_detail));

            $validator = Validator::make($inputs, [
                'journey_type1_day1_title' => 'required|max:100',
                'journey_type1_day1_detail' => 'required',
                'journey_type1_day2_title' => 'required|max:100',
                'journey_type1_day2_detail' => 'required',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $init = $this->initUpdateImage('type1_day1_', $inputs, $cruise, $request, $arr_delete);
            $inputs = $init[0];
            $arr_delete = $init[1];

            $init = $this->initUpdateImage('type1_day2_', $inputs, $cruise, $request, $arr_delete);
            $inputs = $init[0];
            $arr_delete = $init[1];

            $cruise->update($inputs);

            return redirect()->route('backend.cruise.journal.index', ['id' => $id])
                ->with('success', __('web.create-successed', ['name' => __('backend/cruise.journal')]));
        } catch (\Exception $exception) {
            if (array_key_exists('journey_type1_day1_image_1', $inputs) && exist_image($inputs['journey_type1_day1_image_1'])) remove_image($inputs['journey_type1_day1_image_1']);
            if (array_key_exists('journey_type1_day1_image_2', $inputs) && exist_image($inputs['journey_type1_day1_image_2'])) remove_image($inputs['journey_type1_day1_image_2']);
            if (array_key_exists('journey_type1_day1_image_3', $inputs) && exist_image($inputs['journey_type1_day1_image_3'])) remove_image($inputs['journey_type1_day1_image_3']);

            if (array_key_exists('journey_type1_day2_image_1', $inputs) && exist_image($inputs['journey_type1_day2_image_1'])) remove_image($inputs['journey_type1_day2_image_1']);
            if (array_key_exists('journey_type1_day2_image_2', $inputs) && exist_image($inputs['journey_type1_day2_image_2'])) remove_image($inputs['journey_type1_day2_image_2']);
            if (array_key_exists('journey_type1_day2_image_3', $inputs) && exist_image($inputs['journey_type1_day2_image_3'])) remove_image($inputs['journey_type1_day2_image_3']);

            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend/cruise.journal')]));
        }
    }

    public function updateType2(Request $request, $id)
    {
        $cruise = Cruise::query()->findOrFail($id);

        $inputs = $request->except('type_journal',
            'type2_day1_album',
            'is_no_action_type2_day1_image_album_0',
            'is_no_action_type2_day1_image_album_1',
            'is_no_action_type2_day1_image_album_2',
            'type2_day2_album',
            'is_no_action_type2_day2_image_album_0',
            'is_no_action_type2_day2_image_album_1',
            'is_no_action_type2_day2_image_album_2',
            'type2_day3_album',
            'is_no_action_type2_day3_image_album_0',
            'is_no_action_type2_day3_image_album_1',
            'is_no_action_type2_day3_image_album_2'
        );

        $arr_delete = [];
        try {
            $inputs['journey_type2_day1_title'] = clearXSS($request->journey_type2_day1_title);
            $inputs['journey_type2_day1_detail'] = removeScripts(trim($request->journey_type2_day1_detail));
            $inputs['journey_type2_day2_title'] = clearXSS($request->journey_type2_day2_title);
            $inputs['journey_type2_day2_detail'] = removeScripts(trim($request->journey_type2_day2_detail));
            $inputs['journey_type2_day3_title'] = clearXSS($request->journey_type2_day3_title);
            $inputs['journey_type2_day3_detail'] = removeScripts(trim($request->journey_type2_day3_detail));

            $validator = Validator::make($inputs, [
                'journey_type2_day1_title' => 'required|max:100',
                'journey_type2_day1_detail' => 'required',
                'journey_type2_day2_title' => 'required|max:100',
                'journey_type2_day2_detail' => 'required',
                'journey_type2_day3_title' => 'required|max:100',
                'journey_type2_day3_detail' => 'required',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $init = $this->initUpdateImage('type2_day1_', $inputs, $cruise, $request, $arr_delete);
            $inputs = $init[0];
            $arr_delete = $init[1];

            $init = $this->initUpdateImage('type2_day2_', $inputs, $cruise, $request, $arr_delete);
            $inputs = $init[0];
            $arr_delete = $init[1];

            $init = $this->initUpdateImage('type2_day3_', $inputs, $cruise, $request, $arr_delete);
            $inputs = $init[0];
            $arr_delete = $init[1];

            $cruise->update($inputs);

            return redirect()->route('backend.cruise.journal.index', ['id' => $id])
                ->with('success', __('web.create-successed', ['name' => __('backend/cruise.journal')]));
        } catch (\Exception $exception) {
            if (array_key_exists('journey_type2_day1_image_1', $inputs) && exist_image($inputs['journey_type2_day1_image_1'])) remove_image($inputs['journey_type2_day1_image_1']);
            if (array_key_exists('journey_type2_day1_image_2', $inputs) && exist_image($inputs['journey_type2_day1_image_2'])) remove_image($inputs['journey_type2_day1_image_2']);
            if (array_key_exists('journey_type2_day1_image_3', $inputs) && exist_image($inputs['journey_type2_day1_image_3'])) remove_image($inputs['journey_type2_day1_image_3']);

            if (array_key_exists('journey_type2_day2_image_1', $inputs) && exist_image($inputs['journey_type2_day2_image_1'])) remove_image($inputs['journey_type2_day2_image_1']);
            if (array_key_exists('journey_type2_day2_image_2', $inputs) && exist_image($inputs['journey_type2_day2_image_2'])) remove_image($inputs['journey_type2_day2_image_2']);
            if (array_key_exists('journey_type2_day2_image_3', $inputs) && exist_image($inputs['journey_type2_day2_image_3'])) remove_image($inputs['journey_type2_day2_image_3']);

            if (array_key_exists('journey_type2_day3_image_1', $inputs) && exist_image($inputs['journey_type2_day3_image_1'])) remove_image($inputs['journey_type2_day3_image_1']);
            if (array_key_exists('journey_type2_day3_image_2', $inputs) && exist_image($inputs['journey_type2_day3_image_2'])) remove_image($inputs['journey_type2_day3_image_2']);
            if (array_key_exists('journey_type2_day3_image_3', $inputs) && exist_image($inputs['journey_type2_day3_image_3'])) remove_image($inputs['journey_type2_day3_image_3']);

            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend/cruise.journal')]));
        }
    }

    public function destroyType1($id)
    {
        try {
            Cruise::query()->findOrFail($id)->update([
                'has_journal_type1' => 0,
                'journey_type1_day1_title' => '',
                'journey_type1_day1_detail' => '',
                'journey_type1_day2_title' => '',
                'journey_type1_day2_detail' => '',

                'journey_type1_day1_image_1' => '',
                'journey_type1_day1_image_2' => '',
                'journey_type1_day1_image_3' => '',
                'journey_type1_day2_image_1' => '',
                'journey_type1_day2_image_2' => '',
                'journey_type1_day2_image_3' => '',
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
    public function destroyType2($id)
    {
        try {
            Cruise::query()->findOrFail($id)->update([
                'has_journal_type2' => 0,
                'journey_type2_day1_title' => '',
                'journey_type2_day1_detail' => '',
                'journey_type2_day2_title' => '',
                'journey_type2_day2_detail' => '',
                'journey_type2_day3_title' => '',
                'journey_type2_day3_detail' => '',

                'journey_type2_day1_image_1' => '',
                'journey_type2_day1_image_2' => '',
                'journey_type2_day1_image_3' => '',
                'journey_type2_day2_image_1' => '',
                'journey_type2_day2_image_2' => '',
                'journey_type2_day2_image_3' => '',
                'journey_type2_day3_image_1' => '',
                'journey_type2_day3_image_2' => '',
                'journey_type2_day3_image_3' => '',
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
