<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.coupon.index');
    }

    public function datatable()
    {
        return DataTables::of(Coupon::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.coupon.edit', ['id' => $row->id]) . '"
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.coupon.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                /*$btn = '<a href="' . route('backend.coupon.edit', ['id' => $row->id]) . '"
                        class="btn btn-primary btn-sm btn-edit" data-id="'. $row->id .'" data-name="'. $row->name .'" 
                        data-remote="' . route('backend.coupon.update', ['id' => $row->id]) . '">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.coupon.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';*/

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.coupon.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->all();

        $inputs['name'] = clearXSS($request->name);
        //$inputs['value_coupon'] = clearXSS($request->value_coupon);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:coupons,name,NULL,id,deleted_at,NULL',
            //'value_coupon' => 'required|numeric|min:0|max:100'
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                Coupon::query()->create($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            Coupon::query()->create($inputs);

            return redirect()->route('backend.coupon.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.coupon')]));
        } catch (\Exception $exception) {
            return back()
                ->with('error', __('web.create-errored', ['name' => __('backend.coupon')]))
                ->with('exception', $exception->getMessage())
                ->withInput();
        }
    }

    public function show(Coupon $coupon)
    {
        //
    }

    public function edit($id)
    {
        $data = Coupon::query()->findOrFail($id);

        return view('backend.coupon.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['name'] = clearXSS($request->name);
        //$inputs['value_coupon'] = clearXSS($request->value_coupon);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:coupons,name,'. $id .',id,deleted_at,NULL',
            //'value_coupon' => 'required|numeric|min:0|max:100'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                Coupon::query()->findOrFail($id)->update($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        try {
            Coupon::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.coupon.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.coupon')]));
        } catch (\Exception $exception) {
            return back()
                ->with('error', __('web.edit-errored', ['name' => __('backend.coupon')]))
                ->with('exception', $exception->getMessage())
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            Coupon::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
