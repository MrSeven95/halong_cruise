<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\CruiseArea;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CruiseAreaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.cruise-area.index');
    }

    public function datatable()
    {
        return DataTables::of(CruiseArea::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.cruise-area.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm btn-edit" data-id="'. $row->id .'" data-name="'. $row->name .'" 
                        data-remote="' . route('backend.cruise-area.update', ['id' => $row->id]) . '">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.cruise-area.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.cruise-area.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->except('ajax');

        $inputs['name'] = clearXSS($request->name);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:cruise_areas,name,NULL,id,deleted_at,NULL',
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                CruiseArea::query()->create($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            CruiseArea::query()->create($inputs);

            return redirect()->route('backend.cruise-area.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.cruise-area')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.create-errored', ['name' => __('backend.cruise-area')]));
        }
    }

    public function show(CruiseArea $cruiseArea)
    {
        //
    }

    public function edit($id)
    {
        $data = CruiseArea::query()->findOrFail($id);

        return view('backend.cruise-area.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except('ajax');

        $inputs['name'] = clearXSS($request->name);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:cruise_areas,name,'. $id .',id,deleted_at,NULL',
        ]);

        if ($request->ajax() && $request->has('ajax')) {
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'validator' => $validator->errors()
                ]);
            }

            try {
                CruiseArea::query()->findOrFail($id)->update($inputs);

                return response()->json([
                    'status' => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'data' => $inputs,
                    'exception' => $e->getMessage()
                ]);
            }
        }

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            CruiseArea::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.cruise-area.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.cruise-area')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend.cruise-area')]));
        }
    }

    public function destroy($id)
    {
        try {
            CruiseArea::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
