<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Cabin;
use DataTables;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.booking.index');
    }

    public function datatable()
    {
        return DataTables::of(Booking::query()
            ->with('cruise:id,name')
            ->with('cabin:id,name')
            ->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.booking.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.booking.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete-booking text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->addColumn('cruise_name', function ($row) {
                return $row->cruise ? $row->cruise->name : '';
            })
            ->addColumn('cabin_name', function ($row) {
                return $row->cabin ? $row->cabin->name : '';
            })
            ->addColumn('email_phone_number', function ($row) {
                return ($row->email ? $row->email .' - ' : '') . ($row->phone_number ? $row->phone_number : '');
            })
            ->addColumn('passenger', function ($row) {
                return ($row->num_adults ? $row->num_adults .' adults - ' : '') . ($row->num_childs ? $row->num_childs .' childs' : '');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function ajaxDashboardChartist(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->dataAjaxDashboardChartist($request);
        }

        return null;
    }

    public function dataAjaxDashboardChartist(Request $request)
    {
        try {
            $res = array();

            $fromData = $request->fromDate;
            $toDate = $request->toDate;

            $typeView = $request->typeView;

            switch ($typeView)
            {
                case 'day':
                    $res['labels'] = [];
                    break;
            }

            $res['labels'] = ['0', '1'];
            $res['low'] = 0;
            $res['high'] = 29;
            $res['series'] = ['5', '10'];
            $res['req'] = $request;

            return response()->json([
                'status' => 'success',
                'dataChartist' => $res
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function ajaxDashboardTable(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->dataAjaxDashboardTable();
        }
    }

    public function dataAjaxDashboardTable()
    {

        return DataTables::of(Booking::query()
            ->with('cruise:id,name')
            ->with('cabin:id,name')
            ->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.booking.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.booking.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete-booking text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->addColumn('cruise_name', function ($row) {
                return $row->cruise ? $row->cruise->name : '';
            })
            ->addColumn('cabin_name', function ($row) {
                return $row->cabin ? $row->cabin->name : '';
            })
            ->addColumn('email_phone_number', function ($row) {
                return ($row->email ? $row->email .' - ' : '') . ($row->phone_number ? $row->phone_number : '');
            })
            ->addColumn('passenger', function ($row) {
                return ($row->num_adults ? $row->num_adults .' adults - ' : '') . ($row->num_childs ? $row->num_childs .' childs' : '');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
