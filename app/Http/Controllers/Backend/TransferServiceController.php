<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\TransferService;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransferServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.transfer-service.index');
    }

    public function datatable()
    {
        return DataTables::of(TransferService::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.transfer-service.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.transfer-service.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.transfer-service.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->all();

        $inputs['name'] = clearXSS($request->name);
        $inputs['type_transfer'] = clearXSS($request->type_transfer);
        $inputs['type_price'] = clearXSS($request->type_price);
        $inputs['price'] = clearXSS($request->price);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:transfer_services,name,NULL,id,deleted_at,NULL',
            'type_transfer' => 'required|max:255',
            'type_price' => 'required|max:255',
            'price' => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            TransferService::query()->create($inputs);

            return redirect()->route('backend.transfer-service.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.transfer-service')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.transfer-service')]))
                ->withInput();
        }
    }

    public function show(TransferService $transferService)
    {
        //
    }

    public function edit($id)
    {
        $data = TransferService::query()->findOrFail($id);

        return view('backend.transfer-service.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['name'] = clearXSS($request->name);
        $inputs['type_transfer'] = clearXSS($request->type_transfer);
        $inputs['type_price'] = clearXSS($request->type_price);
        $inputs['price'] = clearXSS($request->price);

        $validator = Validator::make($inputs, [
            'name' => 'required|max:255|unique:transfer_services,name,'. $id .',id,deleted_at,NULL',
            'type_transfer' => 'required|max:255',
            'type_price' => 'required|max:255',
            'price' => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            TransferService::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.transfer-service.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.transfer-service')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('backend.transfer-service')]))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            TransferService::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
