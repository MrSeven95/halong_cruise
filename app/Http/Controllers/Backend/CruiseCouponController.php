<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Cruise;
use App\Models\CruiseCoupon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CruiseCouponController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.cruise-coupon.index');
    }


    public function show(Request $request, $id)
    {
        //
    }

    public function datatable() {
        return DataTables::of(CruiseCoupon::query()
            ->with('cruise:id,name')
            ->with('coupon:id,name')
            ->get())
            ->addIndexColumn()
            ->addColumn('cruise', function ($row) {
                return $row->cruise ? $row->cruise->name : '';
            })
            ->addColumn('coupon', function ($row) {
                return $row->coupon ? $row->coupon->name : '';
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.cruise-coupon.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.cruise-coupon.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listCruises = Cruise::query()->select('id', 'name')->get();
        $listCoupons = Coupon::query()->select('id', 'name')->get();

        return view('backend.cruise-coupon.create')
            ->with([
                'listCruises' => $listCruises,
                'listCoupons' => $listCoupons
            ]);
    }

    public function store(Request $request)
    {
        try {
            $inputs = $request->all();

            $inputs['coupon_id'] = clearXSS($request->coupon_id);
            $inputs['discount'] = clearXSS($request->discount);
            $inputs['from'] = clearXSS($request->from);
            $inputs['to'] = clearXSS($request->to);

            try {
                $inputs['from'] = \DateTime::createFromFormat('d/m/Y', $request->from);
                if ($inputs['from']) {
                    $inputs['from'] = $inputs['from']->format('Y-m-d');
                }
                $inputs['to'] = \DateTime::createFromFormat('d/m/Y', $request->to);
                if ($inputs['to']) {
                    $inputs['to'] = $inputs['to']->format('Y-m-d');
                }
            } catch (\Exception $exception) {
                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput();
            }

            $validator = Validator::make($inputs, [
                'coupon_id' => 'required|numeric|min:0',
                'discount' => 'required|numeric|min:0|max:100',
                'from' => 'required|date_format:Y-m-d',
                'to' => 'required|date_format:Y-m-d',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $cruises = $request->cruise_id;
            if (!is_array($cruises)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend.cruise') .'!')
                    ->withInput();
            }

            DB::transaction(function () use ($inputs, $cruises) {
                foreach ($cruises as $cruise_id) {
                    CruiseCoupon::query()->create([
                        'cruise_id' => $cruise_id,
                        'coupon_id' => $inputs['coupon_id'],
                        'discount' => $inputs['discount'],
                        'from' => $inputs['from'],
                        'to' => $inputs['to'],
                    ]);
                }
            });

            return redirect()->route('backend.cruise-coupon.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.cruise-promotion')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.cruise-promotion')]))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $listCruises = Cruise::query()->select('id', 'name')->get();
        $listCoupons = Coupon::query()->select('id', 'name')->get();

        $data = CruiseCoupon::query()->findOrFail($id);

        return view('backend.cruise-coupon.edit')
            ->with([
                'listCruises' => $listCruises,
                'listCoupons' => $listCoupons,
                'data' => $data
            ]);
    }

    public function update(Request $request, $id)
    {
        try {
            $inputs = $request->all();

            $inputs['coupon_id'] = clearXSS($request->coupon_id);
            $inputs['discount'] = clearXSS($request->discount);
            $inputs['from'] = clearXSS($request->from);
            $inputs['to'] = clearXSS($request->to);

            try {
                $inputs['from'] = \DateTime::createFromFormat('d/m/Y', $request->from);
                if ($inputs['from']) {
                    $inputs['from'] = $inputs['from']->format('Y-m-d');
                }
                $inputs['to'] = \DateTime::createFromFormat('d/m/Y', $request->to);
                if ($inputs['to']) {
                    $inputs['to'] = $inputs['to']->format('Y-m-d');
                }
            } catch (\Exception $exception) {
                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput();
            }

            $validator = Validator::make($inputs, [
                'coupon_id' => 'required|numeric|min:0',
                'discount' => 'required|numeric|min:0|max:100',
                'from' => 'required|date_format:Y-m-d',
                'to' => 'required|date_format:Y-m-d',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $cruises = $request->cruise_id;
            if (!is_array($cruises)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend.cruise') .'!')
                    ->withInput();
            }

            DB::transaction(function () use ($inputs, $cruises, $id) {
                $cruiseCoupon = CruiseCoupon::query()->findOrFail($id);

                foreach ($cruises as $cruise_id) {
                    if ($cruise_id == $cruiseCoupon->cruise_id) {
                        $cruiseCoupon->update([
                            'coupon_id' => $inputs['coupon_id'],
                            'discount' => $inputs['discount'],
                            'from' => $inputs['from'],
                            'to' => $inputs['to'],
                        ]);
                    } else {
                        CruiseCoupon::query()->create([
                            'cruise_id' => $cruise_id,
                            'coupon_id' => $inputs['coupon_id'],
                            'discount' => $inputs['discount'],
                            'from' => $inputs['from'],
                            'to' => $inputs['to'],
                        ]);
                    }
                }
            });

            return redirect()->route('backend.cruise-coupon.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.cruise-promotion')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('backend.cruise-promotion')]))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            CruiseCoupon::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
