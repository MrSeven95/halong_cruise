<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cruise;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.review.index');
    }

    public function datatable()
    {
        return DataTables::of(Review::query()
            ->with('cruise:id,name')
            ->get())
            ->addIndexColumn()
            ->addColumn('col_status', function ($row) {
                $col = 'status';
                $checked = ($row[$col] == 1 ? ' checked' : '');
                $res = '<div class="bt-switch" data-remote="'. route('backend.review.status', $row->id) .'" 
                data-column="'. $col .'" data-name="'. __('backend/review.status') .'"><input type="checkbox"'. $checked .' data-id="'. $row['id'] .'" 
                data-on-color="success" data-off-color="info" data-on-text="'. __('backend/review.status-on') .'" data-off-text="'. __('backend/review.status-off') .'"></div>';

                return $res;
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.review.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.review.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->addColumn('cruise_name', function($row) {
                return $row->cruise->name;
            })
            ->rawColumns(['col_status', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listCruises = Cruise::all();
        $maxRanking = Review::query()->max('display_ranking');
        return view('backend.review.create')
            ->with('maxRanking', $maxRanking + 1)
            ->with('listCruises', $listCruises);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->except('time_send_date', 'time_send_hour');

        $inputs['name'] = clearXSS($request->name);
        $inputs['country'] = clearXSS($request->country);
        $inputs['message'] = clearXSS($request->message);
        $inputs['title'] = clearXSS($request->title);
        $inputs['point_cruise_quality'] = clearXSS($request->point_cruise_quality);
        $inputs['point_food_and_drink'] = clearXSS($request->point_food_and_drink);
        $inputs['point_cabin_quality'] = clearXSS($request->point_cabin_quality);
        $inputs['title'] = clearXSS($request->title);
        $inputs['point_customer_service'] = clearXSS($request->point_customer_service);
        $inputs['point_activities'] = clearXSS($request->point_activities);
        $inputs['traveler_type'] = clearXSS($request->traveler_type);
        $inputs['traveler_rating'] = clearXSS($request->traveler_rating);
        $inputs['display_ranking'] = clearXSS($request->display_ranking);

        try {
            $inputs['time_send'] = \DateTime::createFromFormat('d/m/Y H:i:s', $request->time_send_date . ' ' . $request->time_send_hour);
            if ($inputs['time_send']) {
                $inputs['time_send'] = $inputs['time_send']->format('Y-m-d H:i:s');
            }
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput();
        }

        $validator = Validator::make($inputs, [
            'name' => 'required|max:100',
            'country' => 'required|max:100',
            'message' => 'required',
            'time_send' => 'required|date_format:Y-m-d H:i:s',
            'title' => 'required|max:100',
            'point_cruise_quality' => 'required|numeric|min:0|max:10',
            'point_food_and_drink' => 'required|numeric|min:0|max:10',
            'point_cabin_quality' => 'required|numeric|min:0|max:10',
            'point_customer_service' => 'required|numeric|min:0|max:10',
            'point_activities' => 'required|numeric|min:0|max:10',
            'traveler_type' => 'required|max:100',
            'traveler_rating' => 'required|numeric|min:0|max:10',
            'display_ranking' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            Review::query()->create($inputs);

            return redirect()->route('backend.review.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.review')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.review')]))
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listCruises = Cruise::all();
        $data = Review::query()->findOrFail($id);
        $maxRanking = Review::query()->max('display_ranking');

        return view('backend.review.edit')
            ->with('maxRanking', $maxRanking + 1)
            ->with('listCruises', $listCruises)
            ->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except('time_send_date', 'time_send_hour');

        $inputs['name'] = clearXSS($request->name);
        $inputs['country'] = clearXSS($request->country);
        $inputs['message'] = clearXSS($request->message);
        $inputs['title'] = clearXSS($request->title);
        $inputs['point_cruise_quality'] = clearXSS($request->point_cruise_quality);
        $inputs['point_food_and_drink'] = clearXSS($request->point_food_and_drink);
        $inputs['point_cabin_quality'] = clearXSS($request->point_cabin_quality);
        $inputs['point_customer_service'] = clearXSS($request->point_customer_service);
        $inputs['point_activities'] = clearXSS($request->point_activities);
        $inputs['traveler_type'] = clearXSS($request->traveler_type);
        $inputs['traveler_rating'] = clearXSS($request->traveler_rating);
        $inputs['display_ranking'] = clearXSS($request->display_ranking);

        try {
            $inputs['time_send'] = \DateTime::createFromFormat('d/m/Y H:i:s', $request->time_send_date . ' ' . $request->time_send_hour);
            if ($inputs['time_send']) {
                $inputs['time_send'] = $inputs['time_send']->format('Y-m-d H:i:s');
            }
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput();
        }

        $validator = Validator::make($inputs, [
            'name' => 'required|max:100',
            'country' => 'required|max:100',
            'message' => 'required|max:1000',
            'time_send' => 'required|date_format:Y-m-d H:i:s',
            'title' => 'required|max:100',
            'point_cruise_quality' => 'required|numeric|min:0|max:10',
            'point_food_and_drink' => 'required|numeric|min:0|max:10',
            'point_cabin_quality' => 'required|numeric|min:0|max:10',
            'point_customer_service' => 'required|numeric|min:0|max:10',
            'point_activities' => 'required|numeric|min:0|max:10',
            'traveler_type' => 'required|max:100',
            'traveler_rating' => 'required|numeric|min:0|max:10',
            'display_ranking' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            Review::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.review.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.review')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('backend.review')]))
                ->withInput();
        }
    }

    public function status(Request $request, $id)
    {
        $value = $request->status;
        if ($value == 'true') $value = 1;
        elseif ($value == 'false') $value = 0;
        else  {
            return response()->json([
                'status' => 'error'
            ]);
        }

        try {
            Review::query()->findOrFail($id)->update([
                'status' => $value
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Review::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
