<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\BedType;
use App\Models\Cabin;
use App\Models\CabinPrice;
use App\Models\Cruise;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CabinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable($id);
        }

        $data = Cruise::query()->findOrFail($id);

        return view('backend.cabin.index')
            ->with('data', $data);
    }

    public function datatable($cruise_id)
    {
        return DataTables::of(Cabin::query()
            ->where(['cruise_id' => $cruise_id])
            ->with('bedType:id,name')
            ->with('cabinPrice')
            ->get())
            ->addIndexColumn()
            ->addColumn('image', function ($row) {
                return $row->image_thumbnail ? $row->image_thumbnail : '';
            })
            ->addColumn('bed_type', function ($row) {
                return $row->bedType ? $row->bedType->name : '';
            })
            ->addColumn('cabin_price', function ($row) {
                $cabinPrice = $row->cabinPrice;
                //$cabinPrice = CabinPrice::query()->where(['cabin_id' => $row['id']])->get();
                $link = '';

                if ($cabinPrice && $cabinPrice->count() > 0) {
                    $minPrice = $cabinPrice->min('price');
                    //$minPrice = CabinPrice::query()->where(['cabin_id' => $row['id']])->min('price');
                    if ($minPrice > 0)
                        $link .= 'From '. $minPrice .' USD';

                    $link .= '<a href="' . route('backend.cabin-price.index', ['id' => $row->id]) . '" 
                        class="btn btn-primary mt-2" style="width: 120px">Update Price</a>';
                } else {
                    $link = '<a href="' . route('backend.cabin-price.index', ['id' => $row->id]) . '" 
                        class="btn btn-success" style="width: 120px">Add Price</a>';
                }

                return $link;
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.cabin.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.cabin.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->rawColumns(['more', 'action', 'cabin_price'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = Cruise::query()->findOrFail($id);
        $bedTypes = BedType::all();

        return view('backend.cabin.create')
            ->with([
                'data' => $data,
                'bedTypes' => $bedTypes
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $inputs = $request->except('image_album');

        $inputs['name'] = clearXSS($request->name);
        $inputs['cabin_size'] = clearXSS($request->cabin_size);
        $inputs['bed_type_id'] = clearXSS($request->bed_type_id);
        $inputs['capacity_adult'] = clearXSS($request->capacity_adult);
        $inputs['capacity_child'] = clearXSS($request->capacity_child);
        $inputs['introduction'] = clearXSS($request->introduction);

        if ($request->has('bathroom_towel')) {
            $inputs['bathroom_towel'] = 1;
        } else {
            $inputs['bathroom_towel'] = 0;
        }
        if ($request->has('bathroom_dryer')) {
            $inputs['bathroom_dryer'] = 1;
        } else {
            $inputs['bathroom_dryer'] = 0;
        }
        if ($request->has('bathroom_supplies')) {
            $inputs['bathroom_supplies'] = 1;
        } else {
            $inputs['bathroom_supplies'] = 0;
        }
        if ($request->has('bathroom_bathtub')) {
            $inputs['bathroom_bathtub'] = 1;
        } else {
            $inputs['bathroom_bathtub'] = 0;
        }
        if ($request->has('food_and_drinks_mini_bar')) {
            $inputs['food_and_drinks_mini_bar'] = 1;
        } else {
            $inputs['food_and_drinks_mini_bar'] = 0;
        }
        if ($request->has('food_and_drinks_fridge')) {
            $inputs['food_and_drinks_fridge'] = 1;
        } else {
            $inputs['food_and_drinks_fridge'] = 0;
        }
        if ($request->has('food_and_drinks_drinks')) {
            $inputs['food_and_drinks_drinks'] = 1;
        } else {
            $inputs['food_and_drinks_drinks'] = 0;
        }
        if ($request->has('food_and_drinks_foods')) {
            $inputs['food_and_drinks_foods'] = 1;
        } else {
            $inputs['food_and_drinks_foods'] = 0;
        }
        if ($request->has('cabin_facilities_set_of_bed_sheets')) {
            $inputs['cabin_facilities_set_of_bed_sheets'] = 1;
        } else {
            $inputs['cabin_facilities_set_of_bed_sheets'] = 0;
        }
        if ($request->has('cabin_facilities_hairdryer')) {
            $inputs['cabin_facilities_hairdryer'] = 1;
        } else {
            $inputs['cabin_facilities_hairdryer'] = 0;
        }
        if ($request->has('cabin_facilities_tivi')) {
            $inputs['cabin_facilities_tivi'] = 1;
        } else {
            $inputs['cabin_facilities_tivi'] = 0;
        }
        if ($request->has('cabin_facilities_air_conditional')) {
            $inputs['cabin_facilities_air_conditional'] = 1;
        } else {
            $inputs['cabin_facilities_air_conditional'] = 0;
        }
        if ($request->has('cabin_facilities_balcony')) {
            $inputs['cabin_facilities_balcony'] = 1;
        } else {
            $inputs['cabin_facilities_balcony'] = 0;
        }
        if ($request->has('cabin_facilities_wifi')) {
            $inputs['cabin_facilities_wifi'] = 1;
        } else {
            $inputs['cabin_facilities_wifi'] = 0;
        }
        if ($request->has('cabin_facilities_slippers')) {
            $inputs['cabin_facilities_slippers'] = 1;
        } else {
            $inputs['cabin_facilities_slippers'] = 0;
        }
        if ($request->has('cabin_facilities_wardrobe')) {
            $inputs['cabin_facilities_wardrobe'] = 1;
        } else {
            $inputs['cabin_facilities_wardrobe'] = 0;
        }
        if ($request->has('cabin_facilities_sofa')) {
            $inputs['cabin_facilities_sofa'] = 1;
        } else {
            $inputs['cabin_facilities_sofa'] = 0;
        }
        if ($request->has('cabin_facilities_smoking')) {
            $inputs['cabin_facilities_smoking'] = 1;
        } else {
            $inputs['cabin_facilities_smoking'] = 0;
        }
        
        $validator = Validator::make($inputs, [
            'name' => 'required|max:100',
            'cabin_size' => 'required|numeric|min:1',
            'bed_type_id' => 'required|numeric|min:1',
            'capacity_adult' => 'required|numeric|min:1|max:4',
            'capacity_child' => 'required|numeric|min:1|max:4',
            'introduction' => 'required',

            'bathroom_towel' => 'numeric|min:0|max:1',
            'bathroom_dryer' => 'numeric|min:0|max:1',
            'bathroom_supplies' => 'numeric|min:0|max:1',
            'bathroom_bathtub' => 'numeric|min:0|max:1',
            'food_and_drinks_mini_bar' => 'numeric|min:0|max:1',
            'food_and_drinks_fridge' => 'numeric|min:0|max:1',
            'food_and_drinks_drinks' => 'numeric|min:0|max:1',
            'food_and_drinks_foods' => 'numeric|min:0|max:1',
            'cabin_facilities_set_of_bed_sheets' => 'numeric|min:0|max:1',
            'cabin_facilities_hairdryer' => 'numeric|min:0|max:1',
            'cabin_facilities_tivi' => 'numeric|min:0|max:1',
            'cabin_facilities_air_conditional' => 'numeric|min:0|max:1',
            'cabin_facilities_balcony' => 'numeric|min:0|max:1',
            'cabin_facilities_wifi' => 'numeric|min:0|max:1',
            'cabin_facilities_slippers' => 'numeric|min:0|max:1',
            'cabin_facilities_wardrobe' => 'numeric|min:0|max:1',
            'cabin_facilities_sofa' => 'numeric|min:0|max:1',
            'cabin_facilities_smoking' => 'numeric|min:0|max:1',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $cruise = Cruise::query()->findOrFail($id);
            $slug = $cruise->slug;

            if ($request->has('image_avatar')) {
                $inputs['image_avatar'] = upload_image($request->file('image_avatar'), $slug);
                $inputs['image_thumbnail'] = make_thumb('600', '400', $inputs['image_avatar']);
            }

            if ($request->has('image_album') && $request->hasFile('image_album')) {
                $i = 1;
                foreach ($request->file('image_album') as $file) {
                    $inputs['image_album_'. $i++] = upload_image($file, $slug);
                }
            }

            Cabin::query()->create($inputs);

            return redirect()->route('backend.cruise.cabin.index', ['id' => $id])
                ->with('success', __('web.create-successed', ['name' => __('backend.cabin')]));
        } catch (\Exception $exception) {
            if (exist_image($inputs['image_avatar'])) remove_image($inputs['image_avatar']);
            if (exist_image($inputs['image_thumbnail'])) remove_image($inputs['image_thumbnail']);

            if (exist_image($inputs['image_album_1'])) remove_image($inputs['image_album_1']);
            if (exist_image($inputs['image_album_2'])) remove_image($inputs['image_album_2']);
            if (exist_image($inputs['image_album_3'])) remove_image($inputs['image_album_3']);
            if (exist_image($inputs['image_album_4'])) remove_image($inputs['image_album_4']);
            if (exist_image($inputs['image_album_5'])) remove_image($inputs['image_album_5']);

            return back()
                ->with('error', __('web.create-errored', ['name' => __('backend.cabin')]))
                ->with('exception', $exception->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function show(Cabin $cabin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Cabin::query()->findOrFail($id);
        $cruise = Cruise::query()->findOrFail($data->cruise_id);
        $bedTypes = BedType::all();

        return view('backend.cabin.edit')
            ->with([
                'cruise' => $cruise,
                'data' => $data,
                'bedTypes' => $bedTypes
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except(['image_album', 'is_no_action_image_avatar', 'is_no_action_image_album_0',
            'is_no_action_image_album_1', 'is_no_action_image_album_2', 'is_no_action_image_album_3', 'is_no_action_image_album_4']);

        $inputs['name'] = clearXSS($request->name);
        $inputs['cabin_size'] = clearXSS($request->cabin_size);
        $inputs['bed_type_id'] = clearXSS($request->bed_type_id);
        $inputs['capacity_adult'] = clearXSS($request->capacity_adult);
        $inputs['capacity_child'] = clearXSS($request->capacity_child);
        $inputs['introduction'] = clearXSS($request->introduction);

        if ($request->has('bathroom_towel')) {
            $inputs['bathroom_towel'] = 1;
        } else {
            $inputs['bathroom_towel'] = 0;
        }
        if ($request->has('bathroom_dryer')) {
            $inputs['bathroom_dryer'] = 1;
        } else {
            $inputs['bathroom_dryer'] = 0;
        }
        if ($request->has('bathroom_supplies')) {
            $inputs['bathroom_supplies'] = 1;
        } else {
            $inputs['bathroom_supplies'] = 0;
        }
        if ($request->has('bathroom_bathtub')) {
            $inputs['bathroom_bathtub'] = 1;
        } else {
            $inputs['bathroom_bathtub'] = 0;
        }
        if ($request->has('food_and_drinks_mini_bar')) {
            $inputs['food_and_drinks_mini_bar'] = 1;
        } else {
            $inputs['food_and_drinks_mini_bar'] = 0;
        }
        if ($request->has('food_and_drinks_fridge')) {
            $inputs['food_and_drinks_fridge'] = 1;
        } else {
            $inputs['food_and_drinks_fridge'] = 0;
        }
        if ($request->has('food_and_drinks_drinks')) {
            $inputs['food_and_drinks_drinks'] = 1;
        } else {
            $inputs['food_and_drinks_drinks'] = 0;
        }
        if ($request->has('food_and_drinks_foods')) {
            $inputs['food_and_drinks_foods'] = 1;
        } else {
            $inputs['food_and_drinks_foods'] = 0;
        }
        if ($request->has('cabin_facilities_set_of_bed_sheets')) {
            $inputs['cabin_facilities_set_of_bed_sheets'] = 1;
        } else {
            $inputs['cabin_facilities_set_of_bed_sheets'] = 0;
        }
        if ($request->has('cabin_facilities_hairdryer')) {
            $inputs['cabin_facilities_hairdryer'] = 1;
        } else {
            $inputs['cabin_facilities_hairdryer'] = 0;
        }
        if ($request->has('cabin_facilities_tivi')) {
            $inputs['cabin_facilities_tivi'] = 1;
        } else {
            $inputs['cabin_facilities_tivi'] = 0;
        }
        if ($request->has('cabin_facilities_air_conditional')) {
            $inputs['cabin_facilities_air_conditional'] = 1;
        } else {
            $inputs['cabin_facilities_air_conditional'] = 0;
        }
        if ($request->has('cabin_facilities_balcony')) {
            $inputs['cabin_facilities_balcony'] = 1;
        } else {
            $inputs['cabin_facilities_balcony'] = 0;
        }
        if ($request->has('cabin_facilities_wifi')) {
            $inputs['cabin_facilities_wifi'] = 1;
        } else {
            $inputs['cabin_facilities_wifi'] = 0;
        }
        if ($request->has('cabin_facilities_slippers')) {
            $inputs['cabin_facilities_slippers'] = 1;
        } else {
            $inputs['cabin_facilities_slippers'] = 0;
        }
        if ($request->has('cabin_facilities_wardrobe')) {
            $inputs['cabin_facilities_wardrobe'] = 1;
        } else {
            $inputs['cabin_facilities_wardrobe'] = 0;
        }
        if ($request->has('cabin_facilities_sofa')) {
            $inputs['cabin_facilities_sofa'] = 1;
        } else {
            $inputs['cabin_facilities_sofa'] = 0;
        }
        if ($request->has('cabin_facilities_smoking')) {
            $inputs['cabin_facilities_smoking'] = 1;
        } else {
            $inputs['cabin_facilities_smoking'] = 0;
        }

        $validator = Validator::make($inputs, [
            'name' => 'required|max:100',
            'cabin_size' => 'required|numeric|min:1',
            'bed_type_id' => 'required|numeric|min:1',
            'capacity_adult' => 'required|numeric|min:1|max:4',
            'capacity_child' => 'required|numeric|min:1|max:4',
            'introduction' => 'required',

            'bathroom_towel' => 'numeric|min:0|max:1',
            'bathroom_dryer' => 'numeric|min:0|max:1',
            'bathroom_supplies' => 'numeric|min:0|max:1',
            'bathroom_bathtub' => 'numeric|min:0|max:1',
            'food_and_drinks_mini_bar' => 'numeric|min:0|max:1',
            'food_and_drinks_fridge' => 'numeric|min:0|max:1',
            'food_and_drinks_drinks' => 'numeric|min:0|max:1',
            'food_and_drinks_foods' => 'numeric|min:0|max:1',
            'cabin_facilities_set_of_bed_sheets' => 'numeric|min:0|max:1',
            'cabin_facilities_hairdryer' => 'numeric|min:0|max:1',
            'cabin_facilities_tivi' => 'numeric|min:0|max:1',
            'cabin_facilities_air_conditional' => 'numeric|min:0|max:1',
            'cabin_facilities_balcony' => 'numeric|min:0|max:1',
            'cabin_facilities_wifi' => 'numeric|min:0|max:1',
            'cabin_facilities_slippers' => 'numeric|min:0|max:1',
            'cabin_facilities_wardrobe' => 'numeric|min:0|max:1',
            'cabin_facilities_sofa' => 'numeric|min:0|max:1',
            'cabin_facilities_smoking' => 'numeric|min:0|max:1',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $arr_delete = [];
        try {
            $cabin = Cabin::query()->findOrFail($id);

            $slug = $cabin->cruise->slug;

            if ($request->has('image_avatar')) {
                $inputs['image_avatar'] = upload_image($request->file('image_avatar'), $slug);
                $inputs['image_thumbnail'] = make_thumb('600', '400', $inputs['image_avatar']);

                array_push($arr_delete, $cabin->image_avatar);
                array_push($arr_delete, $cabin->image_thumbnail);
            } else if ($request->has('is_no_action_image_avatar') && $request->input('is_no_action_image_avatar') == 'false') {
                $inputs['image_avatar'] = null;
                $inputs['image_thumbnail'] = null;

                array_push($arr_delete, $cabin->image_avatar);
                array_push($arr_delete, $cabin->image_thumbnail);
            }

            $count = 0;
            if ($cabin->image_album_1 != '' && $request->has('is_no_action_image_album_0') && $request->input('is_no_action_image_album_0') == 'true') {
                $count++;
            }
            if ($cabin->image_album_2 != '' && $request->has('is_no_action_image_album_1') && $request->input('is_no_action_image_album_1') == 'true') {
                $count++;
            }
            if ($cabin->image_album_3 != '' && $request->has('is_no_action_image_album_2') && $request->input('is_no_action_image_album_2') == 'true') {
                $count++;
            }
            if ($cabin->image_album_4 != '' && $request->has('is_no_action_image_album_3') && $request->input('is_no_action_image_album_3') == 'true') {
                $count++;
            }
            if ($cabin->image_album_5 != '' && $request->has('is_no_action_image_album_4') && $request->input('is_no_action_image_album_4') == 'true') {
                $count++;
            }
            if ($request->has('is_no_action_image_album_0') && $request->input('is_no_action_image_album_0') == 'false') {
                $inputs['image_album_1'] = null;

                array_push($arr_delete, $cabin->image_album_1);
            }
            if ($request->has('is_no_action_image_album_1') && $request->input('is_no_action_image_album_1') == 'false') {
                $inputs['image_album_2'] = null;

                array_push($arr_delete, $cabin->image_album_1);
            }
            if ($request->has('is_no_action_image_album_2') && $request->input('is_no_action_image_album_2') == 'false') {
                $inputs['image_album_3'] = null;

                array_push($arr_delete, $cabin->image_album_1);
            }
            if ($request->has('is_no_action_image_album_3') && $request->input('is_no_action_image_album_3') == 'false') {
                $inputs['image_album_4'] = null;

                array_push($arr_delete, $cabin->image_album_1);
            }
            if ($request->has('is_no_action_image_album_4') && $request->input('is_no_action_image_album_4') == 'false') {
                $inputs['image_album_5'] = null;

                array_push($arr_delete, $cabin->image_album_1);
            }

            if ($request->has('image_album') && $request->hasFile('image_album')) {
                $i = $count;
                foreach ($request->file('image_album') as $file) {
                    $inputs['image_album_'. ++$i] = upload_image($file, $slug);
                }
            }

            $cabin->update($inputs);

        } catch (\Exception $exception) {
            if (array_key_exists('image_avatar', $inputs) && exist_image($inputs['image_avatar'])) remove_image($inputs['image_avatar']);
            if (array_key_exists('image_thumbnail', $inputs) && exist_image($inputs['image_thumbnail'])) remove_image($inputs['image_thumbnail']);

            if (array_key_exists('image_album_1', $inputs) && exist_image($inputs['image_album_1'])) remove_image($inputs['image_album_1']);
            if (array_key_exists('image_album_2', $inputs) && exist_image($inputs['image_album_2'])) remove_image($inputs['image_album_2']);
            if (array_key_exists('image_album_3', $inputs) && exist_image($inputs['image_album_3'])) remove_image($inputs['image_album_3']);
            if (array_key_exists('image_album_4', $inputs) && exist_image($inputs['image_album_4'])) remove_image($inputs['image_album_4']);
            if (array_key_exists('image_album_5', $inputs) && exist_image($inputs['image_album_5'])) remove_image($inputs['image_album_5']);

            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend.cabin')]));
        }

        foreach ($arr_delete as $value) {
            if (exist_image($value)) remove_image($value);
        }

        return redirect()->route('backend.cruise.cabin.index', ['id' => $cabin->cruise_id])
            ->with('success', __('web.edit-successed', ['name' => __('backend.cabin')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cabin  $cabin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Cabin::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
