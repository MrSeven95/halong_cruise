<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ConfigFaq;
use App\Models\Faq;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.faq.index');
    }

    public function datatable()
    {
        return DataTables::of(Faq::where('id', '>', 0)->get())
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                $btn = '<a href="' . route('backend.faq.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.faq.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';

                return $btn;
            })
            ->addColumn('col_status', function ($row) {
                $col = 'status';
                $checked = ($row[$col] == 1 ? ' checked' : '');
                $res = '<div class="bt-switch" data-remote="'. route('backend.faq.status', $row->id) .'" 
                data-column="'. $col .'" data-name="'. __('backend/faq.status') .'"><input type="checkbox"'. $checked .' data-id="'. $row['id'] .'" 
                data-on-color="success" data-off-color="info" data-on-text="'. __('backend/faq.status-on') .'" data-off-text="'. __('backend/faq.status-off') .'"></div>';

                return $res;
            })
            ->rawColumns(['col_status', 'action'])
            ->make(true);
    }

    public function create()
    {
        return view('backend.faq.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->all();

        $inputs['subject'] = clearXSS($request->subject);
        $inputs['question'] = clearXSS($request->question);
        $inputs['answer'] = removeScripts(trim($request->answer));

        $validator = Validator::make($inputs, [
            'subject' => 'required',
            'question' => 'required',
            'answer' => 'required'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            Faq::query()->create($inputs);

            return redirect()->route('backend.faq.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.faq')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.create-errored', ['name' => __('backend.faq')]));
        }
    }

    public function show(Faq $faq)
    {
        //
    }

    public function edit($id)
    {
        $data = Faq::query()->findOrFail($id);

        return view('backend.faq.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['subject'] = clearXSS($request->subject);
        $inputs['question'] = clearXSS($request->question);
        $inputs['answer'] = removeScripts(trim($inputs['answer']));

        $validator = Validator::make($inputs, [
            'subject' => 'required',
            'question' => 'required',
            'answer' => 'required'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            Faq::query()->findOrFail($id)->update($inputs);

            return redirect()->route('backend.faq.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.faq')]));;
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend.faq')]));
        }
    }

    public function status(Request $request, $id)
    {
        $value = $request->status;
        if ($value == 'true') $value = 1;
        elseif ($value == 'false') $value = 0;
        else {
            return response()->json([
                'status' => 'error'
            ]);
        }

        try {
            Faq::query()->findOrFail($id)->update([
                'status' => $value
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            Faq::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
    public function editConfig()
    {
        $data = ConfigFaq::query()->first();
        return view('backend.faq.config')
            ->with('data', $data);
    }

    public function updateConfig(Request $request)
    {
        $inputs = $request->except(['is_no_action_image_avatar', 'is_no_action_image_seo', 'areas']);

        $inputs['heading'] = removeScripts(trim($inputs['heading']));
        $inputs['slug'] = removeScripts(trim($inputs['slug']));
        $inputs['meta_title'] = removeScripts(trim($inputs['meta_title']));
        $inputs['meta_description'] = removeScripts(trim($inputs['meta_description']));
        $inputs['meta_keyword'] = removeScripts(trim($inputs['meta_keyword']));

        $validator = Validator::make($inputs, [
            'heading' => 'required|max:100',
            'slug' => 'required|max:100',
            'meta_title' => 'required|max:255',
            'meta_description' => 'required',
            'meta_keyword' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $slug = "faq";

            if ($request->has('image_avatar')) {
                $inputs['image_avatar'] = upload_image($request->file('image_avatar'), $slug);
                //$inputs['image_thumbnail'] = make_thumb('600', '450', $inputs['image_avatar']);
            } else if ($request->has('is_no_action_image_avatar') && $request->input('is_no_action_image_avatar') == 'false') {
                $inputs['image_avatar'] = null;
            }

            if ($request->has('image_seo')) {
                $inputs['image_seo'] = upload_image($request->file('image_seo'), $slug);
            } else if ($request->has('is_no_action_image_seo') && $request->input('is_no_action_image_seo') == 'false') {
                $inputs['image_seo'] = null;
            }

            if (ConfigFaq::query()->count('heading') == 1) {
                ConfigFaq::query()->first()->update($inputs);
            } else {
                ConfigFaq::query()->create($inputs);
            }

            return redirect()->route('backend.faq.index')
                ->with('success', __('web.edit-successed', ['name' => __('backend.faq-config')]));
        } catch (\Exception $exception) {
            if (exist_image($inputs['image_avatar'])) remove_image($inputs['image_avatar']);
            if (exist_image($inputs['image_seo'])) remove_image($inputs['image_seo']);

            return back()
                ->with('exception', $exception->getMessage())
                ->withInput()
                ->with('error', __('web.edit-errored', ['name' => __('backend.faq-config')]));
        }
    }
}
