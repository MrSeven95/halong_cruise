<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\FreeService;
use App\Models\Cruise;
use App\Models\CruiseFreeService;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CruiseFreeServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('ajax')) {
            return $this->datatable();
        }

        return view('backend.cruise-free-service.index');
    }


    public function show(Request $request, $id)
    {
        //
    }

    public function datatable() {
        return DataTables::of(CruiseFreeService::query()
            ->with('cruise:id,name')
            ->with('freeService:id,name')
            ->get())
            ->addIndexColumn()
            ->addColumn('cruise', function ($row) {
                return $row->cruise ? $row->cruise->name : '';
            })
            ->addColumn('free-service', function ($row) {
                return $row->freeService ? $row->freeService->name : '';
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('backend.cruise-free-service.edit', ['id' => $row->id]) . '" 
                        class="btn btn-primary btn-sm">'.__('web.edit').'</a>';

                $btn .= '<a href="#" data-remote="' . route('backend.cruise-free-service.destroy', ['id' => $row->id]) . '" 
                        class="btn btn-dark btn-sm btn-delete text-white">'.__('web.delete').'</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listCruises = Cruise::query()->select('id', 'name')->get();
        $listFreeServices = FreeService::query()->select('id', 'name')->get();

        return view('backend.cruise-free-service.create')
            ->with([
                'listCruises' => $listCruises,
                'listFreeServices' => $listFreeServices
            ]);
    }

    public function store(Request $request)
    {
        try {
            $inputs = $request->all();

            $inputs['from'] = clearXSS($request->from);
            $inputs['to'] = clearXSS($request->to);

            try {
                $inputs['from'] = \DateTime::createFromFormat('d/m/Y', $request->from);
                if ($inputs['from']) {
                    $inputs['from'] = $inputs['from']->format('Y-m-d');
                }
                $inputs['to'] = \DateTime::createFromFormat('d/m/Y', $request->to);
                if ($inputs['to']) {
                    $inputs['to'] = $inputs['to']->format('Y-m-d');
                }
            } catch (\Exception $exception) {
                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput();
            }

            $validator = Validator::make($inputs, [
                'from' => 'required|date_format:Y-m-d',
                'to' => 'required|date_format:Y-m-d',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $cruises = $request->cruise_id;
            if (!is_array($cruises)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend.cruise') .'!')
                    ->withInput();
            }

            $freeServices = $request->free_service;
            if (!is_array($freeServices)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend.free-service') .'!')
                    ->withInput();
            }

            DB::transaction(function () use ($inputs, $cruises, $freeServices) {
                foreach ($cruises as $cruise_id) {
                    foreach ($freeServices as $freeService_id) {
                        CruiseFreeService::query()->create([
                            'cruise_id' => $cruise_id,
                            'free_service_id' => $freeService_id,
                            'from' => $inputs['from'],
                            'to' => $inputs['to'],
                        ]);
                    }
                }
            });

            return redirect()->route('backend.cruise-free-service.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.cruise-free-service')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.cruise-free-service')]))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $listCruises = Cruise::query()->select('id', 'name')->get();
        $listFreeServices = FreeService::query()->select('id', 'name')->get();
        $data = CruiseFreeService::query()->findOrFail($id);

        return view('backend.cruise-free-service.edit')
            ->with([
                'listCruises' => $listCruises,
                'listFreeServices' => $listFreeServices,
                'data' => $data
            ]);
    }

    public function update(Request $request, $id)
    {
        try {
            $inputs = $request->all();

            $inputs['from'] = clearXSS($request->from);
            $inputs['to'] = clearXSS($request->to);

            try {
                $inputs['from'] = \DateTime::createFromFormat('d/m/Y', $request->from);
                if ($inputs['from']) {
                    $inputs['from'] = $inputs['from']->format('Y-m-d');
                }
                $inputs['to'] = \DateTime::createFromFormat('d/m/Y', $request->to);
                if ($inputs['to']) {
                    $inputs['to'] = $inputs['to']->format('Y-m-d');
                }
            } catch (\Exception $exception) {
                return back()
                    ->with('exception', $exception->getMessage())
                    ->withInput();
            }

            $validator = Validator::make($inputs, [
                'from' => 'required|date_format:Y-m-d',
                'to' => 'required|date_format:Y-m-d',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $cruises = $request->cruise_id;
            if (!is_array($cruises)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend.cruise') .'!')
                    ->withInput();
            }

            $freeServices = $request->free_service;
            if (!is_array($freeServices)) {
                return back()
                    ->with('exception', 'Wrong data '. __('backend.free-service') .'!')
                    ->withInput();
            }

            DB::transaction(function () use ($inputs, $cruises, $freeServices, $id) {
                $cruiseFreeService = CruiseFreeService::query()->findOrFail($id);

                foreach ($cruises as $cruise_id) {
                    if ($cruise_id == $cruiseFreeService->cruise_id) {
                        if (!in_array($cruiseFreeService->free_service_id, $freeServices)) {
                            $cruiseFreeService->delete();
                        }
                        foreach ($freeServices as $freeService_id) {
                            if ($freeService_id == $cruiseFreeService->free_service_id) {
                                $cruiseFreeService->update([
                                    'from' => $inputs['from'],
                                    'to' => $inputs['to'],
                                ]);
                            } else {
                                CruiseFreeService::query()->create([
                                    'cruise_id' => $cruise_id,
                                    'free_service_id' => $freeService_id,
                                    'from' => $inputs['from'],
                                    'to' => $inputs['to'],
                                ]);
                            }
                        }
                    } else {
                        foreach ($freeServices as $freeService_id) {
                            CruiseFreeService::query()->create([
                                'cruise_id' => $cruise_id,
                                'free_service_id' => $freeService_id,
                                'from' => $inputs['from'],
                                'to' => $inputs['to'],
                            ]);
                        }
                    }
                }
            });

            return redirect()->route('backend.cruise-free-service.index')
                ->with('success', __('web.create-successed', ['name' => __('backend.cruise-free-service')]));
        } catch (\Exception $exception) {
            return back()
                ->with('exception', $exception->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('backend.cruise-free-service')]))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            CruiseFreeService::query()->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
