<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TrimDauSao
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function __construct(){}

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }
        return $next($request);

        $routeName =  $request->route()->getActionName();

        $needFilter = false;
        $search = [
            '@create', '@edit',
            'CruiseAreaController@index',
            'OtherServiceController@index',
        ];

        foreach ($search as $value) {
            if(preg_match("/{$value}/i", $routeName)) {
                $needFilter = true;
                break;
            }
        }

        if($needFilter) {
            $res = $next($request);

            if(!method_exists($res, 'getOriginalContent'))
                return $res;

            $data = $res->getOriginalContent();

            $search = '(*)</label>';
            $replace = '<span style="color:red">(*)</span></label>';

            $content = str_replace($search, $replace, $data);

            $content = $data;

            //$res->setContent($content);

            return $res;
        }

        return $next($request);
    }
}
