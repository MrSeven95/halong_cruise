$('#myCarousel').carousel({
  interval: false
});
$('#myCarousel').carousel({
  interval: 10000
});
$(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if(scroll < 300){
        $('#menu').css('background', 'transparent');
        $('#menu .navbar-brand img').attr('src','img/logo_white.png');
        $('#menu .navbar-toggler').css('background','transparent');
        $('#menu .navbar-toggler-icon').removeClass('navbar-toggler-fixed');
    } else{
        $('#menu').css('background', '#141F35');
        $('#menu .navbar-brand img').attr('src','img/logo_brown.png');
        $('#menu .navbar-toggler-icon').addClass('navbar-toggler-fixed');
    }
});

function setCheckout(){
	var select = $('#itinerary').val();
	var checkin = $('#checkin').val();
	var checkout = $('#checkout');
	if(select != '' & select != 0 && select != null){
		if(checkin != ''){
			// getdate checkin
			var days = parseInt(select);

			var today = new Date(checkin);

			today.setDate(today.getDate() + days);
			str = yyyymmdd(today);
			strYear = str.substr(0,4);
			strMonth = str.substr(4,2);
			strDay = str.substr(6,2);
			string = strMonth+'/'+strDay+'/'+strYear;
			checkout.val(string);
		}
	}
}
function yyyymmdd(dateIn) {
   var yyyy = dateIn.getFullYear();
   var mm = dateIn.getMonth()+1; // getMonth() is zero-based
   var dd  = dateIn.getDate();
   return String(10000*yyyy + 100*mm + dd); // Leading zeros for mm and dd
}

$(document).ready(function(){
	$('#checkin').datepicker();
	$('#icon-calendar').on('click', function(e){
		e.preventDefault();
		$('#checkin').datepicker("show");
	})
});

/* JS search room */
function setRoom(list_room,room){
	html = '';
	html += '<div class="list-room__item mb-3" id="room_'+room+'">';
	html += '<h5>#'+room+' Room</h5>';					
	html += '<div class="row">';					
	html += '<div class="col-sm-4">';					
	html += '<label>Adults</label>';					
	html += '<select name="adults" class="form-control">';					
	html += '<option value="1">1</option>';					
	html += '<option value="2">2</option>';					
	html += '<option value="3">3</option>';					
	html += '<option value="4">4</option>';					
	html += '</select>';					
	html += '</div>';					
	html += '<div class="col-sm-4">';
	html += '<label>Children</label>';					
	html += '<select name="children" class="form-control">';
	html += '<option value="0">0</option>';
	html += '<option value="1">1</option>';
	html += '<option value="2">2</option>';
	html += '</select>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	list_room.append(html);
}

function countAdults(){
	var totalAdults = 0
	$.each($("select[name=adults]"), function( index, value ) {
 		totalAdults += parseInt($(value).val())
	});
	return totalAdults
}
function countChildren(){
	var totalChildren = 0
	$.each($("select[name=children]"), function( index, value ) {
 		totalChildren += parseInt($(value).val())
	});
	return totalChildren;
}
$(document).ready(function(){
	$('#desc_room').on('click', function(){
		room = parseInt($('#total-room').val());
		if(room >1){
			$('#room_' + room).remove(); 
			room = room - 1;
			$('#total-room').val(room);
		}
	})
	$('#asc_room').on('click', function(){
		room = parseInt($('#total-room').val());
		if(room < 5){
			room += 1;
			$('#total-room').val(room);
			setRoom($('#list-room'),$('#total-room').val())
		}
	})
	setRoom($('#list-room'), 1);

	// click binding textbox
	$('.btn-done').click(function(){
		text = '';
		room = $('#total-room').val()
		if(room > 1){
			text += room +' Rooms, ';
		}else
			text += room +' Room, ';
		adults = countAdults();
		if (adults > 1) {
			text += adults + ' Adults, '
		}else
			text += adults + ' Adult, '

		children = countChildren();
		
		text += children + ' Children'

		$('#passenger').val(text);
		$('#RoomAdultModal').modal('hide');
	});
});
/* End JS search room */

/**** JS filter ***/
// Open box filter
$(document).ready(function(){
	$('.area').on('click', function (e){
		e.preventDefault(); 
        e.stopPropagation();
		$('#filter-area').slideToggle("slow");
		$('#filter-area').addClass("down");
		if($('#filter-star').hasClass('down')){
			$('#filter-star').slideUp("slow");
			$('#filter-star').removeClass("down");
		}
		if($('#filter-price').hasClass('down')){
			$('#filter-price').slideUp("slow");
			$('#filter-price').removeClass("down");
		}
	});
	$('.filter-area').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
    });
	$('.star').on('click', function (e){
		e.preventDefault(); 
        e.stopPropagation();
		$('#filter-star').slideToggle("slow");
		$('#filter-star').addClass("down");
		if($('#filter-area').hasClass('down')){
			$('#filter-area').slideUp("slow");
			$('#filter-area').removeClass("down");
		}
		if($('#filter-price').hasClass('down')){
			$('#filter-price').slideUp("slow");
			$('#filter-price').removeClass("down");
		}
	});
	$('.price-range').on('click', function (e){
		e.preventDefault(); 
        e.stopPropagation();
		$('#filter-price').slideToggle("slow");
		$('#filter-price').addClass("down");
		if($('#filter-area').hasClass('down')){
			$('#filter-area').removeClass("down");
			$('#filter-area').slideUp("slow");
		}
		if($('#filter-star').hasClass('down')){
			$('#filter-star').removeClass("down");
			$('#filter-star').slideUp("slow");
		}
	});
	$('body').click(function(e) {
	    $(".filter-area").slideUp();
	    $('.filter-area').removeClass("down");
	});
});

// Price
(function( $ ){
$( document ).ready( function() {
	$( '.input-range').each(function(){
		var value = $(this).attr('data-slider-value');
		var separator = value.indexOf(',');
		if( separator !== -1 ){
			value = value.split(',');
			value.forEach(function(item, i, arr) {
				arr[ i ] = parseFloat( item );
			});
		} else {
			value = parseFloat( value );
		}
		$( this ).slider({
			formatter: function(value) {
				return '$' + value;
			},
			min: parseFloat( $( this ).attr('data-slider-min') ),
			max: parseFloat( $( this ).attr('data-slider-max') ), 
			range: $( this ).attr('data-slider-range'),
			value: value,
			tooltip_split: $( this ).attr('data-slider-tooltip_split'),
			tooltip: $( this ).attr('data-slider-tooltip')
		});
	});
	
 } );
} )( jQuery );
$(document).ready(function(){
	var price = $('.input-range').val();
	if(price > 0){
		price = price.split(',');
		min_price = price[0];
		max_price = price[1];
		$('#min-price').val(min_price);
		$('#max-price').val(max_price);
	}
	
	$('.input-range').on('change', function(){
		price = $(this).val();
		price = price.split(',');
		min_price = price[0];
		max_price = price[1];
		$('#min-price').val(min_price);
		$('#max-price').val(max_price);
		if($('#min-price').val() > 0 || $('#max-price').val() < 100){
			str = '';
			str += '$'+$('#min-price').val();
			str += ' - $' + $('#max-price').val();
			// console.log(str);
			$('#price-field').html(str)
		}else
			$('#price-field').html('please select');
	});
});
$(document).ready(function(){
	$('#filter-star input[type=checkbox]').on('change', function (){
		var favorite = [];
        $.each($("#filter-star input[type='checkbox']:checked"), function(){            
            favorite.push($(this).val());
        });
        if(favorite.length > 0){
        	favorite.reverse();
        	str_star = favorite.join(", ");
        	str_star += ' star';
        	$('#star-field').html(str_star);
        	console.log(favorite);
        }else
        	$('#star-field').html('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>');
        // console.log(favorite.join(", "));
	});
	$('#filter-area input[type=checkbox]').on('change', function (){
		var place = [];
        $.each($("#filter-area input[type='checkbox']:checked"), function(){            
            place.push($(this).val());
        });
        count_place = place.length;
        if(count_place > 0){
        	str_place = count_place+' place';
        	$('#area-field').html(str_place);
        }else
        	$('#area-field').html('please select');
        // console.log(count_place);
	});
});

/*** End JS filter ***/

/**
 * Detail cruise
 */


/**
 * End cruise
 */